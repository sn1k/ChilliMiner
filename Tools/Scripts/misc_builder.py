#!/usr/bin/python
#
#  misc_builder.py
#  Miner

import sys
import os
import subprocess
import shutil
import file_system_utils

ignoreDirs = ["Models", "Text", "Textures", "TextureAtlases"]

#------------------------------------------------------------------------------
# Walks the input directory and converts all DAEs into csmodels.
#
# @author S Downie
#
# @param Input path
# @param Output path
#------------------------------------------------------------------------------
def build(input_path, output_path):
	
	print("-----------------------------------------")
	print("        Copying Everything else")
	print("-----------------------------------------")

	if(input_path.endswith("/") == False):
		input_path = input_path + "/"

	if(output_path.endswith("/") == False):
		output_path = output_path + "/"

	for item in os.listdir(input_path):

		src = os.path.join(input_path, item)
		dst = os.path.join(output_path, item)

		if (os.path.isdir(src)):

			if(item in ignoreDirs): # ignore directories
				continue

			file_system_utils.copy_directory(src, dst)
		else:
			shutil.copy2(src, dst)

		print(dst)

	print (" ")

#------------------------------------------------------------------------------
# The entry point into the script.
#
# @author S Downie
#
# @param The list of arguments.
#------------------------------------------------------------------------------
def main(args):
	if not len(args) is 3:
		print("ERROR: Incorrect parameters supplied.")
		return

	input_path = args[1]
	output_path = args[2]
	build(input_path, output_path)

if __name__ == "__main__":
	main(sys.argv)

