from tkinter import *

import time
import imageBank
import os

import model_builder
import text_builder
import texture_atlas_builder
import texture_builder
import misc_builder

# convert a gif to base64
#import base64
#with open("test.gif", "rb") as image_file:
#    encoded_string = base64.b64encode(image_file.read())
#    print("b64: " + encoded_string)

k_version = "0.5"


k_projectPath = "C:\dev\VS2013Projects\Miner\Content"

model_directory_path = "Models/"
text_directory_path = "Text/"
texture_atlas_directory_path = "TextureAtlases/"
texture_directory_path = "Textures/"

k_inputPath = "PrebuiltResources/"
k_outputPath = "AppResources/"

optionTextures = None
optionTextureAtlases = None
optionModels = None
optionText = None
optionOthers = None


class BuildFlag:

	def __init__(self, in_root, in_text):
		self.var = IntVar()
		self.c = Checkbutton(
			in_root, text=in_text,
			variable=self.var)
		self.c.pack(anchor=W)

	def IsSet(self):
		return self.var.get() == 1


class ImageIcon:

	def __init__(self, in_root, in_b64Data):
	
		photo = PhotoImage(data=in_b64Data)
		photo = photo.subsample(3,3) # scale third width/height

		self.label = Label(in_root, image=photo)
		self.label.image = photo # prevent gc
		self.label.config(width=100)
		self.label.pack()


class HelpMenu:

	def __init__(self, in_root):
		
		menu = Menu(in_root)
		in_root.config(menu=menu)

		menu.add_command(label="Help", command=self.OnMenuHelpSelected)

	def OnMenuHelpSelected(self):
		print("pew")


def callback():
	
	if(optionTextures.IsSet()):
		texture_builder.build(os.path.join(k_inputPath, texture_directory_path), os.path.join(k_outputPath, texture_directory_path))

	if(optionModels.IsSet()):
		model_builder.build_models(os.path.join(k_inputPath, model_directory_path), os.path.join(k_outputPath, model_directory_path))

	if(optionTextureAtlases.IsSet()):
		texture_atlas_builder.build(os.path.join(k_inputPath, texture_atlas_directory_path), os.path.join(k_outputPath, texture_atlas_directory_path))
	
	if(optionText.IsSet()):
		text_builder.build(os.path.join(k_inputPath, text_directory_path), os.path.join(k_outputPath, text_directory_path))

	if(optionOthers.IsSet()):
		misc_builder.build(k_inputPath, k_outputPath)

	print("--Done--")


def BuildMenus(in_root):
	h = HelpMenu(in_root)


def BuildGui():

	global optionTextures, optionTextureAtlases, optionModels, optionText, optionOthers

	root = Tk()
	#root.withdraw()

	k_width = 130
	k_height = 245

	w = Canvas(root)

	root.minsize(k_width, k_height)
	root.maxsize(k_width, k_height)
	root.resizable(0,0)

	# totey window that doesn't show in the taskbar, or alt-tab menu :l
	#root.attributes("-toolwindow", 1)

	w.pack()

	icon = ImageIcon(w, imageBank.icon)

	optionTextures = BuildFlag(w, "Textures")
	optionTextureAtlases = BuildFlag(w, "TextureAtlases")
	optionModels = BuildFlag(w, "Models")
	optionText = BuildFlag(w, "Text")
	optionOthers = BuildFlag(w, "Others")

	b = Button(w, text="Build", width=15, command=callback)
	b.pack(fill=X)

	return root


prevDir = os.getcwd()

os.chdir(k_projectPath)

root = BuildGui()
BuildMenus(root)
mainloop()

#os.chdir(prevDir)
