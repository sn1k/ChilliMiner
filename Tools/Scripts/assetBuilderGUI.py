from tkinter import *

import time
import imageBank



import os
import sys
import model_builder
import text_builder
import texture_atlas_builder
import texture_builder
import misc_builder

model_directory_path = "Models/"
text_directory_path = "Text/"
texture_atlas_directory_path = "TextureAtlases/"
texture_directory_path = "Textures/"


global input_path
global output_path
global optionTextures, optionModels, optionText, optionOthers



# convert a gif to base64
#import base64
#with open("test.gif", "rb") as image_file:
#    encoded_string = base64.b64encode(image_file.read())
#    print("b64: " + encoded_string)

k_version = "0.2"

class BuildFlag:

	def __init__(self, in_root, in_text):
		self.var = IntVar()
		self.c = Checkbutton(
			in_root, text=in_text,
			variable=self.var,
			command=self.cb)
		self.c.pack(anchor=W)

	def cb(self):
		pass#print( "variable is", self.var.get())


class ImageIcon:

	def __init__(self, in_root, in_b64Data):
	
		photo = PhotoImage(data=in_b64Data)
		photo = photo.subsample(3,3) # scale third width/height

		self.label = Label(in_root, image=photo)
		self.label.image = photo # prevent gc
		self.label.config(width=100)
		self.label.pack()


class HelpMenu:

	def __init__(self, in_root):
		
		menu = Menu(in_root)
		in_root.config(menu=menu)

		menu.add_command(label="Help", command=self.OnMenuHelpSelected)

	def OnMenuHelpSelected(self):
		print("pew")


def callback():
	print("button")
	
	if(optionTextures == True):
		print("textures")
		texture_atlas_builder.build(os.path.join(input_path, texture_atlas_directory_path), os.path.join(output_path, texture_atlas_directory_path))
		texture_builder.build(os.path.join(input_path, texture_directory_path), os.path.join(output_path, texture_directory_path))

	if(optionModels == True):
		print("models")
		model_builder.build_models(os.path.join(input_path, model_directory_path), os.path.join(output_path, model_directory_path))
	
	if(optionText == True):
		print("text")
		text_builder.build(os.path.join(input_path, text_directory_path), os.path.join(output_path, text_directory_path))
	
	if(optionOthers == True):
		print("others")
		misc_builder.build(input_path, output_path)



def BuildMenus(in_root):
	h = HelpMenu(in_root)


def BuildGui():
	root = Tk()
	#root.withdraw()

	k_width = 130
	k_height = 230

	w = Canvas(root)

	root.minsize(k_width, k_height)
	root.maxsize(k_width, k_height)
	root.resizable(0,0)

	# totey window that doesn't show in the taskbar, or alt-tab menu :l
	#root.attributes("-toolwindow", 1)

	w.pack()

	icon = ImageIcon(w, imageBank.icon)

	global optionTextures, optionModels, optionText, optionOthers

	optionTextures = BuildFlag(w, "Textures")
	optionModels = BuildFlag(w, "Models")
	optionText = BuildFlag(w, "Text")
	optionOthers = BuildFlag(w, "Others")

	b = Button(w, text="Build", width=15, command=callback)
	b.pack(fill=X)

	return root



if __name__ == "__main__":

	global input_path
	global output_path

	input_path = sys.argv[1]
	output_path = sys.argv[2]

	root = BuildGui()
	BuildMenus(root)
	mainloop()


