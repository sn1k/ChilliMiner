#pragma once

#include <ChilliSource/ChilliSource.h>
#include <ChilliSource/Core/Math.h>

//#include <ChilliSource/Rendering/Model/StaticModelComponent.h>

namespace AsteroidBelt
{
	struct MapAsteroidData
	{
		public:
			CS::Vector3 m_position;
			CS::Vector3 m_scale;
			f32 m_spinSpeed;
	};

	class AsteroidBeltGenerator
	{
		public:
			static std::vector<MapAsteroidData> Generate(u32 in_asteroids);
	};
}