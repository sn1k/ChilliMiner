#include <Map/AsteroidBeltGenerator.h>


using namespace CS;
using namespace AsteroidBelt;


f32 k_width = 200.f;
f32 k_depth = 200.f;
f32 k_height = 200.f;

u32 k_minDistFromCenter = 80;
u32 k_maxDistFromCenter = 200;
u32 k_maxDistFromCenterDrift = 50;

u32 k_minDistFromExisting = 5;

Vector3 k_center = Vector3(k_width*0.5f, k_height*0.5f, k_depth*0.5f);

u32 k_numCandidates = 8;

u64 k_randSeed = 1337;




std::vector<MapAsteroidData> AsteroidBeltGenerator::Generate(u32 in_asteroids)
{
	std::default_random_engine generator(k_randSeed);
	std::uniform_int_distribution<s32> distributionX(-k_width/2, k_width/2);
	std::uniform_int_distribution<s32> distributionY(-k_height/2, k_height/2);
	std::uniform_int_distribution<s32> distributionZ(-k_depth/2, k_depth/2);

	std::uniform_int_distribution<s32> distributionScale(1, 100);

	CS_LOG_VERBOSE("AsteroidBeltGenerator::Generate " + CS::ToString(in_asteroids));

	std::vector<MapAsteroidData> m_points(in_asteroids);
	
	f32 k_gridScale(0.05f);
	f32 k_modelScale(2.f);

	for (int i = 0; i < in_asteroids; ++i)
	{
		f32 scale(distributionScale(generator)*0.01f * k_modelScale);

		Vector3 randPos = Vector3(distributionX(generator)*k_gridScale, distributionY(generator)*k_gridScale, distributionZ(generator)*k_gridScale);
		//Vector3 point = GenerateAsteroid(randPos);

		//if (point == ()) : # we dist cull now, could be empty
		//	continue

		MapAsteroidData data = { randPos, Vector3(scale, scale, scale), 1.f };
		m_points.push_back(data);
	}

	return m_points;
}

/*
Vector3 GenerateAsteroid(const Vector3& in_randPos)
{
	for (int i = 0; i < k_numCandidates; ++i)
	{
		if (IsValidPlacement(in_randPos) == false)
			continue;

		if (m_points.size() == 0)
			return in_randPos;

		//f32 dist = FindClosest()
	}
	candidate = (random.random() * k_width, random.random() * k_height)

if (IsValidPlacement(candidate) == False) :
	continue

if (len(m_points) == 0) :
	return candidate

	dist = distanceSqrd(findClosest(candidate), candidate)

if (dist > bestDistance) :
	bestDistance = dist
	bestCandidate = candidate
}

bool IsValidPlacement(const Vector3& in_pos)
{
	f32 dist = (k_center - in_pos).Length; // can get lensqr instead

	if (dist < k_minDistFromCenter)
		return false;

		//maxDrift = random.random() * k_maxDistFromCenterDrift

	//if (dist > k_maxDistFromCenter + maxDrift) :
	//	return False

	return true;
}

*/