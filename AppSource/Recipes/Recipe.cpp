
#include <Recipes/Recipe.h>

#include <ChilliSource/Core/File/StorageLocation.h>
#include <ChilliSource/Core/Json/JsonUtils.h>

#include <json/json.h>
#include <map> // we map string names from json to enums


using namespace Recipes;

namespace
{
	std::map<std::string, Technologies> k_technologyNames =
	{
		{ "test", Technologies::DRILL_STRENGTH },
		{ "drill_strength", Technologies::DRILL_STRENGTH },
		{ "basics", Technologies::BASIC } };

	std::map<std::string, Hardware> k_hardwareNames =
	{
		{ "hq", Hardware::BUILDING_HQ },
		{ "hq_upgrade_1", Hardware::UPGRADE_HQ_1 } };
	
	std::map<std::string, Logistics::Item> k_materialNames =
	{
		{ "ore_a", Logistics::Item::ORE_A },
		{ "ore_b", Logistics::Item::ORE_B },
		{ "ore_c", Logistics::Item::ORE_C },
		{ "ore_d", Logistics::Item::ORE_D },
		{ "ore_e", Logistics::Item::ORE_E },
		{ "ore_f", Logistics::Item::ORE_F },
		{ "credits", Logistics::Item::CREDITS },
		{ "missile_a", Logistics::Item::MISSILE_A },
		{ "missile_b", Logistics::Item::MISSILE_A },
		{ "missile_c", Logistics::Item::MISSILE_A } };

	std::string k_nameProduct = "product";
	std::string k_nameMaterials = "materials";
	std::string k_nameTechnologies = "research";
	std::string k_nameHardware = "hardware";
}


CRecipe::CRecipe()
{
}

CRecipe::~CRecipe()
{
}

/** 
*	parses json requirements into vectors of materials, research reqs and hardware dependencies
*/
CRecipe CRecipe::Create(std::string in_filePath)
{
	CRecipe recipe;

	Json::Value root;

	if (CS::JsonUtils::ReadJson(CS::StorageLocation::k_package, in_filePath, root) == false)
		CS_LOG_ERROR("CRecipe::Create - Couldn't create json value for file " + in_filePath);

	CS_ASSERT(root[k_nameProduct].isNull() == false, "CRecipe::Create recipe doesn't have an output/product name");
		
	recipe.m_recipeProduct = root[k_nameProduct].asString();

	const Json::Value& materials = root[k_nameMaterials];
	const Json::Value& tech = root[k_nameTechnologies];
	const Json::Value& hardware = root[k_nameHardware];

	// TODO need to pull values off this, make array of object with name : val
	if (materials.isNull() == false)
		for (u32 i(0); i < materials.size(); ++i)
		{
			std::string key(materials[i].asString());
			CS_ASSERT(k_materialNames.count(key) == 1, "CRecipe::Create invalid material type '" + key + "' in " + in_filePath);
			recipe.m_requiredMaterials.push_back(k_materialNames[materials[i].asString()]);
		}
		
	if (tech.isNull() == false)
		for (u32 i(0); i < tech.size(); ++i)
		{
			std::string key(tech[i].asString());
			CS_ASSERT(k_technologyNames.count(key) == 1, "CRecipe::Create invalid technology type '" + key + "' in " + in_filePath);
			recipe.m_requiredTechnologies.push_back(k_technologyNames[tech[i].asString()]);
		}
	
	if (hardware.isNull() == false)
		for (u32 i(0); i < hardware.size(); ++i)
		{
			std::string key(hardware[i].asString());
			CS_ASSERT(k_hardwareNames.count(key) == 1, "CRecipe::Create invalid hardware type '" + key + "' in " + in_filePath);

			recipe.m_requiredHardware.push_back(k_hardwareNames[hardware[i].asString()]);
		}

	return recipe;
}
