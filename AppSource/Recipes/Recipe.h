#ifndef _RECIPES_RECIPE_H_
#define _RECIPES_RECIPE_H_

#include <string>
#include <vector>

#include <Universe/Logistics/Items.h>


namespace Recipes
{
	// TODO both of these need moved to more permanent homes
	enum class Technologies
	{
		BASIC,
		DRILL_STRENGTH
	};

	enum class Hardware
	{
		BUILDING_HQ,
		UPGRADE_HQ_1
	};

	
	class CRecipe
	{
		friend class CRecipe;

		public:
			CRecipe();
			~CRecipe();

			static CRecipe Create(std::string in_recipePath);

	private:

		std::vector<Technologies> m_requiredTechnologies;
		std::vector<Hardware> m_requiredHardware;
		std::vector<Logistics::Item> m_requiredMaterials;

		std::string m_recipeProduct;
	};
}
#endif
