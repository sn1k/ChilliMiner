
#include <Universe/Universe.h>

// debugging
#include <iostream>

#include <ChilliSource/Core/Entity.h>

#include <Universe/Buildings/BuildingSolarPlant.h>
#include <Universe/Buildings/BuildingDrill.h>

#include <Universe/Logistics/Player.h>


// statics live here so we don't lose them

StarSystemPtr CUniverse::mStarSystem = StarSystemPtr();

PlayerPtr CUniverse::m_user = PlayerPtr();

// ----


namespace DEBUG_AsteroidTools
{
	CS::EntitySPtr debugAsteroid;

	CS::EntitySPtr GetDebugAsteroid() { return debugAsteroid; }
};



namespace TOOLS
{
	u32 RandFromRange(u32 inu32StartInclusive, u32 inu32EndInclusive)
	{
		if (inu32StartInclusive == inu32EndInclusive)
			return inu32StartInclusive;

		s32 s32Rand(rand());
		s32Rand = s32Rand < 0 ? -s32Rand : s32Rand;

		return s32Rand % (inu32EndInclusive - inu32StartInclusive + 1) + inu32StartInclusive;
	}
};



// -------------------------


/* stores given asteroid if not already held */
void CStarSystem::AddAsteroid(const CS::EntitySPtr& inpAsteroid)
{
	if (maAsteroids.empty())
	{
		mCurrentAsteroidIndex = 0;
		DEBUG_AsteroidTools::debugAsteroid = inpAsteroid;
	}

	for (auto it(maAsteroids.begin()); it != maAsteroids.end(); ++it) // filter duplicates
	{
		if (*it == inpAsteroid)
			return;
	}
	maAsteroids.push_back(inpAsteroid);

	// handle index asignment better
}


/* Updates the asteroids in this star system */
void CStarSystem::Update(double indMillis)
{
}

CS::EntitySPtr CStarSystem::GetCurrentAsteroid() const
{
	return maAsteroids[mCurrentAsteroidIndex];
}

// -------------------------


CUniverse::CUniverse(u32 inu32Seed)
{
	m_user = PlayerPtr(new CPlayer());

	CreateSystem(inu32Seed);
}


/* Creates a star system */
void CUniverse::CreateSystem(u32 inu32Seed)
{
	mStarSystem = StarSystemPtr(new CStarSystem());

	srand(inu32Seed);
	u32 u32AsteroidsNum(TOOLS::RandFromRange(2, 4));

	for (u32 i(0); i < u32AsteroidsNum; ++i)
	{
		// not used yet
		CS::EntitySPtr asteroidEntity(CS::Entity::Create());
		asteroidEntity->SetName("Asteroid");

		mStarSystem->AddAsteroid(asteroidEntity);
	}
}


/* Updates the held star system */
void CUniverse::Update(double indMillis)
{
	if (mStarSystem)
		mStarSystem->Update(indMillis);
}

/** kinda debugging, need some better place to store currently viewed asteroid */
CS::EntitySPtr CUniverse::GetCurrentAsteroid()
{
	return mStarSystem->GetCurrentAsteroid();
}

PlayerPtr CUniverse::GetPlayer()
{
	return m_user;
}

