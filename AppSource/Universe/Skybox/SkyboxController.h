#ifndef _UNIVERSE_SKYBOXCONTROLLER_H_
#define _UNIVERSE_SKYBOXCONTROLLER_H_

#include <ChilliSource/Core/Entity/Component.h>

#include <Universe/ForwardDeclares.h>


class CSkyboxController : public CS::Component, public std::enable_shared_from_this<CSkyboxController>
{
public:
	CS_DECLARE_NAMEDTYPE(CSkyboxController);
	bool IsA(CS::InterfaceIDType in_interfaceId) const override;

public:
	void SetCamera(CS::EntityWPtr in_camera);

	void TrackCamera();

private:
	CS::EntityWPtr m_cameraEntity;

	CS::EventConnectionUPtr m_cameraTransformConnection;
};

#endif
