
#include <Universe/Skybox/SkyboxController.h>

#include <ChilliSource/Core/Delegate/MakeDelegate.h>
#include <ChilliSource/Core/Base/StandardMacros.h>

#include <ChilliSource/Core/Math/Quaternion.h>


#include <ChilliSource/Core/Entity.h>

// --- component handling

CS_DEFINE_NAMEDTYPE(CSkyboxController);

bool CSkyboxController::IsA(CS::InterfaceIDType in_interfaceId) const
{
	return (CSkyboxController::InterfaceID == in_interfaceId);
}

// ---


// TODO : modify rendering predicate to render our entity first

/*  */
void CSkyboxController::SetCamera(CS::EntityWPtr in_camera)
{
	m_cameraEntity = in_camera;

	CS_ASSERT(m_cameraEntity.expired() == false, "CSkyboxController::SetCamera passed entity wptr expired");
	
	m_cameraTransformConnection = m_cameraEntity.lock()->GetTransform().GetTransformChangedEvent().OpenConnection(CS::MakeDelegate(this, &CSkyboxController::TrackCamera));
}


/*  */
void CSkyboxController::TrackCamera()
{
	CS_ASSERT(GetEntity() != nullptr, "CSkyboxController::TrackCamera we're not attached to anything");

	if (m_cameraEntity.expired())
		return;

	const CS::Quaternion& camOrientation = m_cameraEntity.lock()->GetTransform().GetLocalOrientation();
	CS::Quaternion inverse = CS::Quaternion::Inverse(camOrientation);
	CS::Vector3 axis;
	f32 angle(0.f);
	inverse.ToAxisAngle(axis, angle);

	GetEntity()->GetTransform().RotateTo(axis, angle);
}
