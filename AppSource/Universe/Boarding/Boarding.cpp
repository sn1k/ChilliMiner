
#include <Universe/Boarding/Boarding.h>

#include <vector>


namespace Boarding
{
	float Crew::GetCrewStrength()
	{
		float crewStrength = 0.f;

		for (u32 i = 0; i < m_crew.size(); ++i)
		{
			crewStrength += m_crew[i].m_raidStrength;
		}

		return crewStrength;
	}


	void Raidable::ExecuteRaid(const PendingRaid in_raid)
	{
		float incomingStrength = in_raid.m_incomingCrew->GetCrewStrength();
		float ourStrength = m_currentCrew.GetCrewStrength();

		// get who wins
		bool incomingCrewWins = incomingStrength > ourStrength;

		// calc casualties
		Crew remainingCrew;
		u32 damage = 0;

		if (incomingCrewWins)
		{
			remainingCrew = *in_raid.m_incomingCrew;

			damage = int(ourStrength * 0.5f);
		}
		else
		{
			remainingCrew = m_currentCrew;
			damage = int(incomingStrength * 0.5f);
		}

		// apply damage
		for (u32 i = 0; i < damage; ++i)
		{
			if (i >= remainingCrew.m_crew.size())
				remainingCrew.m_crew.pop_back();
		}

		// send callback
		in_raid.m_onRaidComplete(remainingCrew, incomingCrewWins);
	}
}