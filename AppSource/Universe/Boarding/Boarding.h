#ifndef _UNIVERSE_BOARDING_H_
#define _UNIVERSE_BOARDING_H_


//#include <Typedefs.h>
#include <Universe/ForwardDeclares.h>

#include <Universe/Logistics/Inventory.h>

#include <functional>


namespace BuildingTypes
{
	/*enum class ENUM	{
		SOLAR_SMALL = 0,
		SOLAR_LARGE,
		HAB,
		LIFE_SUPPORT,
		DRILL_SMALL,
		DRILL_LARGE,
		STORAGE_SMALL
	};*/
};

namespace Boarding
{

	class Crewman
	{
	public:
		float m_raidStrength;
		float m_intelligence;
	};


	class Crew
	{
	public:
		int m_maxCrew;

		std::vector<Crewman> m_crew;


		float GetCrewStrength();
	};

	class PendingRaid
	{
	public:
		std::function<void(Crew, bool)> m_onRaidComplete; // remaining crew, success

		double m_arrivalTime;

		Crew* m_incomingCrew;
	};


	class Raidable
	{
	private:
		Crew m_currentCrew;

	public:
		void ExecuteRaid(const PendingRaid in_raid);
	};



	class Trader : public Raidable
	{
	private:
		std::vector<Inventory::Item> m_cargo;

	};


	class Trajectory
	{};

}
#endif
