
#include <Universe/Missiles/Missile.h>

#include <Universe/Logistics/Player.h>
#include <ChilliSource/Core/Entity.h>


// --- component handling

CS_DEFINE_NAMEDTYPE(MissileController);


bool MissileController::IsA(CS::InterfaceIDType in_interfaceId) const
{
	return (MissileController::InterfaceID == in_interfaceId);
}

// ---

MissileController::MissileController()
:m_velocity(CS::Vector3(0.f, 0.f,0.f))
{
}

MissileController::~MissileController()
{}



const u32 MissileController::GetOwnerID() const
{
	return m_owningPlayer;
}
void MissileController::SetNewOwner(u32 in_owner)
{
	m_owningPlayer = in_owner;
}

void MissileController::OnUpdate(f32 in_deltaSecs)
{
	GetEntity()->GetTransform().MoveBy(m_velocity * in_deltaSecs);
}

void MissileController::ApplyImpulse(CS::Vector3 in_impulse)
{
	m_velocity += in_impulse;
}
