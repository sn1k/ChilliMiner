#ifndef _UNIVERSE_MISSILES_MISSILE_H_
#define _UNIVERSE_MISSILES_MISSILE_H_


//#include <Typedefs.h>
#include <Universe/ForwardDeclares.h>

#include <ChilliSource/Core/Entity/Component.h>
#include <ChilliSource/Core/Math/Vector3.h>

class MissileController : public CS::Component
{
public:
	CS_DECLARE_NAMEDTYPE(Missile);
	bool IsA(CS::InterfaceIDType in_interfaceId) const override;

public:
	MissileController();
	virtual ~MissileController();

	virtual void OnUpdate(f32 in_deltaSecs);

	const u32 GetOwnerID() const;

	void SetNewOwner(u32 in_owner); // TODO pull this out into ownable

	void ApplyImpulse(CS::Vector3 in_impulse);

protected:
	CS::Vector3 m_velocity;

	u32 m_owningPlayer;
};

#endif
