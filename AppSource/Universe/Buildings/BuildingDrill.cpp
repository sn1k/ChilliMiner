
#include <Universe/Buildings/BuildingDrill.h>

#include <Universe/Asteroid/MineableController.h>
#include <Universe/Asteroid/AsteroidController.h>
#include <Universe/Logistics/Inventory.h>

#include <ChilliSource/Core/Math.h>
#include <ChilliSource/Core/Entity.h>

//#include <iostream> // debug

static Inventory::Item s_emptyItem = { Logistics::Item::ORE_A, 0.0 };


CBuildingDrill::CBuildingDrill(BuildingSizes::ENUM ineSize)
:CBuildingController(ineSize)
{
	m_powerConsumeRate = (u32(ineSize) + 1) * 5;

	switch (ineSize)
	{
		case BuildingSizes::ENUM::TINY:
			m_drillPower = 1.f;
			break;
		case BuildingSizes::ENUM::SMALL:
			m_drillPower = 2.f;
			break;
		case BuildingSizes::ENUM::MEDIUM:
			m_drillPower = 4.f;
			break;
		case BuildingSizes::ENUM::LARGE:
			m_drillPower = 16.f;
			break;
	}

	// TODO TEMP REMOVE
	//m_drillPower *= 16.f;// 128.f;


	// TODO rand seed
	u32 k_randSeed(1337);
	m_generator = std::default_random_engine(k_randSeed);
}

CBuildingDrill::~CBuildingDrill()
{
}


// registers this drill onto the parent asteroid entity's mineable controller
void CBuildingDrill::OnBuilt()
{
	auto parentEntity = GetEntity()->GetParent();
	auto mineableController = parentEntity->GetComponent<MineableController>();

	mineableController->RegisterDrill(shared_from_this());
}

//----------------------------------------------------
/// Determine what we mined this tick and how much
/// Called by the mineable controller we registered to
///
/// @author snik
//----------------------------------------------------
Inventory::Item CBuildingDrill::Drill(const InventoryPtr in_oresRemaining, f32 in_deltaSecs)
{
	if (!m_powered)
		return s_emptyItem;

	const Inventory::InventoryMap ores(in_oresRemaining->GetInventory());
	
	// TODO roulette this for greater chances of common ore
	std::uniform_int_distribution<u32> oreSelectionDistribution(0, ores.size()-1);

	u32 selectedIndex(oreSelectionDistribution(m_generator));

	// TODO take into account ore resistance
	f32 retrievedAmount(m_drillPower * in_deltaSecs);

	Inventory::Item drilledOre((Logistics::Item)(selectedIndex + (u32)Logistics::Item::ORE_A), retrievedAmount);

	return drilledOre;
}

void CBuildingDrill::OnUpdate(f32 in_deltaSecs)
{
	CBuildingController::ConsumePower(in_deltaSecs);
}