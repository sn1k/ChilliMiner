
#include <Universe/Buildings/BuildingStorageSmall.h>

//#include <Universe/Asteroid/AsteroidController.h>

//#include <iostream> // debug

// --- component handling

CS_DEFINE_NAMEDTYPE(CBuildingStorageSmall);

bool CBuildingStorageSmall::IsA(CS::InterfaceIDType in_interfaceId) const
{
	return (CBuildingStorageSmall::InterfaceID == in_interfaceId || CBuildingController::InterfaceID == in_interfaceId);
}

// ---

CBuildingStorageSmall::CBuildingStorageSmall(BuildingSizes::ENUM ineSize)
:CBuildingController(ineSize)
{
}


CBuildingStorageSmall::~CBuildingStorageSmall()
{
}


void CBuildingStorageSmall::OnUpdate(f32 in_deltaSecs)
{
	CBuildingController::ConsumePower(in_deltaSecs); // parent handles power issues

	if (!m_powered)
		return;

	//std::cout << "3: " << madStored[u32(Ores::ENUM::THREE)] << ", 4: " << madStored[u32(Ores::ENUM::FOUR)] << "     " << '\r';//std::endl;
}