
#include <Universe/Buildings/BuildingSolarPlant.h>
#include <Universe/Buildings/BuildingHQController.h>

#include <iostream>

// --- component handling

CS_DEFINE_NAMEDTYPE(CBuildingSolarPlant);

bool CBuildingSolarPlant::IsA(CS::InterfaceIDType in_interfaceId) const
{
	return (CBuildingSolarPlant::InterfaceID == in_interfaceId || CBuildingController::InterfaceID == in_interfaceId);
}

// ---

CBuildingSolarPlant::CBuildingSolarPlant( BuildingSizes::ENUM ineSize)
: CBuildingController(ineSize)
, mdPowerGenerationPerSec(20.0)
{
	m_powerConsumeRate = 0.0;// (u32(ineSize) + 1) * 5;

	m_powerConsumeRate = (u32(ineSize) + 1) * 3;
}


CBuildingSolarPlant::~CBuildingSolarPlant()
{
}


/** generates power and distributes it to attached buildings */
void CBuildingSolarPlant::OnUpdate(f32 in_deltaSecs)
{
	double generatedPower(in_deltaSecs * mdPowerGenerationPerSec);

	if (m_hq.expired())
		return;

	m_hq.lock()->StorePower(generatedPower);
	/*if (!mwpparentasteroid.expired())
	{
		mwpparentasteroid.lock()->storepower(generatedpower);
	}*/

	ConsumePower(in_deltaSecs);
}



