#ifndef _UNIVERSE_BUILDINGS_BUILDINGSTORAGE_H_
#define _UNIVERSE_BUILDINGS_BUILDINGSTORAGE_H_

#include <Universe/Buildings/BuildingController.h>

//#include <Universe/Universe.h>

//#include <memory>


class CBuildingStorageSmall : public CBuildingController
{
public:
	CS_DECLARE_NAMEDTYPE(CBuildingStorageSmall);
	bool IsA(CS::InterfaceIDType in_interfaceId) const override;

public:
	CBuildingStorageSmall(BuildingSizes::ENUM ineSize);
	~CBuildingStorageSmall();

	void OnUpdate(f32 in_deltaSecs);

};

#endif
