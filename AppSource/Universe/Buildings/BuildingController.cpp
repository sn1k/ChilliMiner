
#include <Universe/Buildings/BuildingController.h>

#include <Universe/Buildings/BuildingHQController.h>
#include <Universe/Logistics/Player.h>

#ifdef SNIK_DEBUG
	#include <assert.h>
#endif

// --- component handling

CS_DEFINE_NAMEDTYPE(CBuildingController);


bool CBuildingController::IsA(CS::InterfaceIDType in_interfaceId) const
{
	return (CBuildingController::InterfaceID == in_interfaceId);
}

// ---

CBuildingController::CBuildingController()
:CBuildingController(BuildingSizes::ENUM::TINY)
{
}

CBuildingController::CBuildingController(BuildingSizes::ENUM ineSize)
: m_buildingSize(ineSize)
, m_powerStored(0.0)
, m_powerCapacity(60.0)
, m_powered(false)
, m_powerConsumeRate(1.0)
{}


CBuildingController::~CBuildingController()
{}



const BuildingHQWPtr& CBuildingController::GetHQ() const
{
	return m_hq;
}

const u32 CBuildingController::GetOwnerID() const
{
	return m_owningPlayer;
}


void CBuildingController::SetHQ(BuildingHQWPtr in_hq)
{
	m_hq = in_hq;

	m_hq.lock()->RegisterBuilding(this); // let hq know about us for power management
}

void CBuildingController::SetNewOwner(u32 in_owner)
{
	m_owningPlayer = in_owner;
}

void CBuildingController::OnUpdate(f32 in_deltaSecs)
{
}

void CBuildingController::ConsumePower(f32 in_deltaSecs)
{
	double powerDelta(m_powerConsumeRate * in_deltaSecs);

	// determine power state
	m_powered = (m_powerStored >= powerDelta);

	m_powerStored -= powerDelta;

	if (m_powerStored < 0.f)
		m_powerStored = 0.f;
}

bool CBuildingController::IsPowered() const
{
	return m_powered;
}


/** receive power, clamp to capacity
//	@return excess power after storage
*/
double CBuildingController::DrawPower(double in_powerAmount)
{
	m_powerStored += in_powerAmount;

	if (m_powerStored > m_powerCapacity)
	{
		double diff = m_powerCapacity - m_powerStored;
		m_powerStored = m_powerCapacity;
		return in_powerAmount - diff;
	}

	return 0.0;
}


double CBuildingController::GetRequiredPower() const 
{
	return m_powerConsumeRate;
}

void CBuildingController::OnBuilt()
{}
