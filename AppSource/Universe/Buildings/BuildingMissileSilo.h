#ifndef _UNIVERSE_BUILDINGS_BUILDINGMISSILESILO_H_
#define _UNIVERSE_BUILDINGS_BUILDINGMISSILESILO_H_

#include <Universe/Buildings/BuildingController.h>

#include <memory>
#include <vector>


class CBuildingMissileSilo : public CBuildingController//, public std::enable_shared_from_this<CBuildingMissileSilo>
{
public:
	CS_DECLARE_NAMEDTYPE(CBuildingMissileSilo);
	bool IsA(CS::InterfaceIDType in_interfaceId) const override;
public:
	CBuildingMissileSilo();
	~CBuildingMissileSilo();

	void OnUpdate(f32 in_deltaSecs) override;

	void OnBuilt() override;

	void Launch(CS::Vector3 in_target);

private:
	void BuildMissileIfPossible();

private:
	InventoryPtr m_missiles;

	AsteroidPtr m_asteroidController;
};

#endif
