#ifndef _UNIVERSE_BUILDINGS_BUILDINGSOLARPLANT_H_
#define _UNIVERSE_BUILDINGS_BUILDINGSOLARPLANT_H_

#include <Universe/Buildings/BuildingController.h>

#include <vector>


class CBuildingSolarPlant : public CBuildingController
{
public:
	CS_DECLARE_NAMEDTYPE(CBuildingSolarPlant);
	bool IsA(CS::InterfaceIDType in_interfaceId) const override;

public:
	CBuildingSolarPlant(BuildingSizes::ENUM ineSize);
	~CBuildingSolarPlant();

	void OnUpdate(f32 in_deltaSecs) override;

private:
	double mdPowerGenerationPerSec;
};

#endif
