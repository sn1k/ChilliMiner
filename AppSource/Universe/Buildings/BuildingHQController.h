#ifndef _UNIVERSE_BUILDINGS_BUILDINGHQCONTROLLER_H_
#define _UNIVERSE_BUILDINGS_BUILDINGHQCONTROLLER_H_

#include <Universe/Buildings/BuildingController.h>

#include <Universe/ForwardDeclares.h>
#include <Universe/Logistics/Inventory.h>


class CBuildingHQController : public CBuildingController
{
public:
	CS_DECLARE_NAMEDTYPE(CBuildingHQController);
	bool IsA(CS::InterfaceIDType in_interfaceId) const override;

public:
	CBuildingHQController();
	~CBuildingHQController();
	
	void StorePower(double inPower);
	void StoreOre(const Inventory::Item& in_ore);

	InventoryPtr GetExtractedOreInventory() const;

	void RegisterBuilding(CBuildingController* in_building);

private:
	void OnUpdate(f32 in_timeSinceLastUpdate) override;

private:
	std::vector<ShipPtr>				m_shipsInOrbit;
	std::vector<CBuildingController*>	m_buildings;

	InventoryPtr m_extractedOre; // don't like this here, really should use some kind of storage

	// power generation
	double m_generatedPowerStored;
	double m_generatedPowerStorageCapacity;
};

#endif
