#ifndef _UNIVERSE_BUILDINGS_BUILDINGDRILL_H_
#define _UNIVERSE_BUILDINGS_BUILDINGDRILL_H_

#include <Universe/Buildings/BuildingController.h>

#include <Universe/Logistics/Inventory.h>

#include <memory>
#include <random>
#include <vector>


class CBuildingDrill : public CBuildingController, public std::enable_shared_from_this<CBuildingDrill>
{
public:
	CBuildingDrill(BuildingSizes::ENUM ineSize);
	~CBuildingDrill();

	void OnUpdate(f32 in_deltaSecs) override;

	Inventory::Item Drill(const InventoryPtr in_oresRemaining, f32 in_deltaSecs);

	void OnBuilt() override;

private:
	std::default_random_engine m_generator;

	f32 m_drillPower;
};

#endif
