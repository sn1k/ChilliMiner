
#include <Universe/Buildings/BuildingHQController.h>

#include <Universe/Buildings/BuildingSolarPlant.h>
#include <Universe/Buildings/BuildingDrill.h>


#include <algorithm> // std::min

// --- component handling

CS_DEFINE_NAMEDTYPE(CBuildingHQController);

bool CBuildingHQController::IsA(CS::InterfaceIDType in_interfaceId) const
{
	return (CBuildingHQController::InterfaceID == in_interfaceId || CBuildingController::InterfaceID == in_interfaceId);
}

// ---


namespace TOOLS
{
	u32 RandFromRange(u32 inu32StartInclusive, u32 inu32EndInclusive);
};


CBuildingHQController::CBuildingHQController()
: m_generatedPowerStored(0.0)
, m_generatedPowerStorageCapacity(200.f)
{
	m_extractedOre = std::make_shared<CInventory>();

	m_powerConsumeRate = 0.0;
}


CBuildingHQController::~CBuildingHQController()
{
}

/* Updates all buildable areas on this asteroid */
void CBuildingHQController::OnUpdate(f32 in_timeSinceLastUpdate)
{
	for (auto it(m_buildings.begin()); it != m_buildings.end(); ++it)
	{
		if (!(*it))
			continue;

		// handle power generation
		double powerRequired((*it)->GetRequiredPower() * in_timeSinceLastUpdate);

		// we have enough to power
		if (m_generatedPowerStored > 0.0)
		{
			// send power to building, what it needs, or what we have remaining if not enough
			double excess( (*it)->DrawPower(std::min(powerRequired, m_generatedPowerStored)) );
			m_generatedPowerStored -= powerRequired - excess;
		}
	}
}

/** takes power from a solar panel etc and stores up to capacity */
void CBuildingHQController::StorePower(double inPower)
{
	m_generatedPowerStored += inPower;

	if (m_generatedPowerStored > m_generatedPowerStorageCapacity)
		m_generatedPowerStored = m_generatedPowerStorageCapacity;
}

/** hook-in to push drilled ore into ore available on asteroid */
void CBuildingHQController::StoreOre(const Inventory::Item& in_ore)
{
	m_extractedOre->AddItem(in_ore);
}

InventoryPtr CBuildingHQController::GetExtractedOreInventory() const
{
	return m_extractedOre;
}

void CBuildingHQController::RegisterBuilding(CBuildingController* in_building)
{
	for (auto it(m_buildings.begin()); it != m_buildings.end(); ++it)
	{
		if(in_building == *it)
			return;
	}

	m_buildings.push_back(in_building);
}

