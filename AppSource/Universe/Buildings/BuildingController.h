#ifndef _UNIVERSE_BUILDINGS_BUILDING_H_
#define _UNIVERSE_BUILDINGS_BUILDING_H_


//#include <Typedefs.h>
#include <Universe/ForwardDeclares.h>

#include <ChilliSource/Core/Entity/Component.h>

//#include <Universe/Logistics/Inventory.h>

namespace BuildingSizes
{
	enum class ENUM	{ TINY = 0, SMALL = 1, MEDIUM = 2, LARGE = 3 };
};


class CBuildingController : public CS::Component// public CInventory
{
public:
	CS_DECLARE_NAMEDTYPE(CBuildingController);
	bool IsA(CS::InterfaceIDType in_interfaceId) const override;

public:
	CBuildingController();
	CBuildingController(BuildingSizes::ENUM);
	virtual ~CBuildingController();

	virtual void OnUpdate(f32 in_deltaSecs);

	double GetRequiredPower() const;

	virtual void OnBuilt();

	double DrawPower(double indPowerAmount);

	const u32 GetOwnerID() const;
	const BuildingHQWPtr& GetHQ() const;

	void SetHQ(BuildingHQWPtr in_hq);
	void SetNewOwner(u32 in_ownerID);

	bool IsPowered() const;

protected:
	void ConsumePower(f32 in_deltaSecs);

protected:
	BuildingSizes::ENUM m_buildingSize;

	double m_powerCapacity;

	double m_powerConsumeRate;

	bool m_powered;

protected:
	BuildingHQWPtr m_hq;

	double m_powerStored;

	u32 m_owningPlayer;
};

#endif
