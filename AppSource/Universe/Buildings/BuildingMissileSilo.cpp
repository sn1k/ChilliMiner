
#include <Universe/Buildings/BuildingMissileSilo.h>
#include <Universe/Buildings/BuildingHQController.h>

#include <Universe/Factories/FactoryOrbitalObjects.h>

#include <Universe/Asteroid/AsteroidController.h>
#include <Universe/Logistics/Inventory.h>

#include <Universe/Missiles/Missile.h>

#include <ChilliSource/Core/Entity.h>

#include <unordered_map>

#include <ChilliSource/Core/Base/Logging.h>


// TEMP MISSILE RECIPIES
namespace Missiles
{
	typedef std::unordered_map<Logistics::Item, Inventory::InventoryMap> RecipeMap;

	static RecipeMap createRecipes()
	{
		RecipeMap m;
		m[Logistics::Item::MISSILE_A] = Inventory::InventoryMap { { Logistics::Item::ORE_A, 5 }, { Logistics::Item::ORE_B, 2 } };
		m[Logistics::Item::MISSILE_B] = Inventory::InventoryMap { { Logistics::Item::ORE_A, 8 }, { Logistics::Item::ORE_C, 1 } };
		return m;
	}

	static const RecipeMap k_recipies = createRecipes();
};

// --- component handling

CS_DEFINE_NAMEDTYPE(CBuildingMissileSilo);

bool CBuildingMissileSilo::IsA(CS::InterfaceIDType in_interfaceId) const
{
	return (CBuildingMissileSilo::InterfaceID == in_interfaceId || CBuildingController::InterfaceID == in_interfaceId);
}

// ---


CBuildingMissileSilo::CBuildingMissileSilo()
:CBuildingController(BuildingSizes::ENUM::SMALL)
,m_missiles(new CInventory())
{
	
}

CBuildingMissileSilo::~CBuildingMissileSilo()
{
}


//
void CBuildingMissileSilo::OnBuilt()
{
	// TODO assert
	auto parentEntity( GetEntity()->GetParent() );
	m_asteroidController = parentEntity->GetComponent<CAsteroidController>();

	Launch(GetEntity()->GetTransform().GetWorldPosition() + CS::Vector3(1.f, 0.f, 1.f));
}

void CBuildingMissileSilo::OnUpdate(f32 in_deltaSecs)
{
	CBuildingController::ConsumePower(in_deltaSecs); // parent handles power issues

	if (!m_powered)
		return;


	// if not already building
	// see if we have enough to build something
	// pick something
	// else 
	// continue building something if we have enough extracted ore
}

void CBuildingMissileSilo::BuildMissileIfPossible()
{
	BuildingHQWPtr hq = m_asteroidController->GetHQForPlayer(GetOwnerID());

	CS_ASSERT(hq.expired() == false, "CBuildingMissileSilo::BuildMissileIfPossible HQ is expired");

	if (hq.expired())
		return;

	// run recipies, choose first available
	//for (u32 i(0); i < Missiles::k_recipies.size(); ++i)
	InventoryPtr extractedOre = hq.lock()->GetExtractedOreInventory();

	for (Missiles::RecipeMap::const_iterator it(Missiles::k_recipies.begin()); it != Missiles::k_recipies.end(); ++it)
	{
		Inventory::InventoryMap costMap(it->second);

		// see if we can afford this recipe and build the missile
		if (extractedOre->CanAfford(costMap))
		{
			//  remove cost
			for(std::pair<Logistics::Item, f64> item : costMap)
			{
				extractedOre->Remove(item.first, item.second);
			}
			// build missile 
			

			break;
		}
	}
}

// TODO comment
// param in_target (worldspace)
void CBuildingMissileSilo::Launch(CS::Vector3 in_target)
{
	// orbital factory adds to scene for us
	CS::EntitySPtr missile(FactoryOrbitalObjects::CreateMissile(GetOwnerID()) );

	// get direction ( center asteroid to target + center asteroid to us )
	CS::Vector3 asteroidToTarget(in_target - m_asteroidController->GetEntity()->GetTransform().GetWorldPosition());

	std::shared_ptr<MissileController> controller = missile->GetComponent<MissileController>();

	CS::Vector3 normalDir(asteroidToTarget + GetEntity()->GetTransform().GetLocalPosition());
	normalDir.Normalise();

	// move to our position
	missile->GetTransform().SetPosition(GetEntity()->GetTransform().GetWorldPosition());

	const float k_launchStrength(2.f);

	controller->ApplyImpulse(normalDir * k_launchStrength);
}