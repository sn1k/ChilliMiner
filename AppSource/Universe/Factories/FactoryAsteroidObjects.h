#ifndef _UNIVERSE_FACTORIES_FACTORYASTEROIDOBJECTS_H_
#define _UNIVERSE_FACTORIES_FACTORYASTEROIDOBJECTS_H_

#include <ChilliSource/ChilliSource.h>
#include <Universe/Buildings/BuildingController.h>

namespace BuildingTypes
{
	enum class ENUM
	{
		k_none,
		k_HQ,
		k_generator,
		k_drill,
		k_missileSilo
	};
}

namespace IsoSphere
{
	struct NoiseDescription;
}

// ----------------------------------------------
/// Class creates asteroids and anything 
/// that can be built on or nearby them
///
/// @author snik
// ----------------------------------------------
class FactoryAsteroidObjects
{
public:
	FactoryAsteroidObjects() = delete;
	~FactoryAsteroidObjects() = delete;

	static CS::EntitySPtr Create(BuildingTypes::ENUM in_type, u32 in_owningPlayer, BuildingHQWPtr in_hq = BuildingHQWPtr());

	static CS::EntitySPtr CreateAsteroid(std::vector<CS::Vector3>& out_verts, const CS::MaterialCSPtr& in_material, IsoSphere::NoiseDescription in_noiseDesc, f32 in_debugScale = 1.f);
	static CS::EntitySPtr CreateSkybox(CS::EntitySPtr in_camera);

	static CS::EntitySPtr CreateHQ(u32 in_owningPlayer);
	static CS::EntitySPtr CreateGenerator(u32 in_owningPlayer, BuildingHQWPtr in_hq);
	static CS::EntitySPtr CreateDrill(BuildingSizes::ENUM in_size, u32 in_owningPlayer, BuildingHQWPtr in_hq);
	static CS::EntitySPtr CreateMissileSilo(BuildingSizes::ENUM in_size, u32 in_owningPlayer, BuildingHQWPtr in_hq);

	static CS::EntitySPtr CreateGhost();

	static CS::EntitySPtr CreateLine(CS::Vector3 in_start, CS::Vector3 in_end, float in_width);
	static CS::EntitySPtr CreateCube(const CS::MaterialCSPtr& in_material);
	static CS::EntitySPtr CreatePlane(const CS::MaterialCSPtr& in_material);
	
	static void DestroyFactory();

	static CS::MaterialSPtr CreateCustomAsteroidMaterial(const std::string& uniqueId) noexcept;
	

private:

	static CS::EntitySPtr CreateSimple(CS::StorageLocation in_modelLoc, const std::string& in_modelPath, const CS::MaterialCSPtr& in_material);
	static CS::EntitySPtr CreateSimple(CS::StorageLocation in_modelLoc, const std::string& in_modelPath, CS::StorageLocation in_materialLoc, const std::string& in_materialPath);
};

#endif