#include <Universe/Factories/FactoryAsteroidObjects.h>

#include <ChilliSource/Core/Base.h>
#include <ChilliSource/Core/Entity.h>
#include <ChilliSource/Core/Resource.h>
#include <ChilliSource/Core/Math/MathUtils.h>

#include <ChilliSource/Rendering/Base.h>
#include <ChilliSource/Rendering/Material.h>
#include <ChilliSource/Rendering/Model.h>

#include <Universe/ForwardDeclares.h>
#include <Universe/Logistics/Inventory.h>
#include <Universe/Buildings/BuildingHQController.h>
#include <Universe/Buildings/BuildingSolarPlant.h>
#include <Universe/Buildings/BuildingDrill.h>
#include <Universe/Buildings/BuildingMissileSilo.h>

#include <Universe/Asteroid/AsteroidController.h>
#include <Universe/Asteroid/MineableController.h>
#include <Universe/Skybox/SkyboxController.h>

#include <Math/Sphere/Iconosphere.hpp>


//#include <ChilliSource/Core/Resource/ResourcePool.h>
//#include <ChilliSource/Rendering/Material/Material.h>


namespace
{
	const float k_globalBuildingScale = 0.05f;
}


CS::EntitySPtr FactoryAsteroidObjects::Create(BuildingTypes::ENUM in_type, u32 in_owningPlayer, BuildingHQWPtr in_hq)
{
	switch (in_type)
	{
		case BuildingTypes::ENUM::k_HQ:
			return CreateHQ(in_owningPlayer);
		case BuildingTypes::ENUM::k_generator:
			return CreateGenerator(in_owningPlayer, in_hq);
		case BuildingTypes::ENUM::k_drill:
			return CreateDrill(BuildingSizes::ENUM::TINY, in_owningPlayer, in_hq);
		case BuildingTypes::ENUM::k_missileSilo:
			return CreateMissileSilo(BuildingSizes::ENUM::TINY, in_owningPlayer, in_hq);
		default:
			return CS::EntitySPtr(nullptr);
	}
}

CS::EntitySPtr FactoryAsteroidObjects::CreateAsteroid(std::vector<CS::Vector3>& out_verts, const CS::MaterialCSPtr& in_material, IsoSphere::NoiseDescription in_noiseDesc, f32 in_debugScale)
{
	using namespace CS;

	//auto asteroid = IsoSphere::IcoSphereCreator::CreateSphere(in_material, 7.f, out_verts, 3, IsoSphere::NoiseDescription(6, 2.0, 0.4));
	auto asteroid = IsoSphere::IcoSphereCreator::CreateSphere(in_material, 7.f, out_verts, 3, in_noiseDesc, in_debugScale);
	//auto asteroid = IsoSphere::IcoSphereCreator::CreateSphere(in_material, 7.f, out_verts, 3, IsoSphere::NoiseDescription(0, 1.0, 0.4), in_debugScale);
		
	asteroid->AddComponent(std::make_shared<CAsteroidController>());
	asteroid->AddComponent(InventoryPtr(new CInventory()));
	asteroid->AddComponent(MineableControllerPtr(new MineableController()));

	asteroid->SetName("Asteroid");

	return asteroid;
}

CS::EntitySPtr FactoryAsteroidObjects::CreateSkybox(CS::EntitySPtr in_camera)
{
	using namespace CS;
	auto skybox = FactoryAsteroidObjects::CreateSimple(StorageLocation::k_package, "Models/skyboxSphere.csmodel", StorageLocation::k_package, "Materials/skyboxSphere.csmaterial");

	skybox->AddComponent(std::make_shared<CSkyboxController>());
	skybox->GetComponent<CSkyboxController>()->SetCamera(in_camera);

	skybox->GetTransform().ScaleTo(10.f);
	skybox->GetTransform().RotateYBy(-CS::MathUtils::k_pi*0.5f);

	skybox->SetName("Skybox");

	return skybox;
}

CS::EntitySPtr FactoryAsteroidObjects::CreateHQ(u32 in_owningPlayer)
{
	using namespace CS;

	CS::EntitySPtr building = FactoryAsteroidObjects::CreateSimple(StorageLocation::k_package, "Models/hq.csmodel", StorageLocation::k_package, "Materials/hq.csmaterial");
	building->SetName("HQ");

	BuildingHQPtr hq = std::make_shared<CBuildingHQController>();
	hq->SetNewOwner(in_owningPlayer);
	
	building->AddComponent(hq);
	building->GetTransform().ScaleTo(k_globalBuildingScale);

	return building;
}

// create small drill
CS::EntitySPtr FactoryAsteroidObjects::CreateDrill(BuildingSizes::ENUM in_size, u32 in_owningPlayer, BuildingHQWPtr in_hq)
{
	using namespace CS;

	CS::EntitySPtr building = FactoryAsteroidObjects::CreateSimple(StorageLocation::k_package, "Models/hq.csmodel", StorageLocation::k_package, "Materials/hq.csmaterial");
	BuildingDrillPtr drill = BuildingDrillPtr(new CBuildingDrill(in_size));
	building->AddComponent(drill);
	drill->SetNewOwner(in_owningPlayer);
	drill->SetHQ(in_hq);

	building->SetName("Drill size " + CS::ToString(u32(in_size)));
	building->GetTransform().ScaleTo(k_globalBuildingScale);
	return building;
}


CS::EntitySPtr FactoryAsteroidObjects::CreateGenerator(u32 in_owningPlayer, BuildingHQWPtr in_hq)
{
	using namespace CS;
	CS::EntitySPtr building = FactoryAsteroidObjects::CreateSimple(StorageLocation::k_package, "Models/generator.csmodel", StorageLocation::k_package, "Materials/generator.csmaterial");
	building->SetName("Generator Small");

	BuildingPtr generatorController(new CBuildingSolarPlant(BuildingSizes::ENUM::SMALL));
	generatorController->SetHQ(in_hq);
	generatorController->SetNewOwner(in_owningPlayer);
	building->AddComponent(generatorController);
	building->GetTransform().ScaleTo(k_globalBuildingScale);

	return building;
}


CS::EntitySPtr FactoryAsteroidObjects::CreateMissileSilo(BuildingSizes::ENUM in_size, u32 in_owningPlayer, BuildingHQWPtr in_hq)
{
	using namespace CS;
	CS::EntitySPtr building = FactoryAsteroidObjects::CreateSimple(StorageLocation::k_package, "Models/generator.csmodel", StorageLocation::k_package, "Materials/generator.csmaterial");
	building->SetName("Missile Silo Small");

	BuildingPtr siloController(new CBuildingMissileSilo());// BuildingSizes::ENUM::SMALL));
	siloController->SetHQ(in_hq);
	siloController->SetNewOwner(in_owningPlayer);
	building->AddComponent(siloController);
	building->GetTransform().ScaleTo(k_globalBuildingScale);

	return building;
}

CS::EntitySPtr FactoryAsteroidObjects::CreateGhost()
{
	using namespace CS;
	CS::EntitySPtr ghost(FactoryAsteroidObjects::CreateSimple(StorageLocation::k_package, "Models/construction.csmodel", StorageLocation::k_package, "Materials/ghostBuilding.csmaterial"));
	ghost->GetTransform().ScaleTo(k_globalBuildingScale);
	return ghost;
}

CS::EntitySPtr FactoryAsteroidObjects::CreateCube(const CS::MaterialCSPtr& in_material)
{
	using namespace CS;
	return FactoryAsteroidObjects::CreateSimple(StorageLocation::k_package, "Models/cube.csmodel", in_material);// "Materials/generator.csmaterial");
}

CS::EntitySPtr FactoryAsteroidObjects::CreatePlane(const CS::MaterialCSPtr& in_material)
{
	using namespace CS;
	return FactoryAsteroidObjects::CreateSimple(StorageLocation::k_package, "Models/plane.csmodel", in_material);// "Materials/generator.csmaterial");
}

/** TODO */
CS::EntitySPtr FactoryAsteroidObjects::CreateLine(CS::Vector3 in_start, CS::Vector3 in_end, float in_width)
{
	using namespace CS;
	auto line = FactoryAsteroidObjects::CreateSimple(StorageLocation::k_package, "Models/lineMesh.csmodel", StorageLocation::k_package, "Materials/line.csmaterial"); // switch material if not showing

	float len((in_end - in_start).Length());
	CS::Vector3 pos((in_end - in_start) * 0.5f + in_start);
	line->GetTransform().ScaleTo(CS::Vector3(in_width, in_width, len));
	line->GetTransform().SetLookAt(pos, in_end, CS::Vector3::k_unitPositiveY);
	line->GetTransform().SetPosition(pos);
	return line;
}


CS::EntitySPtr FactoryAsteroidObjects::CreateSimple(CS::StorageLocation in_modelLoc, const std::string& in_modelPath, const CS::MaterialCSPtr& in_material)
{
	CS::ResourcePool* resourcePool(CS::Application::Get()->GetResourcePool());
	CS::ModelCSPtr model(resourcePool->LoadResource<CS::Model>(in_modelLoc, in_modelPath));

	//Create the mesh component

	CS::StaticModelComponentSPtr modelComponent = std::make_shared<CS::StaticModelComponent>(model, in_material);

	//create the model entity and add the model component
	CS::EntitySPtr modelEntity = CS::Entity::Create();
	modelEntity->AddComponent(modelComponent);

	return modelEntity;
}

CS::EntitySPtr FactoryAsteroidObjects::CreateSimple(CS::StorageLocation in_modelLoc, const std::string& in_modelPath, CS::StorageLocation in_materialLoc, const std::string& in_materialPath)
{
	CS::ResourcePool* resourcePool(CS::Application::Get()->GetResourcePool());
	CS::MaterialCSPtr material(resourcePool->LoadResource<CS::Material>(in_materialLoc, in_materialPath));

	return CreateSimple(in_modelLoc, in_modelPath, material);
}

void FactoryAsteroidObjects::DestroyFactory()
{
	IsoSphere::IcoSphereCreator::Destroy();
}
