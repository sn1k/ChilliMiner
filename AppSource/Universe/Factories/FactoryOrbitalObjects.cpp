#include <Universe/Factories/FactoryOrbitalObjects.h>

#include <ChilliSource/Core/Base.h>
#include <ChilliSource/Core/Entity.h>
#include <ChilliSource/Rendering/Material.h>
#include <ChilliSource/Core/Resource.h>
#include <ChilliSource/Core/Scene.h>
#include <ChilliSource/Core/Math/MathUtils.h>

#include <ChilliSource/Rendering/Base.h>
#include <ChilliSource/Rendering/Model.h>

#include <Universe/ForwardDeclares.h>
//#include <Universe/Logistics/Inventory.h>
#include <Universe/Buildings/BuildingHQController.h>

#include <Universe/Missiles/Missile.h>

#include <Universe/Asteroid/AsteroidController.h>
//#include <Universe/Asteroid/MineableController.h>
//#include <Universe/Skybox/SkyboxController.h>

// static initialisations
CS::Scene* FactoryOrbitalObjects::s_scene = nullptr;


void FactoryOrbitalObjects::SetScene(CS::Scene* const in_scene)//const CS::SceneSPtr& in_scene)
{
	s_scene = in_scene;
}

CS::EntitySPtr FactoryOrbitalObjects::Create(OrbitalTypes::ENUM in_type, u32 in_owningPlayer)
{
	switch (in_type)
	{
		case OrbitalTypes::ENUM::missile:
			return CreateMissile(in_owningPlayer);
		default:
			return CS::EntitySPtr(nullptr);
	}
}

// Pushes entity onto the scene
CS::EntitySPtr FactoryOrbitalObjects::CreateMissile(u32 in_owningPlayer)
{
	using namespace CS;

	CS::EntitySPtr missile = FactoryOrbitalObjects::CreateSimple(StorageLocation::k_package, "Models/hq.csmodel", StorageLocation::k_package, "Materials/hq.csmaterial");
	missile->SetName("MissileBasic");

	std::shared_ptr<MissileController> controller(new MissileController());
	controller->SetNewOwner(in_owningPlayer);
	missile->AddComponent(controller);

	s_scene->Add(missile);

	return missile;
}


CS::EntitySPtr FactoryOrbitalObjects::CreateSimple(CS::StorageLocation in_modelLoc, const std::string& in_modelPath, CS::StorageLocation in_materialLoc, const std::string& in_materialPath)
{
	CS::ResourcePool* resourcePool = CS::Application::Get()->GetResourcePool();
	CS::ModelCSPtr model = resourcePool->LoadResource<CS::Model>(in_modelLoc, in_modelPath);
	CS::MaterialCSPtr material = resourcePool->LoadResource<CS::Material>(in_materialLoc, in_materialPath);

	//Create the mesh component
	CS::StaticModelComponentSPtr modelComponent = std::make_shared<CS::StaticModelComponent>(model, material);
	
	//create the model entity and add the model component
	CS::EntitySPtr modelEntity = CS::Entity::Create();
	modelEntity->AddComponent(modelComponent);

	return modelEntity;
}
