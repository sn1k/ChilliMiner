#ifndef _UNIVERSE_FACTORIES_FACTORYORBITALOBJECTS_H_
#define _UNIVERSE_FACTORIES_FACTORYORBITALOBJECTS_H_

#include <ChilliSource/ChilliSource.h>
#include <ChilliSource/Core/ForwardDeclarations.h>

namespace OrbitalTypes
{
	enum class ENUM
	{
		missile
	};
}

// ----------------------------------------------
/// Class creates orbitals like ships and missiles
///
/// @author snik
// ----------------------------------------------
class FactoryOrbitalObjects
{
public:
	FactoryOrbitalObjects() = delete;
	~FactoryOrbitalObjects() = delete;

	static void SetScene(CS::Scene* const in_scene);

	static CS::EntitySPtr Create(OrbitalTypes::ENUM in_type, u32 in_owningPlayer);
	
	static CS::EntitySPtr CreateMissile(u32 in_owningPlayer);

private:
	static CS::EntitySPtr CreateSimple(CS::StorageLocation in_modelLoc, const std::string& in_modelPath, CS::StorageLocation in_materialLoc, const std::string& in_materialPath);

private:
	static CS::Scene* s_scene;
};

#endif