#ifndef _UNIVERSE_LOGISTICS_INVENTORY_H_
#define _UNIVERSE_LOGISTICS_INVENTORY_H_

#include <ChilliSource/Core/Entity/Component.h>

//#include <ChilliSource/ChilliSource.h>

#include <vector>
#include <unordered_map>
#include <functional>

#include <Universe/Logistics/Items.h>


namespace Inventory
{
	typedef std::pair<Logistics::Item, f64> Item;

	typedef std::unordered_map<Logistics::Item, f64> InventoryMap;
};


class CInventory : public CS::Component
{
public:
	CS_DECLARE_NAMEDTYPE(CInventory);
	bool IsA(CS::InterfaceIDType in_interfaceId) const override;

public:
	CInventory();

	void Set(Logistics::Item in_type, f64 in_amount);

	void Add(Logistics::Item in_type, f64 in_amount);
	void AddItem(const Inventory::Item& in_item);

	f64 Remove(Logistics::Item in_type, f64 in_amount);

	void Clear();

	Inventory::Item RemoveItem(Logistics::Item in_type, f64 in_amount);
	Inventory::Item RemoveItem(const Inventory::Item& in_item);

	bool CanAfford(Logistics::Item in_type, f64 in_amount) const;
	bool CanAfford(Inventory::InventoryMap in_costs) const;

	const Inventory::InventoryMap GetInventory() const;

	bool AddInventoryChangedListener(const std::function<void(Logistics::Item, f64)>& in_listener);

private:
	void NotifyListenersOfInventoryChange(Logistics::Item in_type, f64 in_amount);

	// TODO add capacity handling, mirrored map for optional caps?

protected:
	Inventory::InventoryMap m_inventoryMap;

	f32 m_maxMass;

private:
	std::list<std::function<void(Logistics::Item, f64)>> m_listeners;
};

#endif
