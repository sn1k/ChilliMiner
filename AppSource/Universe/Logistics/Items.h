#ifndef _UNIVERSE_LOGISTICS_ITEMS_H_
#define _UNIVERSE_LOGISTICS_ITEMS_H_

namespace Logistics
{
	enum class Item
	{
		CREDITS,
		ENERGY,
		ORE_A,
		ORE_B,
		ORE_C,
		ORE_D,
		ORE_E,
		ORE_F,
		MISSILE_A,
		MISSILE_B,
		MISSILE_C
	};
}

#endif