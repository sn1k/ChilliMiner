
#include <Universe/Logistics/Inventory.h>

// --- component handling

CS_DEFINE_NAMEDTYPE(CInventory);

bool CInventory::IsA(CS::InterfaceIDType in_interfaceId) const
{
	return (CInventory::InterfaceID == in_interfaceId);
}

// ---


CInventory::CInventory()
: m_maxMass(1000.f)
, m_listeners(2)
{
}

void CInventory::Set(Logistics::Item in_type, f64 in_amount)
{
	m_inventoryMap[in_type] = in_amount;

	NotifyListenersOfInventoryChange(in_type, in_amount); // TODO better define that this is an add or set for listener
}


void CInventory::Add(Logistics::Item in_type, f64 in_amount)
{
	m_inventoryMap[in_type] += in_amount;

	NotifyListenersOfInventoryChange(in_type, in_amount);
}

void CInventory::AddItem(const Inventory::Item& in_item)
{
	Add(in_item.first, in_item.second);
}

void CInventory::NotifyListenersOfInventoryChange(Logistics::Item in_type, f64 in_amount)
{
	if (in_amount == 0.f)
		return;

	// notify listeners
	for (auto it(m_listeners.begin()); it != m_listeners.end(); ++it)
	{
		if (*it != nullptr)
			(*it)(in_type, in_amount);
	}
}

void CInventory::Clear()
{
	m_inventoryMap.clear();
}

f64 CInventory::Remove(Logistics::Item in_type, f64 in_amount)
{
	if (m_inventoryMap[in_type] >= in_amount)
	{
		m_inventoryMap[in_type] -= in_amount;

		NotifyListenersOfInventoryChange(in_type, -in_amount);

		return in_amount;
	}

	// not enough, return whatever we have
	f64 storedAmount(m_inventoryMap[in_type]);
	m_inventoryMap[in_type] = 0;

	NotifyListenersOfInventoryChange(in_type, -storedAmount);

	return storedAmount;
}

Inventory::Item CInventory::RemoveItem(const Inventory::Item& in_item)
{
	f64 amount = Remove(in_item.first, in_item.second);
	return Inventory::Item(in_item.first, amount);
}

Inventory::Item CInventory::RemoveItem(Logistics::Item in_type, f64 in_amount)
{
	f64 amount = Remove(in_type, in_amount);
	return Inventory::Item(in_type, amount);
}

const Inventory::InventoryMap CInventory::GetInventory() const
{
	return m_inventoryMap;
}

bool CInventory::CanAfford(Logistics::Item in_type, f64 in_amount) const
{
	return m_inventoryMap.at(in_type) >= in_amount;
}

bool CInventory::CanAfford(Inventory::InventoryMap in_costs) const
{
	for (auto item : in_costs)
	{
		if (m_inventoryMap.at(item.first) < item.second)
			return false;
	}
	
	return true;
}

bool CInventory::AddInventoryChangedListener(const std::function<void(Logistics::Item, f64)>& in_listener)
{
	// we'd normally do a Contains check here, but std::function has no comparison operator
	m_listeners.push_back(in_listener);
	return true;
}
