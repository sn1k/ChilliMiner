
#include <Universe/Logistics/Player.h>

#include <Universe/Logistics/Inventory.h>

static u32 s_playerCount(0);

CPlayer::CPlayer()
: mstrName("Unnamed")
, m_inventory(new CInventory())
{
	m_inventory->Add(Logistics::Item::CREDITS, 1500);
	m_id = s_playerCount++;
}

InventoryPtr CPlayer::GetInventory() const
{
	return m_inventory;
}


CPlayer::~CPlayer()
{
}
