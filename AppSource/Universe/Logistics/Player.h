#ifndef _UNIVERSE_LOGISTICS_PLAYER_H_
#define _UNIVERSE_LOGISTICS_PLAYER_H_

#include <string>
//#include <Typedefs.h>

#include <Universe/ForwardDeclares.h>
#include <ChilliSource/ChilliSource.h>

class CPlayer
{
public:
	CPlayer();
	~CPlayer();

	InventoryPtr GetInventory() const;
	inline const std::string& GetName() const { return mstrName; }
	inline u32 GetID() const { return m_id; }

private:
	InventoryPtr m_inventory;
	std::string mstrName;

	u32 m_id;
};

#endif
