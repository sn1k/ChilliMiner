
#pragma once

#include <memory>

class CUniverse;
class CAsteroidController;
class CBuildableArea;
class CShip;
class CStarSystem;

class CStorageController;
class CInventory;
class CPlayer;

typedef std::shared_ptr<CUniverse>				UniversePtr;
typedef std::shared_ptr<CAsteroidController>	AsteroidPtr;
typedef std::weak_ptr<CAsteroidController>		AsteroidWeakPtr;
typedef std::shared_ptr<CShip>					ShipPtr;
typedef std::shared_ptr<CStarSystem>			StarSystemPtr;

typedef std::shared_ptr<CInventory>			InventoryPtr;
typedef std::shared_ptr<CPlayer>			PlayerPtr;
typedef std::weak_ptr<CPlayer>				PlayerWPtr;

// buildings
class CBuildingController;
class CBuildingSolarPlant;
class CBuildingDrill;
class CBuildingHQController;

typedef std::shared_ptr<CBuildingController>	BuildingPtr;
typedef std::shared_ptr<CBuildingSolarPlant>	BuildingSolarPlantPtr;
typedef std::shared_ptr<CBuildingDrill>			BuildingDrillPtr;
typedef std::shared_ptr<CBuildingHQController>	BuildingHQPtr;
typedef std::weak_ptr<CBuildingHQController>	BuildingHQWPtr;

// orbitals

class MissileController; // temp

typedef std::shared_ptr<MissileController>	MissilePtr;
