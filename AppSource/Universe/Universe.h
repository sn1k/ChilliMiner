#ifndef _UNIVERSE_H_
#define _UNIVERSE_H_

#include <ChilliSource/ChilliSource.h>

#include <Vector>

#include <Universe/ForwardDeclares.h>
//#include <Typedefs.h>

#include <Universe/Buildings/BuildingController.h>


namespace TOOLS
{
	//u32 RandFromRange(u32 inu32StartInclusive, u32 inu32EndInclusive);
};


namespace Universe
{
	class CStarSystem;
	class CAsteroid;
	class CBuildableArea;
}


class CShip
{};



// -- TODO wanting these classes to encapsulate serialisable data only, to be passed to
// -- the asteroid/map states for display and modification

class CStarSystem
{
public:
	void AddAsteroid(const CS::EntitySPtr& inpAsteroid);

	void Update(double indMillis);

	CS::EntitySPtr GetCurrentAsteroid() const;
private:
	std::vector<CS::EntitySPtr> maAsteroids;
	u32 mCurrentAsteroidIndex;

	friend class CUniverse; // debug
};

class CUniverse
{
public:
	CUniverse(u32 inu32Seed);
	void Update(double indMillis);

	static CS::EntitySPtr GetCurrentAsteroid();

	static PlayerPtr GetPlayer();

private:
	void CreateSystem(u32 inu32Seed);

private:
	static StarSystemPtr mStarSystem;

	static PlayerPtr m_user;
};

#endif
