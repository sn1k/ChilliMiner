#ifndef _UNIVERSE_ASTEROID_MINEABLECONTROLLER_H_
#define _UNIVERSE_ASTEROID_MINEABLECONTROLLER_H_

#include <memory>
#include <map>

#include <ChilliSource/Core/Entity/Component.h>
#include <ChilliSource/Core/Math/Vector2.h>

#include <Universe/ForwardDeclares.h>

#include <Universe/Logistics/Inventory.h>
#include <Universe/Logistics/Items.h>

//----------------------------------------------------
/// Handles registration of drills on this asteroid
/// and manages extraction of any ore into their inventories.
///
/// @author snik
//----------------------------------------------------
class MineableController final : public CS::Component
{
public:
	CS_DECLARE_NAMEDTYPE(MineableController);
	bool IsA(CS::InterfaceIDType in_interfaceId) const override;

public:
	MineableController();
	~MineableController();
	
	void RegisterDrill(const BuildingDrillPtr& in_drill);
	void DeRegisterDrill(const BuildingDrillPtr& in_drill);

	void SetRemainingOreData(const Logistics::Item in_oreType, const u32 in_oreQuantity, std::unique_ptr<u8[]> in_asteroidOreDensityData, const u32 in_asteroidOreTextureSize);
	void SetRemainingOreTextureDimensions(const CS::Integer2& in_asteroidOverayTextureDimensions);

	bool UpdateRemainingOreTexture(const Inventory::Item in_oreType, const CS::Vector3& in_position);

	void RebuildRemainingOreTexture();
	

public:
	static const CS::Integer2 GetRemainingOreTextureDimensions();
	static const size_t GetRemainingTextureSize();

private:
	void OnUpdate(f32 in_timeSinceLastUpdate) override;

	void OnAddedToEntity() override;

	CS::MaterialCSPtr CreateMinedTextureMaterial(const CS::Colour& in_colour, const CS::TextureCSPtr& in_texture0, const CS::TextureCSPtr& in_texture1);

	CS::Vector2 GetAsteroidTextureCoord(const CS::Vector3& in_point);

private:
	static const u32 k_oreTextureWidth;
	static const u32 k_oreTextureHeight;

private:
	std::vector<BuildingDrillPtr> m_registeredDrills;

	std::map<Logistics::Item, std::unique_ptr<u8[]>> m_oreDensityTextures;
//	std::map<Logistics::Item, std::unique_ptr<u8[]>> m_originalOreDensityTextures;

	/// amounts per texel
	std::map<Logistics::Item, f32*> m_oreAmountsGrid;
	std::map<Logistics::Item, f32> m_maxAmountPerTexel;

	InventoryPtr m_remainingOre;
	//InventoryPtr m_originalOreAmounts;

	InventoryPtr m_drilledOrePendingDisplayUpdate;

	AsteroidPtr m_asteroidController;

	f32 m_timeSinceLastUpdate;
	f32 m_timeSinceLastMinedTextureUpdate;

	const f32 k_updateTime = 0.5f;
	const f32 k_updateMinedTextureTime = 5.f;

	// texture stuff
	CS::Integer2 m_asteroidOreTextureDimensions;
	u32 m_asteroidOreTextureSize;

	
	std::unique_ptr<u8[]> m_remainingOreTexture; // 2d map of what's been mined (can make 3d later)

	bool m_oreTextureDirty;
};

// TODO move to forward declares
typedef std::shared_ptr<MineableController>	MineableControllerPtr;

#endif
