
#include <Universe/Asteroid/AsteroidController.h>

#include <Universe/Buildings/BuildingHQController.h>

#include <ChilliSource/Core/Entity.h>


// --- component handling

CS_DEFINE_NAMEDTYPE(CAsteroidController);



bool CAsteroidController::IsA(CS::InterfaceIDType in_interfaceId) const
{
	return (CAsteroidController::InterfaceID == in_interfaceId);
}

// ---

CAsteroidController::CAsteroidController()
:m_orbitHeight(2.f)
{
}

BuildingHQWPtr CAsteroidController::GetHQForPlayer(u32 in_playerID)
{
	for (auto pair : m_registeredHQs)
	{
		if (pair.first == in_playerID)
			return pair.second;
	}

	return BuildingHQWPtr();
}

void CAsteroidController::RegisterHQ(CS::EntitySPtr in_hqEntity)
{
	CS_ASSERT(in_hqEntity != nullptr, "CAsteroidController::RegisterHQ passed player wp has expired");
	
	BuildingHQPtr hq(in_hqEntity->GetComponent<CBuildingHQController>());

	CS_ASSERT(hq != nullptr, "CAsteroidController::RegisterHQ no hq on passed entity!");

	m_registeredHQs[hq->GetOwnerID()] = hq;
}

void CAsteroidController::SetOrbitalHeight(f32 in_height)
{
	m_orbitHeight = in_height;
}


