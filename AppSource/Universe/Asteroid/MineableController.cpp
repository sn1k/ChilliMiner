
#include <Universe/Asteroid/MineableController.h>

#include <Universe/Logistics/Player.h>

#include <Universe/Buildings/BuildingDrill.h>
#include <Universe/Buildings/BuildingHQController.h>

#include <Universe/Asteroid/AsteroidController.h>

#include <ChilliSource/Core/Entity.h>

#include <ChilliSource/Rendering/Material/Material.h>
#include <ChilliSource/Rendering/Material/MaterialFactory.h>

#include <ChilliSource/Rendering/Model/StaticModelComponent.h>

#include <ChilliSource/Rendering/Shader/Shader.h>

#include <ChilliSource/Rendering/Texture/Texture.h>
#include <ChilliSource/Rendering/Texture/TextureDesc.h>


#include <ChilliSource/Core/Base/Application.h>
#include <ChilliSource/Core/Math.h>
#include <ChilliSource/Core/Resource.h>


const u32 MineableController::k_oreTextureWidth = 32;
const u32 MineableController::k_oreTextureHeight = 32; //26 fails, 27 works :l


// --- component handling

CS_DEFINE_NAMEDTYPE(MineableController);

bool MineableController::IsA(CS::InterfaceIDType in_interfaceId) const
{
	return (MineableController::InterfaceID == in_interfaceId);
}

// ---

MineableController::MineableController()
:m_remainingOreTexture(new u8[MineableController::k_oreTextureWidth*MineableController::k_oreTextureHeight * 3])
, m_asteroidOreTextureSize(0)
, m_oreTextureDirty(false)
{
}

MineableController::~MineableController()
{
	for(auto& i : m_oreAmountsGrid)
		delete[] i.second;
}

void MineableController::OnAddedToEntity()
{
	// TODO assert all this
	// also dangerous, we're ast.controller has an instance component inv, and entity has one too for un-mined
	m_remainingOre = GetEntity()->GetComponent<CInventory>();

	/*if (m_originalOreAmounts == nullptr)
	{
		m_originalOreAmounts = InventoryPtr(new CInventory());
		GetEntity()->AddComponent(m_originalOreAmounts);
	}*/

	m_drilledOrePendingDisplayUpdate = InventoryPtr(new CInventory());

	m_asteroidController = GetEntity()->GetComponent<CAsteroidController>();

	m_timeSinceLastUpdate = 0.f;
	m_timeSinceLastMinedTextureUpdate = 0.f;
}

void MineableController::RegisterDrill(const BuildingDrillPtr& in_drill)
{
	// ignore duplicates
	for(std::vector<BuildingDrillPtr>::iterator it(m_registeredDrills.begin()); it != m_registeredDrills.end(); ++it)
	{
		if (*it == in_drill)
			return;
	}

	m_registeredDrills.push_back(in_drill);
}

void MineableController::DeRegisterDrill(const BuildingDrillPtr& in_drill)
{
	for (std::vector<BuildingDrillPtr>::iterator it(m_registeredDrills.begin()); it != m_registeredDrills.end(); ++it)
	{
		if (*it == in_drill)
		{
			m_registeredDrills.erase(it);
			return;
		}
	}
}

//----------------------------------------------------
/// Runs drill extraction, and sends them, from asteroid
/// inventory, whatever they pulled out
///
/// @author snik
//----------------------------------------------------
void MineableController::OnUpdate(f32 in_timeSinceLastUpdate)
{
	m_timeSinceLastUpdate += in_timeSinceLastUpdate;

	if (m_timeSinceLastUpdate < k_updateTime)
		return;
	
	m_timeSinceLastUpdate -= k_updateTime;

	bool expiredHQs(false);

	for (u32 i(0); i < m_registeredDrills.size(); ++i)
	{
		// get drill owner
		BuildingHQWPtr hq = m_asteroidController->GetHQForPlayer(m_registeredDrills[i]->GetOwnerID());

		if (hq.expired())
		{
			expiredHQs = true;
			continue; // don't extract, but ensure other drills still do
		}

		if (m_registeredDrills[i]->IsPowered() == false)
			continue;

		// drill interacts with asteroid controller to pull ore out of it
		Inventory::Item drilledOre(m_registeredDrills[i]->Drill(m_remainingOre, m_timeSinceLastUpdate));

		Logistics::Item oreType(drilledOre.first);

		
		// update the texture that holds the remaining ore
		bool successfullyDrilled(UpdateRemainingOreTexture(drilledOre, m_registeredDrills[i]->GetEntity()->GetTransform().GetWorldPosition()));

		if (successfullyDrilled == false)
			continue;

		m_remainingOre->RemoveItem(drilledOre); // take from asteroid

		hq.lock()->StoreOre(drilledOre); // move to extracted pool

		m_drilledOrePendingDisplayUpdate->AddItem(drilledOre);
		
		CS_LOG_VERBOSE("extracted " + CS::ToString((u32)drilledOre.first) + ", amount: " + CS::ToString(drilledOre.second));
	}

	// if we encountered an expired hq, run the list backwards and deregister anything that's gone
	if (expiredHQs && m_registeredDrills.size() > 0)
	{
		for (s32 i(m_registeredDrills.size() - 1); i >= 0; --i)
		{
			BuildingHQWPtr hq = m_asteroidController->GetHQForPlayer(m_registeredDrills[i]->GetOwnerID());

			if (hq.expired())
				DeRegisterDrill(m_registeredDrills[i]);
		}
	}

	// handle rebuilding of the mined texture so we can update the overlay if needed
	if (m_registeredDrills.size() && m_drilledOrePendingDisplayUpdate->GetInventory().size())
	{
		m_timeSinceLastMinedTextureUpdate += in_timeSinceLastUpdate;

		if (m_timeSinceLastMinedTextureUpdate < k_updateMinedTextureTime)
			return;

		m_timeSinceLastMinedTextureUpdate -= k_updateMinedTextureTime;
		
		RebuildRemainingOreTexture();

		// TODO must be a more performant way of doing this
		m_drilledOrePendingDisplayUpdate->Clear();
	}
}


void MineableController::SetRemainingOreTextureDimensions(const CS::Integer2& in_asteroidOverayTextureDimensions)
{
	m_asteroidOreTextureDimensions = in_asteroidOverayTextureDimensions;
}


bool MineableController::UpdateRemainingOreTexture(const Inventory::Item in_oreInfo, const CS::Vector3& in_position)
{
	//if (in_ore.second == 0)
	//	return;

	CS::Vector2 uv(GetAsteroidTextureCoord(in_position));

	const u32 width(MineableController::k_oreTextureWidth);
	const u32 height(MineableController::k_oreTextureHeight);

	u32 pixelX(uv.x * width);
	u32 pixelY(uv.y * height);

	u32 pixelStart((pixelY * width + pixelX) * 3);
	u32 gridStart(pixelY * width + pixelX);

	Logistics::Item oreType(in_oreInfo.first);
	f64 drilledAmount(in_oreInfo.second);

	//f32 currentOre = m_remainingOre->GetInventory().at(in_oreInfo);
	//f32 originalOre = m_originalOreAmounts->GetInventory().at(in_oreInfo);
	//f32 remainingRatio = currentOre / originalOre;

	// determine if we have anything to extract

	// if so pull what we have requested value wise
	// reduce m_remainingOre


	// get normalised remaining ore
	// TODO also want to originally display available/max_potential, then multiply by the extracted factor

	// we're pulling multiple ore types, so the ratio keeps changing, want to mix colour for them if possible, or some better way of describing
	if (oreType != Logistics::Item::ORE_A)
		return false;

	if (m_oreAmountsGrid[oreType][gridStart] == 0)
		return false;

	m_oreAmountsGrid[oreType][gridStart] -= drilledAmount;

	if (m_oreAmountsGrid[oreType][gridStart] < 0.f)
		m_oreAmountsGrid[oreType][gridStart] = 0.f;
	
	f32 t = m_oreAmountsGrid[oreType][gridStart];
	f32 tot = m_maxAmountPerTexel[oreType];
	u8 amountRemaining = (m_oreAmountsGrid[oreType][gridStart]/ m_maxAmountPerTexel[oreType]) * 255;
	
	CS_LOG_VERBOSE("m_oreAmountsGrid " + CS::ToString(t) + ", m_maxAmountPerTexel: " + CS::ToString(tot));
	
	//todo hookup the ore extraction to the amounts texture so they stop when exhausted

	//u8 val = m_originalOreDensityTextures[in_oreInfo][pixelStart + 0] * remainingRatio;
	/*u8 original1 = m_originalOreDensityTextures[in_oreInfo][pixelStart + 1];
	u8 original2 = m_originalOreDensityTextures[in_oreInfo][pixelStart + 2];*/

	// change colour of texel
	m_oreDensityTextures[oreType][pixelStart + 0] = amountRemaining; // r
	m_oreDensityTextures[oreType][pixelStart + 1] = amountRemaining; // g
	m_oreDensityTextures[oreType][pixelStart + 2] = amountRemaining; // b

	m_oreTextureDirty = true;

	return true;
}


CS::MaterialCSPtr MineableController::CreateMinedTextureMaterial(const CS::Colour& in_colour, const CS::TextureCSPtr& in_texture0, const CS::TextureCSPtr& in_texture1)
{
	static u64 poolPixie(0);

	auto resPool = CS::Application::Get()->GetResourcePool();
	auto materialName = "_AsteroidOreMaterial" + CS::ToString(poolPixie++);
	auto material = resPool->GetResource<CS::Material>(materialName);

	if (material == nullptr)
	{
		// can prob shift this to component lifetime
		auto customShader = resPool->LoadResource<CS::Shader>(CS::StorageLocation::k_package, "Shaders/Mining.csshader");

		auto materialFactory = CS::Application::Get()->GetSystem<CS::MaterialFactory>();

		//const CS::Colour specularColour(0.5f, 0.5f, 0.5f, 1.0f);
		//const f32 shininess = 10.0f;

		auto materialModifiable = materialFactory->CreateCustom(materialName);
		materialModifiable->AddTexture(in_texture0);
		materialModifiable->AddTexture(in_texture1);

		materialModifiable->SetEmissive(CS::Colour::k_black);
		//materialModifiable->SetAmbient(ambientColour);
		materialModifiable->SetDiffuse(in_colour);
		//materialModifiable->SetSpecular(Colour(specularColour.r, specularColour.g, specularColour.b, shininess));

		materialModifiable->SetShadingType(CS::MaterialShadingType::k_custom);
		//materialModifiable->SetCustomShader(CS::VertexFormat::k_staticMesh, customShader);
		materialModifiable->AddCustomShader(customShader, CS::RenderPasses::k_base); // ????

		materialModifiable->SetLoadState(CS::Resource::LoadState::k_loaded);

		return materialModifiable;
	}

	return material;
}


CS::Vector2 MineableController::GetAsteroidTextureCoord(const CS::Vector3& in_point)
{
	CS::Vector3 selectedPoint(in_point);

	// need to rotate to take into account asteroid's transform
	CS::Transform& t(GetEntity()->GetTransform());
	CS::Quaternion q(t.GetLocalOrientation());
	q.Inverse();

	selectedPoint -= t.GetLocalPosition();
	selectedPoint.Rotate(q);
	selectedPoint += t.GetLocalPosition();

	f32 tu = 0.5f + atan2(selectedPoint.x, selectedPoint.z) / (2 * CS::MathUtils::k_pi);
	f32 radius = selectedPoint.Length(); // - asteroid position
	f32 tv = 0.5f - asin(selectedPoint.y / radius) / CS::MathUtils::k_pi;

	// TODO - need to -1 instead?
	if (tu > 1.f)
		tu = 1.f;
	if (tv > 1.f)
		tv = 1.f;

	return CS::Vector2(tu, tv);
}


void MineableController::SetRemainingOreData(const Logistics::Item in_oreType, const u32 in_oreQuantity, std::unique_ptr<u8[]> in_asteroidOreDensityData, const u32 in_asteroidOreTextureSize)
{
	m_asteroidOreTextureSize = in_asteroidOreTextureSize;

	m_oreDensityTextures[in_oreType] = std::move(in_asteroidOreDensityData);

	// build float textures to hold the actual ore values per texel, that correspond to colour values in render textures
	if (m_oreAmountsGrid.find(in_oreType) == m_oreAmountsGrid.end())
		m_oreAmountsGrid[in_oreType] = (new f32[k_oreTextureWidth*k_oreTextureHeight]);

	//todo // need to use the passed quantity of ore to work out a normalisation algo so we can assign actual float values to the amountTexture

		// sum all byte values, divide by oreQuant? gets you value per byte
	//in_oreQuantity

	const auto& densityData = m_oreDensityTextures[in_oreType];
	f64 cumulativeValue(0.0);

	// sum the values in the texture so we can normalise them for ore value based on total ore passed in
	for (u32 i(0); i < k_oreTextureWidth * k_oreTextureHeight; ++i)
	{		
		cumulativeValue += densityData[i];
	}

	const f32 conversionValue(in_oreQuantity / cumulativeValue);

	auto& valueTexture(m_oreAmountsGrid[in_oreType]);

	// set ore value using density amount and conversion val
	for (u32 i(0); i < k_oreTextureWidth * k_oreTextureHeight; ++i)
	{
		valueTexture[i] = (densityData[i]/255.f) * conversionValue;
	}
	
	m_remainingOre->Set(in_oreType, in_oreQuantity); // this will be decreased on drill() by a mirrored amount on density texture
//	m_originalOreAmounts->Set(in_oreInfo, in_oreQuantity);

	// store what value we have if texel is 255
	m_maxAmountPerTexel[in_oreType] = conversionValue;
	m_oreTextureDirty = true;
}

void MineableController::RebuildRemainingOreTexture()
{
	if (m_oreTextureDirty == false)
		return;

	// build texture based off what we have remaining
	//m_remainingOre

	const size_t texSize(GetRemainingTextureSize());
	u8* oreTexturecopy(new u8[texSize]);
	
	// TODO add/swap through multiple applied textures

	memcpy_s(oreTexturecopy, texSize, m_oreDensityTextures[Logistics::Item::ORE_A].get(), texSize);

	// TODO remove - temp make part of the overlay red
	/*for (u32 i(0); i < (64 * 64) / 2; i += 3)
	{
		oreTexturecopy[i] = 255;
	}*/

	CS::Texture::DataUPtr dataPtr(oreTexturecopy);

	auto resPool = CS::Application::Get()->GetResourcePool();

	// TODO find a way around recreating textures that isn't retarded
	static u64 poolPixie(0);

	//auto resource = resPool->GetResource<CS::Texture>("temptex");

	//if (resource == nullptr)
	{
		CS::TextureSPtr minedTexture = resPool->CreateResource<CS::Texture>("_AsteroidOreTexture " + CS::ToString(poolPixie++));
		CS::TextureDesc desc(GetRemainingOreTextureDimensions(), CS::ImageFormat::k_RGB888, CS::ImageCompression::k_none, true);
		desc.SetFilterMode(CS::TextureFilterMode::k_nearest);
		desc.SetMipmappingEnabled(false);

		minedTexture->Build(std::move(dataPtr), (u32)GetRemainingTextureSize(), desc);
		minedTexture->SetLoadState(CS::Resource::LoadState::k_loaded);

		auto modelComponent(GetEntity()->GetComponent<CS::StaticModelComponent>());

		auto mat = CreateMinedTextureMaterial(CS::Colour::k_white, modelComponent->GetMaterialForMesh(0)->GetTexture(0), minedTexture); //minedTexture);

		modelComponent->SetMaterial(mat);
	}

	m_oreTextureDirty = false;
}


const size_t MineableController::GetRemainingTextureSize()
{
	return MineableController::k_oreTextureWidth * MineableController::k_oreTextureHeight * 3;
}

const CS::Integer2 MineableController::GetRemainingOreTextureDimensions()
{
	return CS::Integer2(MineableController::k_oreTextureWidth, MineableController::k_oreTextureHeight);
}
