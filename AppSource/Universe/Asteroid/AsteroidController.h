#ifndef _UNIVERSE_ASTEROIDCONTROLLER_H_
#define _UNIVERSE_ASTEROIDCONTROLLER_H_

#include <ChilliSource/Core/Entity/Component.h>

#include <unordered_map>

#include <Universe/ForwardDeclares.h>
#include <Universe/Factories/FactoryAsteroidObjects.h>


class CAsteroidController : public CS::Component, public std::enable_shared_from_this<CAsteroidController>
{
public:
	CS_DECLARE_NAMEDTYPE(CAsteroidController);
	bool IsA(CS::InterfaceIDType in_interfaceId) const override;
	
public:
	CAsteroidController();
	//~CAsteroidController();

	BuildingHQWPtr GetHQForPlayer(u32 in_playerID);

	void RegisterHQ(CS::EntitySPtr in_hqEntity);
	
	void SetOrbitalHeight(f32 in_height);
		
private:
	std::vector<ShipPtr>			mShipsInOrbit;
	std::vector<BuildingPtr>		mBuildings;

	std::unordered_map<u32, BuildingHQWPtr> m_registeredHQs;
	
	f32 m_orbitHeight;
	
	// meta
	bool m_buildable;
};

#endif
