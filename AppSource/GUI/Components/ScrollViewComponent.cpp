//
//  ScrollViewComponent.cpp
//
//  Created by Hugh McLaughlin on 14/01/2015.
//  Copyright (c) 2015 Tag Games Ltd. All rights reserved.
//

#include <GUI/Components/ScrollViewComponent.h>

#include <ChilliSource/Core/Container/Property/PropertyTypes.h>
#include <ChilliSource/Core/Math.h>

#include <ChilliSource/UI/Base.h>
#include <ChilliSource/UI/ProgressBar.h>
#include <ChilliSource/UI/Slider.h>

namespace Miner
{
    namespace
    {
        const char k_canScrollVerticalKey[] = "CanScrollVertical";
        const char k_canScrollHorizontalKey[] = "CanScrollHorizontal";
        const char k_showProgressBars[] = "ShowProgressBars";
        const char k_scrollVelocity[] = "ScrollVelocity";
        
        const std::vector<CS::PropertyMap::PropertyDesc> k_propertyDescs =
        {
            { CS::PropertyTypes::Bool(), k_canScrollVerticalKey },
            { CS::PropertyTypes::Bool(), k_canScrollHorizontalKey },
            { CS::PropertyTypes::Bool(), k_showProgressBars },
            { CS::PropertyTypes::Vector2(), k_scrollVelocity }
        };
        
        const f32 k_scrollDeceleration = 0.9f;
        const f32 k_scrollStoppingPoint = 0.05f;
        const f32 k_overBounceMax = 60.0f;
        const f32 k_bounceBackVelocity = 15.0f;
        const f32 k_bounceBackAcceleration = 1.15f;
    }
    
    CS_DEFINE_NAMEDTYPE(ScrollViewComponent);
    
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    const std::vector<CS::PropertyMap::PropertyDesc>& ScrollViewComponent::GetPropertyDescs()
    {
        return  k_propertyDescs;
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    bool ScrollViewComponent::IsA(CS::InterfaceIDType in_interfaceId) const
    {
        return (CS::UIComponent::InterfaceID == in_interfaceId || ScrollViewComponent::InterfaceID == in_interfaceId);
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    ScrollViewComponent::ScrollViewComponent(const std::string& in_componentName, const CS::PropertyMap& in_properties)
    :CS::UIComponent(in_componentName)
    {
        RegisterProperty<bool>(CS::PropertyTypes::Bool(), k_canScrollVerticalKey, CS::MakeDelegate(this, &ScrollViewComponent::IsVerticalScrollingEnabled), CS::MakeDelegate(this, &ScrollViewComponent::SetVerticalScrollingEnabled));
        RegisterProperty<bool>(CS::PropertyTypes::Bool(), k_canScrollHorizontalKey, CS::MakeDelegate(this, &ScrollViewComponent::IsHorizontalScrollingEnabled), CS::MakeDelegate(this, &ScrollViewComponent::SetHorizontalScrollingEnabled));
        RegisterProperty<bool>(CS::PropertyTypes::Bool(), k_showProgressBars, CS::MakeDelegate(this, &ScrollViewComponent::IsProgressBarsEnabled), CS::MakeDelegate(this, &ScrollViewComponent::SetProgressBarsEnabled));
        RegisterProperty<CS::Vector2>(CS::PropertyTypes::Vector2(), k_scrollVelocity, CS::MakeDelegate(this, &ScrollViewComponent::GetScrollVelocity), CS::MakeDelegate(this, &ScrollViewComponent::SetScrollVeclocity));
        
        ApplyRegisteredProperties(in_properties);
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    void ScrollViewComponent::OnInit()
    {
        m_contentWidget = GetWidget()->GetInternalWidgetRecursive("ContentWidget");
        m_horizontalProgressBar = GetWidget()->GetInternalWidgetRecursive("HorizontalScrollBar");
        m_verticalProgressBar = GetWidget()->GetInternalWidgetRecursive("VerticalScrollBar");
        
        SetProgressBarsEnabled(m_enableProgressBars);
        
        //Setup input events
        m_widgetTouchConnections.push_back(GetWidget()->GetPressedInsideEvent().OpenConnection(CS::MakeDelegate(this, &ScrollViewComponent::OnPressedInside)));
        m_widgetTouchConnections.push_back(GetWidget()->GetReleasedInsideEvent().OpenConnection(CS::MakeDelegate(this, &ScrollViewComponent::OnReleaseInside)));
        m_widgetTouchConnections.push_back(GetWidget()->GetReleasedOutsideEvent().OpenConnection(CS::MakeDelegate(this, &ScrollViewComponent::OnReleaseOutside)));
        m_widgetTouchConnections.push_back(GetWidget()->GetDraggedInsideEvent().OpenConnection(CS::MakeDelegate(this, &ScrollViewComponent::OnDraggedInside)));
        m_widgetTouchConnections.push_back(GetWidget()->GetDraggedOutsideEvent().OpenConnection(CS::MakeDelegate(this, &ScrollViewComponent::OnDraggedOutside)));
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    void ScrollViewComponent::OnAddedToCanvas()
    {
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    void ScrollViewComponent::OnUpdate(f32 in_deltaTime)
    {
        if(m_scrollVelocity.x != 0.0f || m_scrollVelocity.y != 0.0f)
        {
            auto clampedAxis = UpdateContentPosition(m_scrollVelocity);
            
            if(m_isDragging && !m_touchMoved)
            {
                m_scrollVelocity = CS::Vector2::k_zero;
            }
            else if(m_isDragging == false && (m_isOverscrolling.first || m_isOverscrolling.second))
            {
                const auto& contentPosition = m_contentWidget->GetLocalAbsolutePosition();
                const auto& scrollInRange = IsInScrollRange(contentPosition);
                
                //Check X
                if(m_isOverscrolling.first && scrollInRange.first)
                {
                    m_isOverscrolling.first = false;
                    m_scrollVelocity.x = 0.0f;
                }
                else
                {
                    m_scrollVelocity.x = m_scrollVelocity.x * k_bounceBackAcceleration;
                }
                
                //Check Y
                if(m_isOverscrolling.second && scrollInRange.second)
                {
                    m_isOverscrolling.second = false;
                    m_scrollVelocity.y = 0.0f;
                }
                else
                {
                    m_scrollVelocity.y = m_scrollVelocity.y * k_bounceBackAcceleration;
                }
            }
            else if(!m_isDragging)
            {
                m_scrollVelocity *= k_scrollDeceleration;
            }
            
            if(std::abs(m_scrollVelocity.x) < k_scrollStoppingPoint || clampedAxis.first)
            {
                m_scrollVelocity.x = 0.0f;
            }
            
            if(std::abs(m_scrollVelocity.y) < k_scrollStoppingPoint || clampedAxis.second)
            {
                m_scrollVelocity.y = 0.0f;
            }
            
            if(m_isDragging)
            {
                //Reset the velocity
                m_scrollVelocity = CS::Vector2::k_zero;
            }
            
            UpdateScrollBars();
            
            m_touchMoved = false;
        }
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    BooleanPair ScrollViewComponent::UpdateContentPosition(const CS::Vector2& in_positionDelta)
    {
        const auto& currentPosition = m_contentWidget->GetLocalAbsolutePosition();
        const auto proposedPosition = currentPosition + in_positionDelta;
   
        auto finalPosition = GetClampedPosition(proposedPosition);
   
        if(!m_enableHorizontalScrolling)
        {
            finalPosition.x = 0.0f;
        }
        
        if(!m_enableVerticalScrolling)
        {
            finalPosition.y = 0.0f;
        }
        
        const auto& isContentSmaller = IsContentSmallerThanScrollArea();
        if(isContentSmaller.first)
        {
            finalPosition.x = 0.0f;
        }
        
        if(isContentSmaller.second)
        {
            finalPosition.y = 0.0f;
        }
        
        m_contentWidget->SetAbsolutePosition(finalPosition);
        
        return { finalPosition.x != proposedPosition.x, finalPosition.y != proposedPosition.y };
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    void ScrollViewComponent::UpdateScrollBars()
    {
        const auto& currentPosition = m_contentWidget->GetLocalAbsolutePosition();
        
        const auto& lowerBounds = GetLowerPositionBound();
        const auto& upperBounds = GetHigherPositionBound();
        
        const auto& isContentSmaller = IsContentSmallerThanScrollArea();
        
        if(isContentSmaller.first)
        {
            m_horizontalProgressBar->SetVisible(false);
        }
        else if(m_enableProgressBars)
        {
            m_horizontalProgressBar->SetVisible(true);
        }
        
        if(isContentSmaller.second)
        {
            m_verticalProgressBar->SetVisible(false);
        }
        else if(m_enableProgressBars)
        {
            m_verticalProgressBar->SetVisible(true);
        }
        
        if(m_enableHorizontalScrolling && !isContentSmaller.first)
        {
            if(m_isOverscrolling.first)
            {
                f32 rangeX = upperBounds.x - lowerBounds.x;
                f32 percentageThroughContentX = CS::MathUtils::Clamp(std::abs(currentPosition.x / rangeX), 0.0f, 1.0f);
                
                auto slider = m_horizontalProgressBar->GetComponent<CS::SliderUIComponent>();
                CS_ASSERT(slider, "Cannot be null");
                
                slider->SetSliderPosition(percentageThroughContentX);
            }
        }
        
        if(m_enableVerticalScrolling && !isContentSmaller.second)
        {
            if(m_isOverscrolling.second)
            {
                f32 rangeY = upperBounds.y - lowerBounds.y;
                f32 percentageThroughContentY = CS::MathUtils::Clamp(std::abs(currentPosition.y / rangeY), 0.0f, 1.0f);
                
                auto slider = m_verticalProgressBar->GetComponent<CS::SliderUIComponent>();
                CS_ASSERT(slider, "Cannot be null");
                
                slider->SetSliderPosition(percentageThroughContentY);
            }
        }
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    BooleanPair ScrollViewComponent::IsContentSmallerThanScrollArea()
    {
        const auto& scrollSize = GetWidget()->GetFinalSize();
        const auto& contentSize = m_contentWidget->GetFinalSize();
        
        return { contentSize.x <= scrollSize.x, contentSize.y <= scrollSize.y };
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    CS::Vector2 ScrollViewComponent::GetLowerPositionBound()
    {
        const auto& scrollSize = GetWidget()->GetFinalSize();
        const auto& contentSize = m_contentWidget->GetFinalSize();
        
        f32 leftBound = (-contentSize.x + scrollSize.x);
        f32 bottomBound = 0;
        
        return CS::Vector2(leftBound, bottomBound);
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    CS::Vector2 ScrollViewComponent::GetHigherPositionBound()
    {
        const auto& scrollSize = GetWidget()->GetFinalSize();
        const auto& contentSize = m_contentWidget->GetFinalSize();
        
        f32 topBound = (contentSize.y - scrollSize.y);
        f32 rightBound = 0;
        
        return CS::Vector2(rightBound, topBound);
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    CS::Vector2 ScrollViewComponent::GetClampedPosition(const CS::Vector2& in_requestedPosition)
    {
        const auto& lowerBound = GetLowerPositionBound();
        const auto& upperBound = GetHigherPositionBound();
        
        const auto& isInScrollRange = IsInScrollRange(in_requestedPosition);
        
        if(m_isDragging && (!isInScrollRange.first || !isInScrollRange.second))
        {
            f32 overscrollX = CS::MathUtils::Clamp(in_requestedPosition.x, lowerBound.x - k_overBounceMax, upperBound.x + k_overBounceMax);
            f32 overscrollY = CS::MathUtils::Clamp(in_requestedPosition.y, lowerBound.y - k_overBounceMax, upperBound.y + k_overBounceMax);
            
            m_isOverscrolling.first = !isInScrollRange.first;
            m_isOverscrolling.second = !isInScrollRange.second;

            return CS::Vector2(overscrollX, overscrollY);
        }
        else if(m_isDragging == false && (m_isOverscrolling.first || m_isOverscrolling.second))
        {
            bool isLessX = in_requestedPosition.x <= lowerBound.x;
            bool isLessY = in_requestedPosition.y <= lowerBound.y;
            
            const auto& currentPosition = m_contentWidget->GetLocalAbsolutePosition();
            
            bool isXOverscrollingBackToPositive = currentPosition.x < in_requestedPosition.x;
            bool isYOverscrollingBackToPositive = currentPosition.y < in_requestedPosition.y;
            
            f32 overscrollX = 0.0f;
            f32 overscrollY = 0.0f;
            
            //Calculate X overscroll
            if(m_isOverscrolling.first)
            {
                if(isLessX)
                {
                    overscrollX = CS::MathUtils::Clamp(in_requestedPosition.x, lowerBound.x - k_overBounceMax, lowerBound.x);
                }
                else
                {
                    if(isXOverscrollingBackToPositive)
                    {
                        overscrollX = CS::MathUtils::Clamp(in_requestedPosition.x, lowerBound.x - k_overBounceMax, lowerBound.x);
                    }
                    else
                    {
                        overscrollX = CS::MathUtils::Clamp(in_requestedPosition.x, upperBound.x, upperBound.x + k_overBounceMax);
                    }
                }
            }
            else
            {
                overscrollX = CS::MathUtils::Clamp(in_requestedPosition.x, lowerBound.x - k_overBounceMax, upperBound.x + k_overBounceMax);
            }
            
            //Calculate Y overscroll
            if(m_isOverscrolling.second)
            {
                if(isLessY)
                {
                    overscrollY = CS::MathUtils::Clamp(in_requestedPosition.y, lowerBound.y - k_overBounceMax, lowerBound.y);
                }
                else
                {
                    if(isYOverscrollingBackToPositive)
                    {
                        overscrollY = CS::MathUtils::Clamp(in_requestedPosition.y, lowerBound.y - k_overBounceMax, lowerBound.x);
                    }
                    else
                    {
                        overscrollY = CS::MathUtils::Clamp(in_requestedPosition.y, upperBound.y, upperBound.y + k_overBounceMax);
                    }
                }
            }
            else
            {
                overscrollY = CS::MathUtils::Clamp(in_requestedPosition.y, lowerBound.y - k_overBounceMax, upperBound.y + k_overBounceMax);
            }
 
            return CS::Vector2(overscrollX, overscrollY);
        }
        else
        {
            f32 clampedX = CS::MathUtils::Clamp(in_requestedPosition.x, lowerBound.x, upperBound.x);
            f32 clampedY = CS::MathUtils::Clamp(in_requestedPosition.y, lowerBound.y, upperBound.y);

            return CS::Vector2(clampedX, clampedY);
        }
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    BooleanPair ScrollViewComponent::IsInScrollRange(const CS::Vector2& in_requestedPosition)
    {
        const auto& lowerBound = GetLowerPositionBound();
        const auto& upperBound = GetHigherPositionBound();
        
        f32 newX = CS::MathUtils::Clamp(in_requestedPosition.x, lowerBound.x, upperBound.x);
        f32 newY = CS::MathUtils::Clamp(in_requestedPosition.y, lowerBound.y, upperBound.y);
        
        bool inRangeX = in_requestedPosition.x == newX;
        bool inRangeY = in_requestedPosition.y == newY;
        
        return { inRangeX, inRangeY };
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    CS::Vector2 ScrollViewComponent::GetOverscrollVelocity(const CS::Vector2& in_currentPosition)
    {
        const auto& lowerBound = GetLowerPositionBound();
        const auto& upperBound = GetHigherPositionBound();
        
        bool isLessX = in_currentPosition.x < lowerBound.x;
        bool isMoreX = in_currentPosition.x > upperBound.x;
        
        bool isLessY = in_currentPosition.y < lowerBound.y;
        bool isMoreY = in_currentPosition.y > upperBound.y;
        
        f32 bounceBackVelocityX = 0.0f;
        f32 bounceBackVelocityY = 0.0f;
        
        if(isLessX)
        {
            bounceBackVelocityX = k_bounceBackVelocity;
        }
        else if(isMoreX)
        {
            bounceBackVelocityX = -k_bounceBackVelocity;
        }
        
        if(isLessY)
        {
            bounceBackVelocityY = k_bounceBackVelocity;
        }
        else if(isMoreY)
        {
            bounceBackVelocityY = -k_bounceBackVelocity;
        }
        
        return CS::Vector2(bounceBackVelocityX, bounceBackVelocityY);
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    void ScrollViewComponent::SetVerticalScrollingEnabled(bool in_enable)
    {
        m_enableVerticalScrolling = in_enable;
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    void ScrollViewComponent::SetHorizontalScrollingEnabled(bool in_enable)
    {
        m_enableHorizontalScrolling = in_enable;
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    void ScrollViewComponent::SetProgressBarsEnabled(bool in_enable)
    {
        m_enableProgressBars = in_enable;
        
        if(m_horizontalProgressBar && m_verticalProgressBar)
        {
            m_horizontalProgressBar->SetVisible(in_enable);
            m_verticalProgressBar->SetVisible(in_enable);
            
            if(in_enable)
            {
                if(!m_enableHorizontalScrolling)
                {
                    m_horizontalProgressBar->SetVisible(false);
                }
                
                if(!m_enableVerticalScrolling)
                {
                    m_verticalProgressBar->SetVisible(false);
                }
            }
        }
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    void ScrollViewComponent::SetScrollVeclocity(const CS::Vector2& in_scrollVelocity)
    {
        m_scrollVelocity = in_scrollVelocity;
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    void ScrollViewComponent::OnPressedInside(CS::Widget* in_widget, const CS::Pointer& in_pointer, CS::Pointer::InputType in_type)
    {
        if (m_draggable)
        {
            m_isDragging = true;
            m_scrollVelocity.x = 0.0f;
            m_scrollVelocity.y = 0.0f;
        }
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    void ScrollViewComponent::OnReleaseInside(CS::Widget* in_widget, const CS::Pointer& in_pointer, CS::Pointer::InputType in_type)
    {
        OnRelease(in_widget, in_pointer, in_type);
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    void ScrollViewComponent::OnReleaseOutside(CS::Widget* in_widget, const CS::Pointer& in_pointer, CS::Pointer::InputType in_type)
    {
        OnRelease(in_widget, in_pointer, in_type);
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    void ScrollViewComponent::OnRelease(CS::Widget* in_widget, const CS::Pointer& in_pointer, CS::Pointer::InputType)
    {
        if (m_draggable)
        {
            m_isDragging = false;
            
            if(m_isOverscrolling.first || m_isOverscrolling.second)
            {
                const auto& currentPosition = m_contentWidget->GetLocalAbsolutePosition();
                const auto& overscrollVelocity = GetOverscrollVelocity(currentPosition);
                
                if(overscrollVelocity.x != 0.0f && m_isOverscrolling.first)
                {
                    m_scrollVelocity.x = overscrollVelocity.x;
                }
                
                if(overscrollVelocity.y != 0.0f && m_isOverscrolling.second)
                {
                    m_scrollVelocity.y = overscrollVelocity.y;
                }
            }
        }
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    void ScrollViewComponent::OnDraggedInside(CS::Widget* in_widget, const CS::Pointer& in_pointer)
    {
        OnDragged(in_widget, in_pointer);
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    void ScrollViewComponent::OnDraggedOutside(CS::Widget* in_widget, const CS::Pointer& in_pointer)
    {
        OnDragged(in_widget, in_pointer);
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    void ScrollViewComponent::OnDragged(CS::Widget* in_widget, const CS::Pointer& in_pointer)
    {
        if (m_draggable)
        {
            if(m_isDragging)
            {
                const auto& position = in_pointer.GetPosition();
                const auto& prevPosition = in_pointer.GetPreviousPosition();
                
                m_scrollVelocity.x += (position.x - prevPosition.x);
                m_scrollVelocity.y += (position.y - prevPosition.y);
                
                m_touchMoved = true;
            }
        }
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    void ScrollViewComponent::SetIsDraggable(bool in_draggable)
    {
        m_draggable = in_draggable;
        if (!in_draggable)
        {
            if (m_isDragging)
            {
                m_isDragging = false;
            }
        }
    }

}