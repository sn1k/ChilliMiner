//
//  ScrollViewComponent.h
//
//  Created by Hugh McLaughlin on 14/01/2015.
//  Copyright (c) 2015 Tag Games Ltd. All rights reserved.
//

#ifndef _APPSOURCE_GUI_COMPONENTS_SCROLLVIEWCOMPONENT_H_
#define _APPSOURCE_GUI_COMPONENTS_SCROLLVIEWCOMPONENT_H_

#include <ChilliSource/ChilliSource.h>
#include <ChilliSource/Core/Base/Colour.h>
#include <ChilliSource/Core/Container/Property/PropertyMap.h>
#include <ChilliSource/Core/Event/EventConnection.h>
#include <ChilliSource/Input/Pointer/Pointer.h>
#include <ChilliSource/UI/Base/UIComponent.h>

namespace Miner
{
    using BooleanPair = std::pair<bool, bool>;
    //-------------------------------------------------------------------
    /// UIComponent that will allow a widget to be scrolled
    ///
    /// @author HMcLaughlin
    //-------------------------------------------------------------------
    class ScrollViewComponent final : public CS::UIComponent
    {
    public:
        CS_DECLARE_NAMEDTYPE(ScrollViewComponent);
        //-------------------------------------------------------------------
        /// @author HMcLaughlin
        ///
        /// @return The list of properties supported by a highlight component.
        //-------------------------------------------------------------------
        static const std::vector<CS::PropertyMap::PropertyDesc>& GetPropertyDescs();
        //-------------------------------------------------------------------
        /// Allows querying of whether or not the component implements the
        /// interface associated with the given interface Id.
        ///
        /// @author HMcLaughlin
        /// @param The interface Id.
        /// @return Whether the object implements the given interface.
        //-------------------------------------------------------------------
        bool IsA(CS::InterfaceIDType in_interfaceId) const override;
        //-------------------------------------------------------------------
        /// Enables Vertical Scrolling
        ///
        /// @author HMcLaughlin
        /// @param Enable
        //-------------------------------------------------------------------
        void SetVerticalScrollingEnabled(bool in_enable);
        //-------------------------------------------------------------------
        /// Enables Horizontal Scrolling
        ///
        /// @author HMcLaughlin
        /// @param Enable
        //-------------------------------------------------------------------
        void SetHorizontalScrollingEnabled(bool in_enable);
        //-------------------------------------------------------------------
        /// Enables ProgressBars
        ///
        /// @author HMcLaughlin
        /// @param Enable
        //-------------------------------------------------------------------
        void SetProgressBarsEnabled(bool in_enable);
        //-------------------------------------------------------------------
        /// Sets the scroll velocity
        ///
        /// @author HMcLaughlin
        /// @param Scroll Velocity
        //-------------------------------------------------------------------
        void SetScrollVeclocity(const CS::Vector2& in_scrollVelocity);
        //-------------------------------------------------------------------
        /// @author HMcLaughlin
        /// @return Enable
        //-------------------------------------------------------------------
        bool IsVerticalScrollingEnabled() { return m_enableVerticalScrolling; }
        //-------------------------------------------------------------------
        /// @author HMcLaughlin
        /// @return Enable
        //-------------------------------------------------------------------
        bool IsHorizontalScrollingEnabled() { return m_enableHorizontalScrolling; }
        //-------------------------------------------------------------------
        /// @author HMcLaughlin
        /// @return Enable
        //-------------------------------------------------------------------
        bool IsProgressBarsEnabled() { return m_enableProgressBars; }
        //-------------------------------------------------------------------
        /// @author HMcLaughlin
        /// @return Enable
        //-------------------------------------------------------------------
        CS::Vector2 GetScrollVelocity() { return m_scrollVelocity; }
        //-------------------------------------------------------------------
        /// @author T Kane
        /// @param true means it can be scrolled with drags. False means not
        //-------------------------------------------------------------------
        void SetIsDraggable(bool in_draggable);
        //-------------------------------------------------------------------
        /// Returns if the content pane is smaller than the scrollpane
        ///
        /// @author HMcLaughlin
        /// @return IsSmaller (x - Is Width Smaller, y - IsHeightSmaller)
        //-------------------------------------------------------------------
        BooleanPair IsContentSmallerThanScrollArea();
        
    private:
		friend class CS::UIComponentFactory;
        //-------------------------------------------------------------------
        /// Constructor
        ///
        /// @author HMcLaughlin
        /// @param The component name.
        /// @param The property map.
        //-------------------------------------------------------------------
        ScrollViewComponent(const std::string& in_componentName, const CS::PropertyMap& in_properties);
        //----------------------------------------------------------------
        /// @author HMcLaughlin
        //----------------------------------------------------------------
        void OnInit() override;
        //----------------------------------------------------------------
        /// This is called when the owning widget is added to the canvas.
        ///
        /// @author HMcLaughlin
        //----------------------------------------------------------------
        void OnAddedToCanvas() override;
        //----------------------------------------------------------------
        /// @author HMcLaughlin
        //----------------------------------------------------------------
        void OnUpdate(f32 in_deltaTime) override;
        //-------------------------------------------------------------------
        /// Updates the content position
        ///
        /// @author HMcLaughlin
        /// @param Pointer Position Delta
        /// @return If the position was clamped
        //-------------------------------------------------------------------
        BooleanPair UpdateContentPosition(const CS::Vector2& in_positionDelta);
        //-------------------------------------------------------------------
        /// Updates the scrollbars
        ///
        /// @author HMcLaughlin
        //-------------------------------------------------------------------
        void UpdateScrollBars();
        //-------------------------------------------------------------------
        /// Gets the lowest valid position for the content, i.e. left-most/bottom-most
        ///
        /// @author HMcLaughlin
        /// @return Lowest Valid Position
        //-------------------------------------------------------------------
        CS::Vector2 GetLowerPositionBound();
        //-------------------------------------------------------------------
        /// Gets the highest valid position for the content, i.e. right-most/top-most
        ///
        /// @author HMcLaughlin
        /// @return Highest Valid Position
        //-------------------------------------------------------------------
        CS::Vector2 GetHigherPositionBound();
        //-------------------------------------------------------------------
        /// Gets the position clamped within the valid scrollbounds, this will apply overscrolling if applicable
        ///
        /// @author HMcLaughlin
        /// @param Requested Position
        /// @return Clamped content position
        //-------------------------------------------------------------------
        CS::Vector2 GetClampedPosition(const CS::Vector2& in_requestedPosition);
        //-------------------------------------------------------------------
        /// Gets if the position is withing valid scroll range
        ///
        /// @author HMcLaughlin
        /// @param Requested Position
        /// @return If position is in valid range
        //-------------------------------------------------------------------
        BooleanPair IsInScrollRange(const CS::Vector2& in_requestedPosition);
        //-------------------------------------------------------------------
        /// Gets the overscroll velocity for the current position, this should be used when the user lets a drag go while overscrolling
        ///
        /// @author HMcLaughlin
        /// @param Requested Position
        /// @return Overscroll velocity
        //-------------------------------------------------------------------
        CS::Vector2 GetOverscrollVelocity(const CS::Vector2& in_currentPosition);
        //-------------------------------------------------------------------
        /// Event for when pointer is pressed inside the scrollview
        ///
        /// @author HMcLaughlin
        /// @param Widget
        /// @return Pointer
        //-------------------------------------------------------------------
        void OnPressedInside(CS::Widget* in_widget, const CS::Pointer& in_pointer, CS::Pointer::InputType);
        //-------------------------------------------------------------------
        /// Event for when pointer is released inside the scrollview
        ///
        /// @author HMcLaughlin
        /// @param Widget
        /// @return Pointer
        //-------------------------------------------------------------------
        void OnReleaseInside(CS::Widget* in_widget, const CS::Pointer& in_pointer, CS::Pointer::InputType);
        //-------------------------------------------------------------------
        /// Event for when pointer is released outside the scrollview
        ///
        /// @author HMcLaughlin
        /// @param Widget
        /// @return Pointer
        //-------------------------------------------------------------------
        void OnReleaseOutside(CS::Widget* in_widget, const CS::Pointer& in_pointer, CS::Pointer::InputType);
        //-------------------------------------------------------------------
        /// Event for when pointer is released
        ///
        /// @author HMcLaughlin
        /// @param Widget
        /// @return Pointer
        //-------------------------------------------------------------------
        void OnRelease(CS::Widget* in_widget, const CS::Pointer& in_pointer, CS::Pointer::InputType);
        //-------------------------------------------------------------------
        /// Event for when pointer is dragged inside the scrollview
        ///
        /// @author HMcLaughlin
        /// @param Widget
        /// @return Pointer
        //-------------------------------------------------------------------
        void OnDraggedInside(CS::Widget* in_widget, const CS::Pointer& in_pointer);
        //-------------------------------------------------------------------
        /// Event for when pointer is dragged outside the scrollview
        ///
        /// @author HMcLaughlin
        /// @param Widget
        /// @param Pointer
        //-------------------------------------------------------------------
        void OnDraggedOutside(CS::Widget* in_widget, const CS::Pointer& in_pointer);
        //-------------------------------------------------------------------
        /// Event for when pointer is dragged
        ///
        /// @author HMcLaughlin
        /// @param Widget
        /// @param Pointer
        //-------------------------------------------------------------------
        void OnDragged(CS::Widget* in_widget, const CS::Pointer& in_pointer);
        
    private:
        
        CS::Widget* m_contentWidget = nullptr;
        CS::Widget* m_horizontalProgressBar = nullptr;
        CS::Widget* m_verticalProgressBar = nullptr;
        
    private:

        bool m_enableVerticalScrolling = false;
        bool m_enableHorizontalScrolling = true;
        bool m_enableProgressBars = true;
        
        CS::Vector2 m_scrollVelocity;
        
        BooleanPair m_isOverscrolling = { false, false };
        
        bool m_isDragging = false;
        bool m_touchMoved = false;
        
        std::vector<CS::EventConnectionUPtr> m_widgetTouchConnections;
        bool m_draggable = true;
    };
}

#endif /* defined(_APPSOURCE_GUI_COMPONENTS_SCROLLVIEWCOMPONENT_H_) */
