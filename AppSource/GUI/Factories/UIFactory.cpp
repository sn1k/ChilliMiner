//
// @author snik
//

#include <GUI/Factories/UIFactory.h>

#include <ChilliSource/UI/Base/Canvas.h>
#include <ChilliSource/UI/Base/Widget.h>


enum class ENUM
{
	k_hud,
	k_inventory,
	k_mainMenu,
	k_buildings,
	k_fabrication,
	k_geological
};

static std::string k_CSLocations[] = 
{
	"UI/HUD.csui",			// k_hud
	"UI/InventoryMenu.csui", // k_inventory
	"UI/BuildMenu.csui", // k_mainMenu
	"UI/BuildMenu.csui", // k_buildings
	"UI/BuildMenu.csui", // k_fabrication
	"UI/BuildMenu.csui"  // k_geological
};


HUDUIControllerUPtr UIFactory::CreateHUD(CS::Canvas* in_stateUICanvas,  BuildMenuUIControllerUPtr in_buildmenuController, InventoryUIControllerUPtr in_inventoryController)
{
	CS::WidgetSPtr widget(CreateWidget(k_CSLocations[(u32)ENUM::k_hud], in_stateUICanvas));

	// create controller
	HUDUIControllerUPtr hud( new HUDUIController(widget, std::move(in_buildmenuController), std::move(in_inventoryController)));

	hud->Init();

	return hud;
}


InventoryUIControllerUPtr UIFactory::CreateInventory(CS::Canvas* in_stateUICanvas)
{
	CS::WidgetSPtr widget(CreateWidget(k_CSLocations[(u32)ENUM::k_inventory], in_stateUICanvas));
	widget->SetVisible(false);

	// lift inventory list


	// create controller
	InventoryUIControllerUPtr inventory(new InventoryUIController(widget));

	// init controller
	inventory->Init();
	inventory->Enable(false);

	return inventory;
}


BuildMenuUIControllerUPtr UIFactory::CreateBuildMenu(CS::Canvas* in_stateUICanvas, std::function<void(BuildingTypes::ENUM)>& in_buildingSelectCallback)
{
	CS::WidgetSPtr widget( CreateWidget(k_CSLocations[(u32)ENUM::k_buildings], in_stateUICanvas) );
	widget->SetVisible(false);

	// create controller
	BuildMenuUIControllerUPtr buildMenu(new BuildMenuUIController(widget, in_buildingSelectCallback));

	// init controller
	buildMenu->Init();
	buildMenu->Enable(false);

	return buildMenu;
}

// helper to load widgets from CS files
CS::WidgetSPtr UIFactory::CreateWidget(const std::string& in_packagePath, CS::Canvas* in_stateUICanvas)
{
	auto widgetFactory = CS::Application::Get()->GetWidgetFactory();
	auto resPool = CS::Application::Get()->GetResourcePool();

	auto templateWidget = resPool->LoadResource<CS::WidgetTemplate>(CS::StorageLocation::k_package, in_packagePath);

	CS::WidgetSPtr widget ( widgetFactory->Create(templateWidget) );

	in_stateUICanvas->AddWidget(widget);

	return widget;
}