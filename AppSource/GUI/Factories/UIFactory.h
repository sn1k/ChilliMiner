//
// @author snik
//

#ifndef _APPSOURCE_GUI_FACTORIES_UIFACTORY_H_
#define _APPSOURCE_GUI_FACTORIES_UIFACTORY_H_

#include <ChilliSource/UI/ForwardDeclarations.h>

#include <ChilliSource/Core/Base/Application.h>
#include <ChilliSource/Core/Resource/ResourcePool.h>

#include <ChilliSource/UI/Base/Widget.h>
#include <ChilliSource/UI/Base/WidgetFactory.h>


// maybe move this to cpp with forward declaring the base type?
#include <GUI/Controllers/InventoryUIController.h>
#include <GUI/Controllers/BuildMenuUIController.h>
#include <GUI/Controllers/HUDUIController.h>

#include <Universe/Factories/FactoryAsteroidObjects.h> // for callback type in buildmenu callback, maybe move the enum declaration

CS_FORWARDDECLARE_CLASS(UIFactory);


//-------------------------------------------------------------------
/// 
///
/// @author snik
//-------------------------------------------------------------------
class UIFactory
{

public:
	UIFactory() = delete;
	~UIFactory() = delete;

	static HUDUIControllerUPtr CreateHUD(CS::Canvas* in_stateUICanvas, BuildMenuUIControllerUPtr in_buildmenuController, InventoryUIControllerUPtr in_inventoryController);
	static InventoryUIControllerUPtr CreateInventory(CS::Canvas* in_stateUICanvas);
	static BuildMenuUIControllerUPtr CreateBuildMenu(CS::Canvas* in_stateUICanvas, std::function<void(BuildingTypes::ENUM)>& in_buildingSelectedCallback);

private:
	static CS::WidgetSPtr CreateWidget(const std::string& in_packagePath, CS::Canvas* in_stateUICanvas);

};


#endif /* defined(_APPSOURCE_GUI_FACTORIES_UIFACTORY_H_) */
