//
//

#include <GUI/Controllers/HUDUIController.h>

#include <ChilliSource/UI/Text/TextComponent.h>

#include <Universe/Buildings/BuildingHQController.h>


HUDUIController::HUDUIController(
	const CS::WidgetSPtr& in_root,
	BuildMenuUIControllerUPtr in_buildmenuController,
	InventoryUIControllerUPtr in_inventoryController)
: UIController(in_root)
, m_buildMenuController(std::move(in_buildmenuController))
, m_inventoryMenuController(std::move(in_inventoryController))
{}

//----------------------------------------------------------------------------------------
/// Finds an opens press connections to the inventory screen buttons.
///
/// @author snik
///
/// @param Widget parent widget
//----------------------------------------------------------------------------------------
void HUDUIController::Init()
{
	/*
	// button hookup
	m_inventoryButtonClose = m_root->GetWidgetRecursive("ButtonClose");
	CS_ASSERT(m_inventoryButtonClose != nullptr, "HUDUIController::Init couldn't find m_inventoryButtonClose");

	m_buttonInventoryMenuCloseConnection = m_inventoryButtonClose->GetReleasedInsideEvent().OpenConnection([this](CS::Widget* in_widget, const CS::Pointer& in_pointer, CS::Pointer::InputType in_inputType)
	{
		Enable(false);
	});*/

	CS::WidgetSPtr buildButton(m_root->GetWidgetRecursive("ButtonBuild"));
	CS::WidgetSPtr inventoryButton(m_root->GetWidgetRecursive("ButtonInventory"));

	m_buttonBuildMenuOpenConnection = buildButton->GetReleasedInsideEvent().OpenConnection([this](CS::Widget* in_widget, const CS::Pointer& in_pointer, CS::Pointer::InputType in_inputType)
	{
		m_buildMenuController->Enable(true);
	});

	m_buttonInventoryMenuOpenConnection = inventoryButton->GetReleasedInsideEvent().OpenConnection([this](CS::Widget* in_widget, const CS::Pointer& in_pointer, CS::Pointer::InputType in_inputType)
	{
		m_inventoryMenuController->Enable(true);
	});

	// TODO replace after hud uicontroller port
	m_currencyALabel = m_root->GetWidget("CurrencyABacking")->GetWidget("LabelCurrencyA");
	m_currencyBLabel = m_root->GetWidget("CurrencyBBacking")->GetWidget("LabelCurrencyB");
}

void HUDUIController::SetHQ(const BuildingHQWPtr& in_hq)
{
	m_playerHQ = in_hq;

	// update inventory ui with actual inventory
	m_inventoryMenuController->SetInventory(in_hq.lock()->GetExtractedOreInventory());
}

/** 
*	Updates the hud label with the current currency value
*/
void HUDUIController::UpdateCurrencyLabel()
{
	if (m_playerHQ.expired() == false)
	{
		Inventory::InventoryMap inv = m_playerHQ.lock()->GetExtractedOreInventory()->GetInventory();

		CS::TextUIComponent* currencyLabel = m_currencyALabel->GetComponent<CS::TextUIComponent>();
		currencyLabel->SetText(CS::ToString(inv[Logistics::Item::ORE_A]));

		currencyLabel = m_currencyBLabel->GetComponent<CS::TextUIComponent>();
		currencyLabel->SetText(CS::ToString(inv[Logistics::Item::ORE_B]));
		
	}
}

void HUDUIController::EnableBuildMenu(bool in_enabled)
{
	m_buildMenuController->Enable(in_enabled);
}
