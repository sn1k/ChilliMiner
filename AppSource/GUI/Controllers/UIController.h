//
// @author snik
//

#ifndef _APPSOURCE_GUI_CONTROLLERS_UICONTROLLER_H_
#define _APPSOURCE_GUI_CONTROLLERS_UICONTROLLER_H_

#include <ChilliSource/UI/ForwardDeclarations.h>

#include <ChilliSource/Core/Base/Application.h>
#include <ChilliSource/Core/Resource/ResourcePool.h>

#include <ChilliSource/UI/Base/Widget.h>
#include <ChilliSource/UI/Base/WidgetFactory.h>
#include <ChilliSource/UI/Base/WidgetTemplate.h>


CS_FORWARDDECLARE_CLASS(UIController);

//-------------------------------------------------------------------
/// 
///
/// @author snik
//-------------------------------------------------------------------
class UIController
{
public:
	UIController() = delete;
	UIController(const CS::WidgetSPtr& in_root);

	virtual void Init() = 0;

	virtual void Update() {}

	void Enable(bool in_enabled);

protected:
	CS::WidgetSPtr m_root;
};


#endif /* defined(_APPSOURCE_GUI_CONTROLLERS_UICONTROLLER_H_) */
