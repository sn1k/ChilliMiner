//
// @author snik
//

#ifndef _APPSOURCE_GUI_CONTROLLERS_HUDUICONTROLLER_H_
#define _APPSOURCE_GUI_CONTROLLERS_HUDUICONTROLLER_H_

#include <GUI/Controllers/UIController.h>

#include <ChilliSource/ChilliSource.h>

#include <Universe/ForwardDeclares.h>


// TODO forward declare these or get the forwards header
#include <GUI/Controllers/BuildMenuUIController.h>
#include <GUI/Controllers/InventoryUIController.h>

CS_FORWARDDECLARE_CLASS(HUDUIController);

//-------------------------------------------------------------------
/// Handles basic hud interactions
///
/// @author snik
//-------------------------------------------------------------------
class HUDUIController final:  public UIController
{
public:
	HUDUIController(const CS::WidgetSPtr& in_root, BuildMenuUIControllerUPtr in_buildmenuController, InventoryUIControllerUPtr in_inventoryController);

	void Init() override;

	void SetHQ(const BuildingHQWPtr& in_hq);

	void UpdateCurrencyLabel();

	void EnableBuildMenu(bool in_enabled);

private:

	InventoryUIControllerUPtr m_inventoryMenuController;
	BuildMenuUIControllerUPtr m_buildMenuController;

	CS::EventConnectionUPtr m_buttonInventoryMenuOpenConnection;
	CS::EventConnectionUPtr m_buttonBuildMenuOpenConnection;

	CS::WidgetSPtr m_currencyALabel;
	CS::WidgetSPtr m_currencyBLabel;

	BuildingHQWPtr m_playerHQ;
       
};

#endif /* defined(_APPSOURCE_GUI_CONTROLLERS_HUDUICONTROLLER_H_) */
