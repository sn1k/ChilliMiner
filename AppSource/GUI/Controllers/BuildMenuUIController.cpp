//
//

#include <GUI/Controllers/BuildMenuUIController.h>

#include <ChilliSource/UI/Text/TextComponent.h>


BuildMenuUIController::BuildMenuUIController(const CS::WidgetSPtr& in_root, BuildingSelectCallback& in_callback)
: UIController(in_root)
, m_selectCallback(in_callback)
{}

//----------------------------------------------------------------------------------------
/// Finds an opens press connections to the inventory screen buttons.
///
/// @author snik
///
/// @param Widget parent widget
//----------------------------------------------------------------------------------------
void BuildMenuUIController::Init()
{
	// button hookup
	//m_inventoryButtonClose = m_root->GetWidgetRecursive("ButtonClose");
	//CS_ASSERT(m_inventoryButtonClose != nullptr, "InventoryUIController::Init couldn't find m_inventoryButtonClose");

	m_buildButtonHQ = m_root->GetWidgetRecursive("ButtonHQ");
	m_buildButtonDrill = m_root->GetWidgetRecursive("ButtonDrill");
	m_buildButtonGenerator = m_root->GetWidgetRecursive("ButtonGenerator");
	m_buildButtonMissileSilo = m_root->GetWidgetRecursive("ButtonMissileSilo");

	// >>>>>>>>>
	// TODO - stop consumption if menu is closed (destroy and rebuild?)

	m_buttonHQSelectConnection = m_buildButtonHQ->GetReleasedInsideEvent().OpenConnection([this](CS::Widget* in_widget, const CS::Pointer& in_pointer, CS::Pointer::InputType in_inputType)
	{
		SelectIfValid(BuildingTypes::ENUM::k_HQ, in_inputType);
	});

	m_buttonDrillSelectConnection = m_buildButtonDrill->GetReleasedInsideEvent().OpenConnection([this](CS::Widget* in_widget, const CS::Pointer& in_pointer, CS::Pointer::InputType in_inputType)
	{
		SelectIfValid(BuildingTypes::ENUM::k_drill, in_inputType);
	});

	m_buttonGeneratorSelectConnection = m_buildButtonGenerator->GetReleasedInsideEvent().OpenConnection([this](CS::Widget* in_widget, const CS::Pointer& in_pointer, CS::Pointer::InputType in_inputType)
	{
		SelectIfValid(BuildingTypes::ENUM::k_generator, in_inputType);
	});

	m_buttonMissileSiloSelectConnection = m_buildButtonMissileSilo->GetReleasedInsideEvent().OpenConnection([this](CS::Widget* in_widget, const CS::Pointer& in_pointer, CS::Pointer::InputType in_inputType)
	{
		SelectIfValid(BuildingTypes::ENUM::k_missileSilo, in_inputType);
	});
}

void BuildMenuUIController::SelectIfValid(BuildingTypes::ENUM in_selection, CS::Pointer::InputType in_inputType)
{
	if (in_inputType != CS::Pointer::GetDefaultInputType() || !m_root->IsVisible())
		return;

	Enable(false);

	m_selectCallback(in_selection);
}

BuildMenuUIController::~BuildMenuUIController()
{
	m_buttonHQSelectConnection->Close();
	m_buttonDrillSelectConnection->Close();
	m_buttonGeneratorSelectConnection->Close();
	m_buttonMissileSiloSelectConnection->Close();
}

