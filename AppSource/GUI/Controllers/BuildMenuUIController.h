//
// @author snik
//

#ifndef _APPSOURCE_GUI_CONTROLLERS_BUILDMENUUICONTROLLER_H_
#define _APPSOURCE_GUI_CONTROLLERS_BUILDMENUUICONTROLLER_H_

#include <GUI/Controllers/UIController.h>

#include <ChilliSource/ChilliSource.h>

#include <Universe/Factories/FactoryAsteroidObjects.h> // for enum type in buildmenu callback (maybe move it)
#include <Universe/ForwardDeclares.h>


CS_FORWARDDECLARE_CLASS(BuildMenuUIController);

//-------------------------------------------------------------------
/// 
///
/// @author snik
//-------------------------------------------------------------------
class BuildMenuUIController final:  public UIController
{
public:
	class IBuildMenuSelection
	{
		public: virtual void BuildingSelected(BuildingTypes::ENUM in_selection) = 0;
	};

public:
	typedef std::function<void(BuildingTypes::ENUM)> BuildingSelectCallback;

	BuildMenuUIController(const CS::WidgetSPtr& in_root, BuildingSelectCallback& in_buildingSelectedCallback);
	~BuildMenuUIController();

	void Init() override;

private:
	void SelectIfValid(BuildingTypes::ENUM in_selection, CS::Pointer::InputType in_inputType);
	
private:
	CS::WidgetSPtr m_buildButtonHQ;
	CS::WidgetSPtr m_buildButtonDrill;
	CS::WidgetSPtr m_buildButtonGenerator;
	CS::WidgetSPtr m_buildButtonMissileSilo;

	CS::EventConnectionUPtr m_buttonHQSelectConnection;
	CS::EventConnectionUPtr m_buttonDrillSelectConnection;
	CS::EventConnectionUPtr m_buttonGeneratorSelectConnection;
	CS::EventConnectionUPtr m_buttonMissileSiloSelectConnection;

	BuildingSelectCallback m_selectCallback;
};

#endif /* defined(_APPSOURCE_GUI_CONTROLLERS_BUILDMENUUICONTROLLER_H_) */
