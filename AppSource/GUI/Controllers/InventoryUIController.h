//
// @author snik
//

#ifndef _APPSOURCE_GUI_CONTROLLERS_INVENTORYUICONTROLLER_H_
#define _APPSOURCE_GUI_CONTROLLERS_INVENTORYUICONTROLLER_H_

#include <GUI/Controllers/UIController.h>

#include <ChilliSource/ChilliSource.h>

#include <Universe/ForwardDeclares.h>
#include <Universe/Logistics/Inventory.h>

CS_FORWARDDECLARE_CLASS(InventoryUIController);

//-------------------------------------------------------------------
/// 
///
/// @author snik
//-------------------------------------------------------------------
class InventoryUIController final:  public UIController
{
public:
	InventoryUIController(const CS::WidgetSPtr& in_root);

	void Init() override;

	void BuildContents();

	void SetInventory(const InventoryPtr& in_inventory);

	void Update() override;

private:
	void UpdateContentQuantities();

	void OnInventoryChanged(Logistics::Item in_type, f64 in_amountChange);

private:
	CS::WidgetSPtr m_inventoryMenu; // root object
	CS::WidgetSPtr m_inventoryButtonClose;

	CS::WidgetSPtr m_scrollWidget; // content base

	std::unordered_map<Logistics::Item, CS::WidgetSPtr> m_itemLinkages;

	InventoryPtr m_inventory;

	CS::EventConnectionUPtr m_buttonInventoryMenuCloseConnection;

	bool m_inventoryChangedListenerAssigned;
       
};

#endif /* defined(_APPSOURCE_GUI_CONTROLLERS_INVENTORYUICONTROLLER_H_) */
