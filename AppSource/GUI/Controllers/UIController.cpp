//
//

#include <GUI/Controllers/UIController.h>

#include <ChilliSource/UI/Base/Widget.h>


UIController::UIController(const CS::WidgetSPtr& in_root) : m_root(in_root)
{
}

void UIController::Enable(bool in_enabled)
{
	m_root->SetVisible(in_enabled);
	m_root->SetInputEnabled(in_enabled);
	m_root->SetInputConsumeEnabled(in_enabled);
}

