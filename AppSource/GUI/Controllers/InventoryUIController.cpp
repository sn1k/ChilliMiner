//
//

#include <GUI/Controllers/InventoryUIController.h>


#include <ChilliSource/UI/Text/TextComponent.h>


InventoryUIController::InventoryUIController(const CS::WidgetSPtr& in_root)
: UIController(in_root)
, m_inventoryChangedListenerAssigned(false)
{}

//----------------------------------------------------------------------------------------
/// Finds an opens press connections to the inventory screen buttons.
///
/// @author snik
///
/// @param Widget parent widget
//----------------------------------------------------------------------------------------
void InventoryUIController::Init()
{
	// button hookup
	m_inventoryButtonClose = m_root->GetWidgetRecursive("ButtonClose");
	CS_ASSERT(m_inventoryButtonClose != nullptr, "InventoryUIController::Init couldn't find m_inventoryButtonClose");

	m_buttonInventoryMenuCloseConnection = m_inventoryButtonClose->GetReleasedInsideEvent().OpenConnection([this](CS::Widget* in_widget, const CS::Pointer& in_pointer, CS::Pointer::InputType in_inputType)
	{
		Enable(false);
	});

}

void InventoryUIController::SetInventory(const InventoryPtr& in_inventory)
{
	m_inventory = in_inventory;

	if (m_inventoryChangedListenerAssigned == false)
		m_inventoryChangedListenerAssigned = m_inventory->AddInventoryChangedListener(std::bind(&InventoryUIController::OnInventoryChanged, this, std::placeholders::_1, std::placeholders::_2));//&);

	BuildContents();
}

void InventoryUIController::OnInventoryChanged(Logistics::Item in_type, f64 in_amountChange)
{
	BuildContents();
}

void InventoryUIController::BuildContents()
{
	m_itemLinkages.clear();

	// create scroll view and add some test stuff
	m_scrollWidget = m_root->GetWidgetRecursive("InventoryScrollView");

	auto widgets(m_scrollWidget->GetWidgets());

	for (auto widget : widgets)
	{
		m_scrollWidget->RemoveWidget(widget.get());
	}

	auto widgetFactory(CS::Application::Get()->GetWidgetFactory());
	auto resPool(CS::Application::Get()->GetResourcePool());

	CS::WidgetTemplateCSPtr templateWidget(resPool->LoadResource<CS::WidgetTemplate>(CS::StorageLocation::k_package, "UI/Inventory/InventoryItem.csui"));

	auto inventoryMap(m_inventory->GetInventory());

	for (auto element : inventoryMap)
	{
		CS::WidgetSPtr widget(widgetFactory->Create(templateWidget));

		float relsizeY = widget->GetLocalRelativeSize().y;
		CS::Vector2 relsize(widget->GetLocalRelativeSize());
		
		//widget->SetRelativePosition(CS::Vector2(0.f, -i*widget->GetLocalRelativeSize().y)); // shift down
		m_scrollWidget->AddWidget(widget);
		
		CS::WidgetSPtr nameLabel(widget->GetWidget("labelItemName"));
		CS_ASSERT(nameLabel != nullptr, "InventoryUI couldn't find name widget for list element");
		CS::TextUIComponent* textComponentName(nameLabel->GetComponent<CS::TextUIComponent>());
		CS_ASSERT(textComponentName != nullptr, "InventoryUI couldn't find name label on widget");

		textComponentName->SetText("element " + CS::ToString((u32)element.first));

		CS::WidgetSPtr descLabel(widget->GetWidget("labelItemDescription"));
		CS_ASSERT(descLabel != nullptr, "InventoryUI couldn't find desc widget for list element");
		CS::TextUIComponent* textComponentDesc(descLabel->GetComponent<CS::TextUIComponent>());
		CS_ASSERT(textComponentDesc != nullptr, "InventoryUI couldn't find desc label on widget");

		textComponentDesc->SetText("amount: " + CS::ToString(element.second));

		// hook up a type/qidget link so we can update later
		m_itemLinkages[element.first] = widget;
	}
}

// gonna need to add/remove elements when map changes

// should prolly store a map and use the widget child order, rebuilding the text and images on it whenever we need to

// may want to reduce the mining/inventory update tick to per second

void InventoryUIController::UpdateContentQuantities()
{
	if (m_itemLinkages.size() == 0)
		return;

	auto children(m_scrollWidget->GetWidgets());

	for (auto& child : m_itemLinkages)
	{
		CS::WidgetSPtr quantLabel(child.second->GetWidget("labelItemQuantity"));
		CS::TextUIComponent* textComponentQuant(quantLabel->GetComponent<CS::TextUIComponent>());

		textComponentQuant->SetText(CS::ToString((u32)child.first));
	}
}


void InventoryUIController::Update()
{
	UpdateContentQuantities();
}

