//
//  State.h
//  Miner
//  Created by Ian Copland on 15/10/2014.
//
//  The MIT License (MIT)
//
//  Copyright (c) 2014 Tag Games Limited
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#ifndef _MINER_STATEMAP_H_
#define _MINER_STATEMAP_H_

#include <ChilliSource/ChilliSource.h>
#include <ChilliSource/Core/State.h>

class CUniverse;

namespace Miner
{
	class StateMap : public CS::State
    {
    public:
        void CreateSystems() override;
        void OnInit() override;
        void OnUpdate(f32 in_deltaTime) override;
        void OnDestroy() override;

	private:
		void AddCamera();
		void OpenInputConnections();
		void LoadAsteroids();
		void AddLights();
		
		void CreateUI();
		void HookUIButtons(const CS::WidgetSPtr& in_parent);

		void UpdateOrbitalCameraPosition(const CS::Vector2& in_positionDiff);

		void UpdateZoomLevel(const CS::Vector2& in_scrollDelta);

		void Pick(CS::Ray in_screenRay);

	private:
		CS::EventConnectionUPtr m_pointerDownConnection;
		CS::EventConnectionUPtr m_pointerMovedConnection;
		CS::EventConnectionUPtr m_pointerUpConnection;
		CS::EventConnectionUPtr m_pointerScrollConnection;

		CS::EventConnectionUPtr m_buttonConnectionExit;
		CS::EventConnectionUPtr m_buttonConnectionSettings;

		std::shared_ptr<CUniverse> mpUniverse;
		
		std::vector<CS::EntityWPtr> m_builtEntities;

		CS::EntitySPtr m_camera;
		CS::CameraComponentSPtr m_cameraComponent;

	private:

		f32 m_cameraZoom = 1.f;
		f32 m_mouseHorizontalAngle = 3.1415926536f;
		f32 m_mouseVerticalAngle = 0.f;
    };
}

#endif
