

#ifndef _MINER_STATES_STATETEST_H_
#define _MINER_STATES_STATETEST_H_

#include <ChilliSource/ChilliSource.h>
#include <ChilliSource/Core/State.h>

class CUniverse;

namespace Miner
{
	class StateTest : public CS::State
    {
    public:
        void CreateSystems() override;
        void OnInit() override;
        void OnUpdate(f32 in_deltaTime) override;
        void OnDestroy() override;

	private:
		void AddCamera();
		void OpenInputConnections();
		void BuildSphere();
		void AddLights();

		void UpdateOrbitalCameraPosition(const CS::Vector2& in_positionDiff);

		void UpdateZoomLevel(const CS::Vector2& in_scrollDelta);


	private:
		CS::EventConnectionUPtr m_pointerDownConnection;
		CS::EventConnectionUPtr m_pointerMovedConnection;
		CS::EventConnectionUPtr m_pointerUpConnection;
		CS::EventConnectionUPtr m_pointerScrollConnection;

		CS::EntitySPtr m_camera;
		CS::CameraComponentSPtr m_cameraComponent;

	private:

		f32 m_cameraZoom = 1.f;
		f32 m_mouseHorizontalAngle = 3.1415926536f;
		f32 m_mouseVerticalAngle = 0.f;
    };
}

#endif
