

#include <States/StateAsteroid.h>

#include <ChilliSource/Audio/CricketAudio.h>

#include <ChilliSource/Core/Base.h>
#include <ChilliSource/Core/Entity.h>
#include <ChilliSource/Core/Math.h>
#include <ChilliSource/Core/Math/Geometry/ShapeIntersection.h>
#include <ChilliSource/Core/Resource.h>
#include <ChilliSource/Core/Scene.h>

#include <ChilliSource/Input/Pointer.h>
#include <ChilliSource/Input/Keyboard/Keyboard.h>
#include <ChilliSource/Input/Keyboard/KeyCode.h>

#include <ChilliSource/Rendering/Base.h>
#include <ChilliSource/Rendering/Camera.h>
#include <ChilliSource/Rendering/Lighting.h>
#include <ChilliSource/Rendering/Material/Material.h>
#include <ChilliSource/Rendering/Material/MaterialFactory.h>
#include <ChilliSource/Rendering/Model.h>
#include <ChilliSource/Rendering/Model/StaticModelComponent.h>
#include <ChilliSource/Rendering/Texture/Texture.h>
#include <ChilliSource/Rendering/Texture/TextureDesc.h>

#include <ChilliSource/UI/Base.h>
#include <ChilliSource/UI/Layout/GridUILayout.h>
#include <ChilliSource/UI/Layout/LayoutUIComponent.h>

#include <GUI/Components/ScrollViewComponent.h>
#include <GUI/Controllers/InventoryUIController.h>
#include <GUI/Factories/UIFactory.h>

#include <Math/PickingTools.h>

#include <Universe/Asteroid/AsteroidController.h>
#include <Universe/Asteroid/MineableController.h>
#include <Universe/Buildings/BuildingHQController.h> // TODO handling extraction here for now, move elsewhere
#include <Universe/Factories/FactoryAsteroidObjects.h>
#include <Universe/Factories/FactoryOrbitalObjects.h> // for static scene set
#include <Universe/Logistics/Player.h>
#include <Universe/Logistics/Inventory.h>
#include <Universe/Universe.h>

// temp for cube debugging
#include <Math/Noise/Simplex.hpp>

#include <Math/Sphere/Iconosphere.hpp>


// DEBUG

f32 g_debugAsteroidUVScale = 0.1f;

u32 g_debugNoiseOcts = 6;
f32 g_debugNoiseLunac = 2.f;
f32 g_debugNoisePersist = 0.55f;//0.4f;

namespace Miner
{
	namespace Picking
	{
		const f32 k_pickingDelay(0.1f);

		f32 remainingSecsTilPickEnabled(0.f);

		BuildingTypes::ENUM m_selectedBuildType = BuildingTypes::ENUM::k_none;
	}

	namespace Camera
	{
		const f32 k_cameraRotateSpeed(0.01f);
		const f32 k_cameraMoveSpeed(1.f);

		const f32 k_cameraRadius(5.f);
		const f32 k_invertY(-1.f);
		const f32 k_maxZoom(1.f);
		const f32 k_minZoom(0.25f);
	}


	namespace animation
	{
		const f32 kAsteroidRotationSpeed(0.25);
	}

	StateAsteroid::StateAsteroid()		
	{
		m_asteroidVerts.reserve(256);
	}

    void StateAsteroid::CreateSystems() noexcept
    {
		CreateSystem<CS::CkAudioPlayer>();
    }
	
	// TODO move to member
	void RegisterCustomCSObjects()
	{
		auto resPool = CS::Application::Get()->GetResourcePool();

		// register scroll view component
		CS::Application::Get()->GetSystem<CS::UIComponentFactory>()->Register<Miner::ScrollViewComponent>("ScrollView");

		// register scroll view csguidef
		CS::WidgetDefCSPtr scrollViewDef = resPool->LoadResource<CS::WidgetDef>(CS::StorageLocation::k_package, "UI/CustomCSUIDefinitions/ScrollView.csuidef");
		CS::Application::Get()->GetSystem<CS::WidgetFactory>()->RegisterDefinition(scrollViewDef);
	}


	void StateAsteroid::OnInit() noexcept
    {
		mpUniverse = std::shared_ptr<CUniverse>(new CUniverse(1337));

		m_asteroidEntity = mpUniverse->GetCurrentAsteroid();

		AddCamera();

		AddSkybox();

		LoadRecipes();

		OpenInputConnections();

		FactoryOrbitalObjects::SetScene(GetMainScene());

		AddAsteroid();

		AddLights();

		RegisterCustomCSObjects();
		
		CS::Canvas* const uiCanvas(GetUICanvas());

		// build inventory ui with new gui factory
		InventoryUIControllerUPtr inventoryMenuController = UIFactory::CreateInventory(uiCanvas);
		
		BuildMenuUIController::BuildingSelectCallback buildmenuCallback(std::bind(&StateAsteroid::BuildingSelected, this, std::placeholders::_1));
		BuildMenuUIControllerUPtr buildMenuController = UIFactory::CreateBuildMenu(uiCanvas, buildmenuCallback);

		// build hud
		m_hudUIController = UIFactory::CreateHUD(uiCanvas, std::move(buildMenuController), std::move(inventoryMenuController));
		
		LoadAudio();
    }

	void StateAsteroid::LoadAudio()
	{
		//auto resourcePool(CS::Application::Get()->GetResourcePool());
		//m_audioBank = resourcePool->LoadResource<CSAudio::CkBank>(CS::StorageLocation::k_package, "AudioBank.ckb");
		
		auto audioSystem( GetSystem<CS::CkAudioPlayer>() );
		audioSystem->PlayMusic(CS::StorageLocation::k_package, "Audio/Xtrullor-TerranEra.ogg");

		// TODO hook somewhere
		audioSystem->SetMusicVolume(0.0f);
	}
	
	void StateAsteroid::BuildingSelected(BuildingTypes::ENUM in_buildingType)
	{
		Picking::m_selectedBuildType = in_buildingType;

		m_placingBuilding = true;

		m_ghostPlacementEntity = FactoryAsteroidObjects::CreateGhost();
	}
	

	void StateAsteroid::AddCamera()
	{
		CS::Screen* screen(CS::Application::Get()->GetScreen());

		//Create the camera component
		m_cameraComponent = std::make_shared<CS::PerspectiveCameraComponent>(screen->GetResolution().x / screen->GetResolution().y, CS::MathUtils::k_pi / 2.0f, 1.0f, 100.0f);


		//create the camera entity and add the camera component
		m_camera = CS::Entity::Create();
		m_camera->AddComponent(m_cameraComponent);

		m_mouseHorizontalAngle = CS::MathUtils::k_pi;

		//add the camera to the scene
		GetMainScene()->Add(m_camera);

		UpdateZoomLevel(CS::Vector2::k_zero);
	}

	void StateAsteroid::AddLights()
	{
		CS::AmbientLightComponentSPtr ambientLightComponent(std::make_shared<CS::AmbientLightComponent>(CS::Colour(0.3f, 0.3f, 0.3f, 1.0f)));

		CS::EntitySPtr ambientLightEnt = CS::Entity::Create();
		ambientLightEnt->AddComponent(ambientLightComponent);

		GetMainScene()->Add(ambientLightEnt);

		CS::DirectionalLightComponentSPtr directionalLightComponent(std::make_shared<CS::DirectionalLightComponent>(CS::DirectionalLightComponent::ShadowQuality::k_high, CS::Colour::k_white));
		directionalLightComponent->SetColour(CS::Colour(0.5f, 0.4f, 0.4f, 1.0f));
		directionalLightComponent->SetShadowVolume(10.0f, 10.0f, 1.0f, 10.0f);
		directionalLightComponent->SetShadowTolerance(0.0005f);

		CS::EntitySPtr directionalLightEnt(CS::Entity::Create());
		directionalLightEnt->AddComponent(directionalLightComponent);
		directionalLightEnt->GetTransform().SetLookAt(CS::Vector3(-5.f, 5.f, -5.f), CS::Vector3::k_zero, CS::Vector3::k_unitPositiveY);

		GetMainScene()->Add(directionalLightEnt);
	}

	void StateAsteroid::AddSkybox()
	{
		//m_camera->AddEntity(FactoryAsteroidObjects::CreateSkybox(m_camera));
	}

	void StateAsteroid::LoadRecipes()
	{
		std::vector<std::string> k_recipePaths = {"Recipes/RecipeHQ.json"};

		for (u32 i(0); i < k_recipePaths.size(); ++i)
		{
			m_recipes.push_back(Recipes::CRecipe::Create(k_recipePaths[i]));
		}
	}


	//
	void StateAsteroid::AddAsteroid()
	{
		CS::ResourcePool* resourcePool(CS::Application::Get()->GetResourcePool());
		//CS::MaterialCSPtr material(resourcePool->LoadResource<CS::Material>(CS::StorageLocation::k_package, "Materials/asteroid.csmaterial"));
						
		// build texture data
		auto textureDimensions(MineableController::GetRemainingOreTextureDimensions());
		auto arraySize(textureDimensions.x * textureDimensions.y * 3);
		auto asteroidOreDensityData = new u8[arraySize];
		
//		double lowestVal = 0.0;
//		double highestVal = 0.0;

		// TODO - want to shift these amounts as a post process to correctly show how rich the ore is (ie shouldn't have 255 values barring exceptional cases)

		for (u32 i(0); i < u32(abs(textureDimensions.x))*u32(abs(textureDimensions.y)); ++i)
		{
			// vert pos is the same as normal, which we pass to asin
			//double u = asin((i%textureDimensions.x) / (f32)textureDimensions.x) / CS::MathUtils::k_pi + 0.5;
			//double v = asin((i/textureDimensions.x) / (f32)textureDimensions.x) / CS::MathUtils::k_pi + 0.5;
			double u = (i%textureDimensions.x) / (f32)textureDimensions.x;
			double v = (i/textureDimensions.x) / (f32)textureDimensions.y;
			
			// TODO remove, doubling uv so we can check wrapping
			//u *= 3.f;
			//v *= 3.f; // had these on at one point, couldn't see much difference

			//double val = ((Simplex::octaveNoise(u, v, 6, 2.0, 0.4) + 1.0) * 0.5) * 255;
			double val = Simplex::octaveNoise(u, v, g_debugNoiseOcts, g_debugNoiseLunac, g_debugNoisePersist);

			//CS_LOG_VERBOSE(CS::ToString(val));

			//val = ((val+1.0) * 0.5) * 255;
			// trying to get rid of what looks like wrap around values
			val = (val +1.2)* 0.5 * 255.0;
			if (val > 255.0)
				val = 255.0;

			u32 x(i * 3);

			asteroidOreDensityData[x] = (u8(val));
			asteroidOreDensityData[x + 1] = (u8(val));
			asteroidOreDensityData[x + 2] = (u8(val));

			// track high/low vals for normalisation
//			if (val < lowestVal)
//				lowestVal = val;
//			if (val > highestVal)
//				highestVal = val;
		}

		// Normalisation pass - not super sure why this is needed but we get values outside of the 0-1 range
//		double rangeScale = 255.0 / (highestVal - lowestVal);
//		double baseInc = -lowestVal;

		auto asteroidOreBDensityData = new u8[arraySize];
		for (u32 i(0); i < u32(abs(textureDimensions.x))*u32(abs(textureDimensions.y)); ++i)
		{
			double u = (i%textureDimensions.x) / (f32)textureDimensions.x;
			double v = (i / textureDimensions.x) / (f32)textureDimensions.y;
			double val = Simplex::octaveNoise(u, v, g_debugNoiseOcts*0.9, g_debugNoiseLunac*0.9, g_debugNoisePersist*0.9);

			// trying to get rid of what looks like wrap around values
			val = (val + 1.2)* 0.5 * 255.0;
			if (val > 255.0)
				val = 255.0;

			u32 x(i * 3);
			asteroidOreBDensityData[x] = (u8(val));
			asteroidOreBDensityData[x + 1] = (u8(val));
			asteroidOreBDensityData[x + 2] = (u8(val));
		}
				
		//m_asteroidEntity = FactoryAsteroidObjects::CreateCube(material); // (5, 2.0, 0.4)

		// create basic mat to overwrite later
		CS::Colour whiteColour(CS::Colour::k_white);
		auto tempMaterialName = "_PrimitiveStaticBlinnColour(" + CS::ToString(CS::Colour::k_white) + ")";
		auto material = resourcePool->GetResource<CS::Material>(tempMaterialName);

		if (material == nullptr)
		{
			const CS::Colour specularColour(0.5f, 0.5f, 0.5f, 1.0f);
			const f32 shininess = 10.0f;

			auto materialFactory = CS::Application::Get()->GetSystem<CS::MaterialFactory>();
			CS_ASSERT(materialFactory, "StateAsteroid could not find required app system: MaterialFactory");

			auto texture = resourcePool->LoadResource<CS::Texture>(CS::StorageLocation::k_chilliSource, "Textures/Blank.csimage");
			material = materialFactory->CreateBlinn(tempMaterialName, texture, CS::Colour::k_black, whiteColour, whiteColour, specularColour, shininess);
		}
		
		// Build asteroid
		m_asteroidEntity = FactoryAsteroidObjects::CreateAsteroid(m_asteroidVerts, material, IsoSphere::NoiseDescription(g_debugNoiseOcts, g_debugNoiseLunac, g_debugNoisePersist), g_debugAsteroidUVScale);
		
		// plane stuff for debugging
		//m_planeSimplex = FactoryAsteroidObjects::CreatePlane(loadedMaterial);
		//m_planeSimplex->GetTransform().SetPosition(2.f, 0.f, 0.f);

		//GetScene()->Add(m_planeSimplex);

		//GetScene()->Add(FactoryAsteroidObjects::CreateLine(CS::Vector3(-1.f, 0.f, 0.f), CS::Vector3(-1.f, 1.f, 0.f), 0.02f));
		//GetScene()->Add(FactoryAsteroidObjects::CreateLine(CS::Vector3(-1.f, 1.f, 0.f), CS::Vector3(1.f, 1.f, 0.f), 0.02f));
		//GetScene()->Add(FactoryAsteroidObjects::CreateLine(CS::Vector3(1.f, 1.f, 0.f), CS::Vector3(1.f, -1.f, 0.f), 0.02f));

	//	think were building the returned verts incorrectly, model shows right, frame mesh is bust though

		// draw lines for retrieved vert mesh
		/*for (u32 i(0); i < (m_asteroidVerts.size())-3; i += 3)
		//	for (u32 i(0); i < 24; i += 3)
		{	
			GetScene()->Add(FactoryAsteroidObjects::CreateLine(m_asteroidVerts[i], m_asteroidVerts[i+1], 0.02f));
			GetScene()->Add(FactoryAsteroidObjects::CreateLine(m_asteroidVerts[i+1], m_asteroidVerts[i + 2], 0.02f));
			GetScene()->Add(FactoryAsteroidObjects::CreateLine(m_asteroidVerts[i+2], m_asteroidVerts[i], 0.02f));
		}*/
		
		//add the sprite to the scene
		GetMainScene()->Add(m_asteroidEntity);

		m_asteroidController = m_asteroidEntity->GetComponent<CAsteroidController>();

		// TODO REMOVE
		if (m_asteroidController == nullptr)
		{
			m_asteroidController = std::make_shared<CAsteroidController>();
			m_asteroidEntity->AddComponent(m_asteroidController);
		}

		CS_ASSERT(m_asteroidController != nullptr && "test", "Couldn't find asteroid controller");

		m_asteroidMineableController = m_asteroidEntity->GetComponent<MineableController>();

		CS_ASSERT(m_asteroidMineableController != nullptr && "test", "Couldn't find asteroid mineable controller");
		
		// TODO dunno how we're generating this
		const u32 tempStartingOreAmount(10000);
						
		m_asteroidMineableController->SetRemainingOreData(Logistics::Item::ORE_A, tempStartingOreAmount, std::unique_ptr<u8[]>(asteroidOreDensityData), arraySize);
		m_asteroidMineableController->SetRemainingOreData(Logistics::Item::ORE_B, tempStartingOreAmount, std::unique_ptr<u8[]>(asteroidOreBDensityData), arraySize);
		m_asteroidMineableController->SetRemainingOreTextureDimensions(textureDimensions);
	}


	/*f32 StateAsteroid::GetTextureUVFromAsteroidPoint(CS::Vector3 in_point)
	{
		CS::Vector2 uv(GetAsteroidTextureCoord(in_point));

		u32 width(m_asteroidOverayTextureDimensions.x);
		u32 height(m_asteroidOverayTextureDimensions.y);

		u32 pixelX(uv.x * width);
		u32 pixelY(uv.y * height);

		u32 pixelStart((pixelY * width + pixelX) * 3);

		u8 red (m_asteroidOverlayTexture[pixelStart]);
		u8 green(m_asteroidOverlayTexture[pixelStart + 1]);
		u8 blue(m_asteroidOverlayTexture[pixelStart + 2]);

		CS_LOG_VERBOSE("r " + CS::ToString(red) + ", g " + CS::ToString(green) + ", b " + CS::ToString(blue));

		return m_asteroidOverayTextureDimensions.x * uv.x;
	}*/




	void StateAsteroid::OnUpdate(f32 in_deltaTime) noexcept
	{
		mpUniverse->Update(in_deltaTime);

		// asteroid rotation
	//	m_asteroidEntity->GetTransform().RotateBy(CS::Vector3::k_unitPositiveY, animation::kAsteroidRotationSpeed * in_deltaTime);

		if (m_asteroidController)
		{
			m_hudUIController->UpdateCurrencyLabel();
		}

		// handle ghost building pointer following
		if (m_placingBuilding)
		{
			// need to pick for position
			CS::Ray raycast(m_cameraComponent->Unproject(m_lastPointerPosition));
			CS::Vector3 hitPoint, normal;

			if (PickPointOnAsteroid(raycast, hitPoint, normal))
			{
				m_lastPickPosition = hitPoint;
				m_hitTriangleNormal = normal;
			}

			if (m_ghostPlacementEntity->GetParent() != nullptr)
				m_asteroidEntity->RemoveEntity(m_ghostPlacementEntity.get());
			
			m_ghostPlacementEntity->GetTransform().SetPosition(m_lastPickPosition);

			f32 genScale(0.05f);

			// position it in world space
			m_ghostPlacementEntity->GetTransform().ScaleTo(genScale);
			m_ghostPlacementEntity->GetTransform().SetLookAt(m_lastPickPosition, m_lastPickPosition + m_hitTriangleNormal, CS::Vector3::k_unitPositiveY);
			//m_ghostPlacementEntity->GetTransform().RotateTo(CS::Vector3::k_unitPositiveX, CS::MathUtils::DegToRad(-90.f));

			// get transform in parent-space
			auto localTransform(m_ghostPlacementEntity->GetTransform().GetWorldTransform() * CS::Matrix4::Inverse(m_asteroidEntity->GetTransform().GetWorldTransform()));
			m_ghostPlacementEntity->GetTransform().SetLocalTransform(localTransform);

			if (m_ghostPlacementEntity->GetParent() == nullptr)
			{
				m_asteroidEntity->AddEntity(m_ghostPlacementEntity);
			}
		}

		//
		m_asteroidMineableController->RebuildRemainingOreTexture();		
    }
	
	void StateAsteroid::OnDestroy() noexcept
    {
		m_pointerDownConnection.reset();
		m_pointerMovedConnection.reset();
		m_pointerUpConnection.reset();
		m_pointerScrollConnection.reset();

		m_keyReleasedConnection.reset();

		// only for DEBUG rendering cleanup <<<
		FactoryAsteroidObjects::DestroyFactory();
    }

	//-----------------------------------------------------------
	/// Opens connections for pointer movement, clicks and scroll
	///
	/// @author snik
	//-----------------------------------------------------------
	void StateAsteroid::OpenInputConnections()
	{
		CS::PointerSystem* pointerSystem = CS::Application::Get()->GetSystem<CS::PointerSystem>();
		CS::Keyboard* keyboardSystem(CS::Application::Get()->GetSystem<CS::Keyboard>());

		m_keyReleasedConnection = keyboardSystem->GetKeyReleasedEvent().OpenConnection([this](CS::KeyCode in_key)
		{
			if (in_key == CS::KeyCode::k_r)
			{
				//g_debugAsteroidUVScale += 0.1f;

				m_asteroidEntity->RemoveFromParent();
				m_planeSimplex->RemoveFromParent();

				AddAsteroid();
				// rebuild asteroid or mess with simplex settings
			}
			else if (in_key == CS::KeyCode::k_a)
			{
				++g_debugNoiseOcts;
				CS_LOG_VERBOSE("g_debugNoiseOcts " + CS::ToString(g_debugNoiseOcts));
			}
			else if (in_key == CS::KeyCode::k_b)
			{
				g_debugNoiseLunac += 0.025;
				CS_LOG_VERBOSE("g_debugNoiseLunac " + CS::ToString(g_debugNoiseLunac));
			}
			else if (in_key == CS::KeyCode::k_c)
			{
				// kinda looks like contrast or noise
				g_debugNoisePersist += 0.025;
				CS_LOG_VERBOSE("g_debugNoisePersist " + CS::ToString(g_debugNoisePersist));
			}
			else if (in_key == CS::KeyCode::k_escape)
			{
				m_hudUIController->EnableBuildMenu(false);
			}
		});


		// handle pointermove
		m_pointerMovedConnection = pointerSystem->GetPointerMovedEvent().OpenConnection([this](const CS::Pointer& in_pointer, f64 in_timestamp)
		{
			if (m_rmbDown)
			{
				UpdateOrbitalCameraPosition(in_pointer.GetPosition() - in_pointer.GetPreviousPosition());
				//CS_LOG_VERBOSE("Pointer " + CS::ToString(in_pointer.GetPosition()));
			}

			m_lastPointerPosition = in_pointer.GetPosition();
		});
		
		// handle pointerdown
		m_pointerDownConnection = pointerSystem->GetPointerDownEventFiltered().OpenConnection([this](const CS::Pointer& in_pointer, f64 in_timestamp, CS::Pointer::InputType in_inputType)
		{
			if (in_inputType == CS::Pointer::InputType::k_leftMouseButton)
			{
				// need to pick for position
				CS::Ray raycast(m_cameraComponent->Unproject(in_pointer.GetPosition()));

				SelectOrPlaceOnAsteroid(raycast);
			}
			else if (in_inputType == CS::Pointer::InputType::k_rightMouseButton)
			{
				m_rmbDown = true;
			}

			/*float entry, exit; // distance down ray?
			if (CS::ShapeIntersection::Intersects(meshComp->GetAABB(), raycast, entry, exit) == true)
			{
				CS_LOG_VERBOSE("hit.");
			}
			else
			{
				CS_LOG_VERBOSE("miss.");
			}*/
		});
		
		// handle pointerup
		m_pointerUpConnection = pointerSystem->GetPointerUpEventFiltered().OpenConnection([this](const CS::Pointer& in_pointer, f64 in_timestamp, CS::Pointer::InputType in_inputType)
		{
			if (in_inputType == CS::Pointer::InputType::k_rightMouseButton)
			{
				m_rmbDown = false;
			}
		});

		// handle scrollwheel
		m_pointerScrollConnection = pointerSystem->GetPointerScrollEventFiltered().OpenConnection([this](const CS::Pointer& in_pointer, f64 in_timestamp, const CS::Vector2& in_something)
		{
			//CS_LOG_VERBOSE("scroll " + CS::ToString(in_something));
			UpdateZoomLevel(in_something);
		});
	}

	void StateAsteroid::UpdateZoomLevel(const CS::Vector2& in_scrollDelta)
	{
		m_cameraZoom += in_scrollDelta.y * -0.05f;

		m_cameraZoom = CS::MathUtils::Clamp(m_cameraZoom, Camera::k_minZoom, Camera::k_maxZoom);

		UpdateOrbitalCameraPosition(CS::Vector2::k_zero);
	}

	void StateAsteroid::UpdateOrbitalCameraPosition(const CS::Vector2& in_positionDiff)
	{
		m_mouseHorizontalAngle += Camera::k_cameraRotateSpeed * in_positionDiff.x * std::fmaxf( m_cameraZoom/ Camera::k_maxZoom, 0.01f);
		m_mouseVerticalAngle += Camera::k_cameraRotateSpeed * in_positionDiff.y * std::fmaxf( m_cameraZoom/ Camera::k_maxZoom, 0.01f) * Camera::k_invertY;

		const float k_halfPi = CS::MathUtils::k_pi*0.5f;

		if (m_mouseVerticalAngle > k_halfPi)
			m_mouseVerticalAngle = k_halfPi;
		else if (m_mouseVerticalAngle < -k_halfPi)
			m_mouseVerticalAngle = -k_halfPi;

		CS::Vector3 campos;
		
		campos.x = sin(m_mouseHorizontalAngle) * Camera::k_cameraRadius * m_cameraZoom;
		campos.y = sin(m_mouseVerticalAngle) * Camera::k_cameraRadius * m_cameraZoom;
		campos.z = cos(m_mouseHorizontalAngle) * Camera::k_cameraRadius * m_cameraZoom;
		
		m_camera->GetTransform().SetLookAt(campos, CS::Vector3::k_zero, CS::Vector3::k_unitPositiveY);
	}

	void StateAsteroid::SelectOrPlaceOnAsteroid(CS::Ray in_screenRay)
	{
		// run intersect check against all built buildings using spheretest
		for (CS::EntityWPtr builtObject : m_builtEntities)
		{
			if (builtObject.expired())
				continue;

			CS::EntitySPtr obj = builtObject.lock();

			CS::Vector3 pos(obj->GetTransform().GetWorldPosition());
			float radius(0.2f);

			CS::Sphere sphere(pos, radius);

			if (CS::ShapeIntersection::Intersects(sphere, in_screenRay))
			{
				CS_LOG_VERBOSE("Hit existing thing!");

				// TODO: push gui for selected thing
				return;
			}
		}

		CS::Vector3 hitPoint, normal;


		// do uv conversion
		PickPointOnAsteroid(in_screenRay, hitPoint, normal);
		//GetTextureUVFromAsteroidPoint(hitPoint);


		// do asteroid mesh picking
		if (Picking::m_selectedBuildType != BuildingTypes::ENUM::k_none &&
			PickPointOnAsteroid(in_screenRay, hitPoint, normal))
		{
			BuildingHQWPtr hq = m_asteroidController->GetHQForPlayer(mpUniverse->GetPlayer()->GetID());

			CS_ASSERT(Picking::m_selectedBuildType == BuildingTypes::ENUM::k_HQ || hq.lock() != nullptr, "Trying to build something but there's no HQ on this asteroid");
			
			CS::EntitySPtr building = FactoryAsteroidObjects::Create(Picking::m_selectedBuildType, mpUniverse->GetPlayer()->GetID(), hq);

			// need to register hqs to players (for multiple players on same asteroid)
			if (Picking::m_selectedBuildType == BuildingTypes::ENUM::k_HQ)
			{
				m_asteroidController->RegisterHQ(building);

				// pull hq now it's registered
				hq = m_asteroidController->GetHQForPlayer(mpUniverse->GetPlayer()->GetID());

				if (hq.expired() == false)
				{
					m_hudUIController->SetHQ(hq);
				}
			}
			
			CS_LOG_VERBOSE("hitpos " + CS::ToString(hitPoint));

			
			// position it in world space
			//normal = CS::Vector3::Rotate(normal, CS::Quaternion::GenericQuaternion(CS::Vector3::k_unitNegativeX*90.f, CS::Vector3::k_zero, CS::Vector3::k_zero));
			building->GetTransform().SetLookAt(hitPoint, hitPoint + normal, CS::Vector3::k_unitPositiveY);
			//building->GetTransform().set(//RotateXBy(CS::MathUtils::DegToRad(-90.f));
		
			// get transform in parent-space
			auto localTransform(building->GetTransform().GetWorldTransform() * CS::Matrix4::Inverse(m_asteroidEntity->GetTransform().GetWorldTransform()));
			building->GetTransform().SetLocalTransform(localTransform);

			m_asteroidEntity->AddEntity(building);

			// send build complete message if building is derrived from CBuildingController
			auto buildingComponent = building->GetComponent<CBuildingController>();

			if (buildingComponent != nullptr)
				buildingComponent->OnBuilt();

			Picking::m_selectedBuildType = BuildingTypes::ENUM::k_none;
			m_placingBuilding = false;

			// remove ghost entity
			if (m_ghostPlacementEntity->GetParent() != nullptr)
				m_asteroidEntity->RemoveEntity(m_ghostPlacementEntity.get());
		}
	}


	bool StateAsteroid::PickPointOnAsteroid(const CS::Ray& in_screenRay, CS::Vector3& out_hitPos, CS::Vector3& out_hitNormal)
	{
		// do asteroid mesh picking
		return PickingTools::Pick(
			in_screenRay,
			m_camera->GetTransform().GetWorldPosition(),
			m_asteroidEntity->GetTransform().GetWorldTransform(),
			m_asteroidVerts,
			out_hitPos,
			out_hitNormal);
	}

	void BuildTriangleMap(const u16* in_indexBuffer, u32 in_indexCount)
	{
		// get tri count

		//Simplex::octaveNoise(u, v, in_octaves, in_lacunarity, in_persistence)

		// get random colours per tri

		u32* colour(new u32[in_indexCount]);

		for (u32 i(0); i < in_indexCount; ++i)
		{
			colour[i] = rand();
		}

		// apply to index buffer
		// send buffer as uniform

		// vert shader -
		// get current index - in int gl_VertexID;
	}
}
