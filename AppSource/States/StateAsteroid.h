//
//  State.h
//  Miner
//  Created by Ian Copland on 15/10/2014.
//
//  The MIT License (MIT)
//
//  Copyright (c) 2014 Tag Games Limited
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#ifndef _MINER_STATEASTEROID_H_
#define _MINER_STATEASTEROID_H_

#include <ChilliSource/Audio/ForwardDeclarations.h>
#include <ChilliSource/ChilliSource.h>
#include <ChilliSource/Core/Math/Vector3.h>
#include <ChilliSource/Core/State.h>
#include <ChilliSource/Input/Pointer.h>

#include <GUI/Controllers/InventoryUIController.h>
#include <GUI/Controllers/BuildMenuUIController.h>
#include <GUI/Controllers/HUDUIController.h>


#include <Universe/Factories/FactoryAsteroidObjects.h>

#include <Recipes/Recipe.h> // make a forward declare


class CUniverse;
class CAsteroidController;
class MineableController;


namespace Miner
{
	class StateAsteroid : public CS::State, private BuildMenuUIController::IBuildMenuSelection
    {
    public:
		StateAsteroid();
        void CreateSystems() noexcept override;
        void OnInit() noexcept override;
        void OnUpdate(f32 in_deltaTime) noexcept override;
        void OnDestroy() noexcept override;

	private:
		void AddCamera();
		void AddSkybox();
		void OpenInputConnections();
		void AddAsteroid();
		void AddLights();

		void LoadRecipes();

		void LoadAudio();
				
		void UpdateOrbitalCameraPosition(const CS::Vector2& in_positionDiff);

		void UpdateZoomLevel(const CS::Vector2& in_scrollDelta);

		void SelectOrPlaceOnAsteroid(CS::Ray in_screenRay);

		bool PickPointOnAsteroid(const CS::Ray& in_screenRay, CS::Vector3& out_hitPos, CS::Vector3& out_hitNormal);
		
		void BuildingSelected(BuildingTypes::ENUM in_enum) override;
		
		//f32 GetTextureUVFromAsteroidPoint(CS::Vector3 in_point);
	//	CS::Vector2 GetAsteroidTextureCoord(const CS::Vector3& in_point);

		//void MineAtUV(const CS::Vector3& in_point);

	private:
		CS::EventConnectionUPtr m_pointerDownConnection;
		CS::EventConnectionUPtr m_pointerMovedConnection;
		CS::EventConnectionUPtr m_pointerUpConnection;
		CS::EventConnectionUPtr m_pointerScrollConnection;

		CS::EventConnectionUPtr m_keyReleasedConnection;

		CS::EventConnectionUPtr m_buttonBuildMenuConnection;
		CS::EventConnectionUPtr m_buttonInventoryMenuConnection;

		CS::EventConnectionUPtr m_buttonInventoryMenuCloseConnection;

		CS::EventConnectionUPtr m_buttonHQSelectConnection;
		CS::EventConnectionUPtr m_buttonDrillSelectConnection;
		CS::EventConnectionUPtr m_buttonGeneratorSelectConnection;
		CS::EventConnectionUPtr m_buttonMissileSiloSelectConnection;

		std::shared_ptr<CUniverse> mpUniverse;

		CS::EntitySPtr m_asteroidEntity;

		CS::EntitySPtr m_planeSimplex;

		CS::EntitySPtr m_ghostPlacementEntity;

		CS::Vector2 m_lastPointerPosition;

		CS::Vector3 m_lastPickPosition;

		CS::Vector3 m_hitTriangleNormal;

		std::vector<CS::EntityWPtr> m_builtEntities;

		CS::EntitySPtr m_camera;
		CS::CameraComponentSPtr m_cameraComponent;

		std::shared_ptr<CAsteroidController> m_asteroidController;
		std::shared_ptr<MineableController> m_asteroidMineableController;

		CS::CkBankCSPtr m_audioBank;
		//CSAudio::CkSoundUPtr m_ambientNoise;

		std::vector<Recipes::CRecipe> m_recipes;

		CS::Integer2 m_asteroidOverayTextureDimensions;
		
		// -- ui
	private:

		HUDUIControllerUPtr m_hudUIController;

		CS::WidgetSPtr m_currencyLabel;

		CS::WidgetSPtr m_hudWidget;
		CS::WidgetSPtr m_buildMenu;
		CS::WidgetSPtr m_inventoryMenu;
		CS::WidgetSPtr m_buttonHudBuild;
		CS::WidgetSPtr m_buttonHudInventory;

		CS::WidgetSPtr m_buildButtonHQ;
		CS::WidgetSPtr m_buildButtonDrill;
		CS::WidgetSPtr m_buildButtonGenerator;
		CS::WidgetSPtr m_buildButtonMissileSilo;

		CS::WidgetSPtr m_inventoryButtonClose;

	private:

		f32 m_cameraZoom = 0.f;
		f32 m_mouseHorizontalAngle = 3.1415926536f;
		f32 m_mouseVerticalAngle = 0.f;
		
		std::vector<CS::Vector3> m_asteroidVerts;
		
		bool m_rmbDown = false;
		bool m_placingBuilding = false;
    };
}

#endif
