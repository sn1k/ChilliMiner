

#include <States/StateTest.h>

#include <ChilliSource/Core/Base.h>
#include <ChilliSource/Core/Entity.h>
#include <ChilliSource/Core/Math.h>
#include <ChilliSource/Core/Resource.h>
#include <ChilliSource/Core/Scene.h>

#include <ChilliSource/Rendering/Base.h>
#include <ChilliSource/Rendering/Camera.h>
#include <ChilliSource/Rendering/Lighting.h>
#include <ChilliSource/Rendering/Model.h>
//#include <ChilliSource/Rendering/Model/StaticModelComponent.h>

#include <ChilliSource/Input/Pointer.h>


namespace Miner
{

	namespace Camera
	{
		const f32 k_cameraRotateSpeed(0.01f);
		const f32 k_cameraMoveSpeed(1.f);

		const f32 k_cameraRadius(5.f);
		const f32 k_invertY(-1.f);
		const f32 k_maxZoom(1.f);
		const f32 k_minZoom(0.25f);
	}


    void StateTest::CreateSystems()
    {
        //Add systems here.
    }
	        
	void StateTest::OnInit()
    {
		AddCamera();

		OpenInputConnections();

		BuildSphere();

		AddLights();
   }
	

	void StateTest::AddCamera()
	{
		CS::Screen* screen(CS::Application::Get()->GetScreen());

		//Create the camera component
		m_cameraComponent = std::make_shared<CS::PerspectiveCameraComponent>(screen->GetResolution().x / screen->GetResolution().y, CS::MathUtils::k_pi / 2.0f, 1.0f, 100.0f);

		//create the camera entity and add the camera component
		m_camera = CS::Entity::Create();
		m_camera->AddComponent(m_cameraComponent);

		m_mouseHorizontalAngle = CS::MathUtils::k_pi;

		//add the camera to the scene
		GetMainScene()->Add(m_camera);

		UpdateZoomLevel(CS::Vector2::k_zero);
	}

	void StateTest::AddLights()
	{
		CS::AmbientLightComponentSPtr ambientLightComponent(std::make_shared<CS::AmbientLightComponent>(CS::Colour(0.3f, 0.3f, 0.3f, 1.0f)));

		CS::EntitySPtr ambientLightEnt = CS::Entity::Create();
		ambientLightEnt->AddComponent(ambientLightComponent);

		GetMainScene()->Add(ambientLightEnt);

		CS::DirectionalLightComponentSPtr directionalLightComponent(std::make_shared<CS::DirectionalLightComponent>(CS::DirectionalLightComponent::ShadowQuality::k_high, CS::Colour::k_white));
		directionalLightComponent->SetColour(CS::Colour(0.5f, 0.4f, 0.4f, 1.0f));
		directionalLightComponent->SetShadowVolume(70.0f, 70.0f, 1.0f, 70.0f);
		directionalLightComponent->SetShadowTolerance(0.005f);

		CS::EntitySPtr directionalLightEnt = CS::Entity::Create();
		directionalLightEnt->AddComponent(directionalLightComponent);
		directionalLightEnt->GetTransform().SetLookAt(CS::Vector3(-5.f, 5.f, -5.f), CS::Vector3::k_zero, CS::Vector3::k_unitPositiveY);

		GetMainScene()->Add(directionalLightEnt);
	}

	//
	void StateTest::BuildSphere()
	{

	}


	void StateTest::OnUpdate(f32 in_deltaTime)
    {
		//m_asteroidEntity->GetTransform().RotateBy(CS::Vector3::k_unitPositiveY, animation::kAsteroidRotationSpeed * in_deltaTime);
    }

	void StateTest::OnDestroy()
    {
		m_pointerDownConnection.reset();
		m_pointerMovedConnection.reset();
		m_pointerUpConnection.reset();
    }

	void StateTest::OpenInputConnections()
	{
		CS::PointerSystem* pointerSystem = CS::Application::Get()->GetSystem<CS::PointerSystem>();

		// handle pointermove
		m_pointerMovedConnection = pointerSystem->GetPointerMovedEvent().OpenConnection([this](const CS::Pointer& in_pointer, f64 in_timestamp)
		{
			UpdateOrbitalCameraPosition(in_pointer.GetPosition() - in_pointer.GetPreviousPosition());
		//	CS_LOG_VERBOSE("Pointer #" + CS::ToString(in_pointer.GetId()) + " Moved.");
		});

		// handle pointerdown
		m_pointerDownConnection = pointerSystem->GetPointerDownEventFiltered().OpenConnection([this](const CS::Pointer& in_pointer, f64 in_timestamp, CS::Pointer::InputType in_inputType)
		{

		});
		
		// handle pointerup
		m_pointerUpConnection = pointerSystem->GetPointerUpEventFiltered().OpenConnection([](const CS::Pointer& in_pointer, f64 in_timestamp, CS::Pointer::InputType in_inputType)
		{
		//	CS_LOG_VERBOSE("Pointer #" + CS::ToString(in_pointer.GetId()) + " Up.");
		});

		// handle scrollwheel
		m_pointerScrollConnection = pointerSystem->GetPointerScrollEventFiltered().OpenConnection([this](const CS::Pointer& in_pointer, f64 in_timestamp, const CS::Vector2& in_something)
		{
		//	CS_LOG_VERBOSE("Pointer #" + CS::ToString(in_pointer.GetId()) + " Scroll");
			UpdateZoomLevel(in_something);
		});
	}


	void StateTest::UpdateZoomLevel(const CS::Vector2& in_scrollDelta)
	{
		m_cameraZoom += in_scrollDelta.y * -0.05f;

		m_cameraZoom = CS::MathUtils::Clamp(m_cameraZoom, Camera::k_minZoom, Camera::k_maxZoom);

		UpdateOrbitalCameraPosition(CS::Vector2::k_zero);
	}


	void StateTest::UpdateOrbitalCameraPosition(const CS::Vector2& in_positionDiff)
	{
		m_mouseHorizontalAngle += Camera::k_cameraRotateSpeed * in_positionDiff.x * (Camera::k_maxZoom - m_cameraZoom);
		m_mouseVerticalAngle += Camera::k_cameraRotateSpeed * in_positionDiff.y * (Camera::k_maxZoom - m_cameraZoom) * Camera::k_invertY;

		const float k_halfPi = CS::MathUtils::k_pi*0.5f;

		if (m_mouseVerticalAngle > k_halfPi)
			m_mouseVerticalAngle = k_halfPi;
		else if (m_mouseVerticalAngle < -k_halfPi)
			m_mouseVerticalAngle = -k_halfPi;

		CS::Vector3 campos;
		
		campos.x = sin(m_mouseHorizontalAngle) * Camera::k_cameraRadius * m_cameraZoom;
		campos.y = sin(m_mouseVerticalAngle) * Camera::k_cameraRadius * m_cameraZoom;
		campos.z = cos(m_mouseHorizontalAngle) * Camera::k_cameraRadius * m_cameraZoom;
		
		m_camera->GetTransform().SetLookAt(campos, CS::Vector3::k_zero, CS::Vector3::k_unitPositiveY);
	}
	
}
