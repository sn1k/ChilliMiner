//
//  App.cpp
//  Miner
//  Created by Ian Copland on 15/10/2014.
//
//  The MIT License (MIT)
//
//  Copyright (c) 2014 Tag Games Limited
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#include <App.h>

#include <States/StateAsteroid.h>

// app systems
#include <ChilliSource/Audio/CricketAudio.h>

#include <ChilliSource/Core/Base/Application.h>
#include <ChilliSource/Core/Entity/PrimitiveEntityFactory.h>
#include <ChilliSource/Core/Threading/TaskScheduler.h>
#include <ChilliSource/UI/Base/WidgetFactory.h>
//#include <ChilliSource/UI/Base/ComponentFactory.h> // dunno where this went

// debug
#include <States/StateMap.h>

#include <ChilliSource/Rendering/Model.h>
#include <ChilliSource/Rendering/Material.h>

// steam include
#include <steam/steam_api.h>
#include <steam/isteamutils.h>

#include <windows.h>

// using steeeeeam
//#define USE_STEAM


#ifdef USE_STEAM

	// steam auth, achievements
	#define USE_GS_AUTH_API

	#ifdef STEAM_CEG
		// Steam DRM header file
		#include "cegclient.h"
	#else
		#define Steamworks_InitCEGLibrary() (true)
		#define Steamworks_TermCEGLibrary()
		#define Steamworks_TestSecret()
		#define Steamworks_SelfCheck()
	#endif

#endif


#if defined(CS_TARGETPLATFORM_ANDROID) && defined(CS_ANDROIDFLAVOUR_GOOGLEPLAY)

std::string GetGooglePlayLvlPublicKey() noexcept
{
	//Enter your Google Play LVL public key here if you are building for Google Play on Android
	return "";
}

#endif


CS::Application* CreateApplication(CS::SystemInfoCUPtr systemInfo) noexcept
{
	return new Miner::App(std::move(systemInfo));
}


int Alert(const char *lpCaption, const char *lpText)
{
#ifndef _WIN32
	fprintf(stderr, "Message: '%s', Detail: '%s'\n", lpCaption, lpText);
	return 0;
#else
	return ::MessageBox(NULL, (LPCWSTR)lpText, (LPCWSTR)lpCaption, MB_OK);
#endif
}

namespace Miner
{
	App::App(CS::SystemInfoCUPtr systemInfo) noexcept
		: Application(std::move(systemInfo))
	{}

    void App::CreateSystems() noexcept
	{
		CreateSystem<CS::CricketAudioSystem>();
		//CreateSystem<CSAudio::CkBankProvider>();

		CreateSystem<CS::CSModelProvider>();
		CreateSystem<CS::PrimitiveEntityFactory>();
		CreateSystem<CS::PrimitiveModelFactory>();
		CreateSystem<CS::WidgetFactory>();
		//CreateSystem<CS::ComponentFactory>(); // for scrollview?
    }

	void App::InitSteam()
	{
		#ifdef USE_STEAM
			CS_LOG_ERROR("[App.cpp] Doing steam initialisation");

			if (SteamAPI_RestartAppIfNecessary(k_uAppIdInvalid))
			{
				// if Steam is not running or the game wasn't started through Steam, SteamAPI_RestartAppIfNecessary starts the 
				// local Steam client and also launches this game again.

				// Once you get a public Steam AppID assigned for this game, you need to replace k_uAppIdInvalid with it and
				// removed steam_appid.txt from the game depot.
				CS_LOG_ERROR("Couldn't  find running instance of Steam, starting Steam now and restarting app");
				return;
			}


			// Init Steam CEG
			if (!Steamworks_InitCEGLibrary())
			{
				Alert("Fatal Error", "Steam must be running to play this game (InitDrmLibrary() failed).\n");

				CS_LOG_ERROR("Steamworks_InitCEGLibrary() failed\n");
				return;
			}

			// Initialize SteamAPI, if this fails we bail out since we depend on Steam for lots of stuff.
			// You don't necessarily have to though if you write your code to check whether all the Steam
			// interfaces are NULL before using them and provide alternate paths when they are unavailable.
			//
			// This will also load the in-game steam overlay dll into your process.  That dll is normally
			// injected by steam when it launches games, but by calling this you cause it to always load,
			// even when not launched via steam.
			if (!SteamAPI_Init())
			{
				Alert("Fatal Error", "Steam must be running to play this game (SteamAPI_Init() failed).\n");

				CS_LOG_ERROR("Steam must be running to play this game (SteamAPI_Init() failed).\n");
				return;
			}

			CS_LOG_ERROR("[App.cpp] Steam initialisation complete");

			if (SteamUser() == nullptr)
			{
				CS_LOG_ERROR("[App.cpp] Not logged into steam");
			}
			else
			{
				if (SteamUser()->BLoggedOn())
				{
					CSteamID m_SteamIDLocalUser = SteamUser()->GetSteamID();

					CS_LOG_ERROR("[App.cpp] Steam user logged on: " + CS::ToString(m_SteamIDLocalUser.GetAccountID()) + " " + SteamFriends()->GetPersonaName());
				}
				else
				{
					Alert("Fatal Error", "Steam user must be logged in to play this game (SteamUser()->BLoggedOn() returned false).\n");
				}
			}

			// do a DRM self check
			Steamworks_SelfCheck();

			// test a user specific secret
			Steamworks_TestSecret();
		#endif
	}

	void App::OnInit() noexcept
	{
		//initialisation stuff here.
		InitSteam();
    }

    void App::PushInitialState() noexcept
    {
        GetStateManager()->Push(CS::StateSPtr(new StateAsteroid()));
		//GetStateManager()->Push(CS::StateSPtr(new StateMap()));
    }

    void App::OnDestroy() noexcept
    {
        //destruction stuff here.

		DestroySteam();
    }

	void App::DestroySteam()
	{
		#ifdef USE_STEAM
			// Shutdown the SteamAPI
			SteamAPI_Shutdown();

			// Shutdown Steam CEG
			Steamworks_TermCEGLibrary();
		#endif
	}
}

