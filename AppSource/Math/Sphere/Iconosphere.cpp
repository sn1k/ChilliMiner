
#include <Math/Sphere/Iconosphere.hpp>

#include <memory>

#include <ChilliSource/Core/Base/Application.h>
#include <ChilliSource/Core/Entity/Entity.h>
#include <ChilliSource/Core/Entity/PrimitiveEntityFactory.h>
#include <ChilliSource/Core/Math/MathUtils.h>
#include <ChilliSource/Core/Resource/ResourcePool.h>

#include <ChilliSource/Rendering/Material/Material.h>
#include <ChilliSource/Rendering/Material/MaterialFactory.h>
#include <ChilliSource/Rendering/Texture/Texture.h>

#include <ChilliSource/Rendering/Model/Model.h>
#include <ChilliSource/Rendering/Model/ModelDesc.h>
#include <ChilliSource/Rendering/Model/StaticModelComponent.h>

#include <Math/Noise/Simplex.hpp>

u64 IsoSphere::IcoSphereCreator::m_entityCount = 0;

namespace DEBUG_ICONOSPHERE
{
	std::vector<CS::EntitySPtr> normals;
}

struct TriangleIndexGroup
{
public:
	TriangleIndexGroup(u16 in_v1, u16 in_v2, u16 in_v3) : v1(in_v1), v2(in_v2), v3(in_v3), normal(CS::Vector3::k_zero){}
	TriangleIndexGroup():v1(0),v2(0),v3(0), normal(CS::Vector3::k_zero){};

public:
	u16 v1;
	u16 v2;
	u16 v3;
	CS::Vector3 normal;

	inline bool contains(u16 index) { return v1 == index || v2 == index || v3 == index; }
	inline bool operator==(const TriangleIndexGroup& rhs) { return v1 == rhs.v1 && v2 == rhs.v2 && v3 == rhs.v3; }
};

// add vertex to mesh, fix position to be on unit sphere, return index
int addVertex(IsoSphere::IcoSphereCreator::Geometry& in_geometry, CS::Vector3 p)
{
	float length = sqrt(p.x * p.x + p.y * p.y + p.z * p.z);
	in_geometry.positions.push_back(CS::Vector3(p.x / length, p.y / length, p.z / length));
	return in_geometry.m_vertexCount++;
}

// return index of point in the middle of p1 and p2
int addMiddlePoint(IsoSphere::IcoSphereCreator::Geometry& in_geometry, int p1, int p2)
{
	// first check if we have it already
	bool firstIsSmaller = p1 < p2;

	CS::Vector3 point1 = in_geometry.positions[p1];
	CS::Vector3 point2 = in_geometry.positions[p2];
	CS::Vector3 middle = CS::Vector3(
		(point1.x + point2.x) * 0.5f,
		(point1.y + point2.y) * 0.5f,
		(point1.z + point2.z) * 0.5f);

	// add vertex makes sure point is on unit sphere
	return addVertex(in_geometry, middle);
}

IsoSphere::IcoSphereCreator::Geometry CreateSphereGeometry(int recursionLevel, signed short in_octaves = 0, double in_lacunarity = 0.0, double in_persistence = 0.0)
{
	using namespace CS;

	IsoSphere::IcoSphereCreator::Geometry geometry;

	u32 numverts(20 * 3 + (20 * 4 * recursionLevel));
	geometry.positions.reserve(numverts);
	geometry.indices.reserve(numverts);
	geometry.normals.reserve(numverts);

	geometry.tangents.reserve(numverts);
	geometry.bitangents.reserve(numverts);

	// create 12 vertices of a icosahedron
	float t = (1.f + sqrtf(5.f)) * 0.5f;

	// debug
	//addVertex(geometry, Vector3(-1.f, t, 0.f));
	//addVertex(geometry, Vector3(1.f, t, 0.f));
	//addVertex(geometry, Vector3(-1.f, -t, 0.f));

	//addVertex(geometry, Vector3(1.f, t, 0.f));
	//addVertex(geometry, Vector3(-1.f, -t, 0.f));
	//addVertex(geometry, Vector3(1.f, -t, 3.f));

	addVertex(geometry, Vector3(-1.f, t, 0.f));
	addVertex(geometry, Vector3(1.f, t, 0.f));
	addVertex(geometry, Vector3(-1.f, -t, 0.f));
	addVertex(geometry, Vector3(1.f, -t, 0.f));

	addVertex(geometry, Vector3(0.f, -1.f, t));
	addVertex(geometry, Vector3(0.f, 1.f, t));
	addVertex(geometry, Vector3(0.f, -1.f, -t));
	addVertex(geometry, Vector3(0.f, 1.f, -t));

	addVertex(geometry, Vector3(t, 0.f, -1.f));
	addVertex(geometry, Vector3(t, 0.f, 1.f));
	addVertex(geometry, Vector3(-t, 0.f, -1.f));
	addVertex(geometry, Vector3(-t, 0.f, 1.f));


	// create 20 triangles of the icosahedron
	std::vector<TriangleIndexGroup> workingFaces;

	// debug
	//workingFaces.push_back(TriangleIndexGroup(0, 1, 2));
	//workingFaces.push_back(TriangleIndexGroup(3, 5, 4));

	// 5 faces around point 0
	workingFaces.push_back(TriangleIndexGroup(0, 11, 5));
	workingFaces.push_back(TriangleIndexGroup(0, 5, 1));
	workingFaces.push_back(TriangleIndexGroup(0, 1, 7));
	workingFaces.push_back(TriangleIndexGroup(0, 7, 10));
	workingFaces.push_back(TriangleIndexGroup(0, 10, 11));

	// 5 adjacent faces 
	workingFaces.push_back(TriangleIndexGroup(1, 5, 9));
	workingFaces.push_back(TriangleIndexGroup(5, 11, 4));
	workingFaces.push_back(TriangleIndexGroup(11, 10, 2));
	workingFaces.push_back(TriangleIndexGroup(10, 7, 6));
	workingFaces.push_back(TriangleIndexGroup(7, 1, 8));

	// 5 faces around point 3
	workingFaces.push_back(TriangleIndexGroup(3, 9, 4));
	workingFaces.push_back(TriangleIndexGroup(3, 4, 2));
	workingFaces.push_back(TriangleIndexGroup(3, 2, 6));
	workingFaces.push_back(TriangleIndexGroup(3, 6, 8));
	workingFaces.push_back(TriangleIndexGroup(3, 8, 9));

	// 5 adjacent faces 
	workingFaces.push_back(TriangleIndexGroup(4, 9, 5));
	workingFaces.push_back(TriangleIndexGroup(2, 4, 11));
	workingFaces.push_back(TriangleIndexGroup(6, 2, 10));
	workingFaces.push_back(TriangleIndexGroup(8, 6, 7));
	workingFaces.push_back(TriangleIndexGroup(9, 8, 1));

	// refine triangles
	for (int i = 0; i < recursionLevel; ++i)
	{
		std::vector<TriangleIndexGroup> updatedFaces;
		
		for(auto tri : workingFaces)
		{
			// replace triangle by 4 triangles, return appended vert number
			int a = addMiddlePoint(geometry, tri.v1, tri.v2);
			int b = addMiddlePoint(geometry, tri.v2, tri.v3);
			int c = addMiddlePoint(geometry, tri.v3, tri.v1);

			updatedFaces.push_back(TriangleIndexGroup(tri.v1, a, c));
			updatedFaces.push_back(TriangleIndexGroup(tri.v2, b, a));
			updatedFaces.push_back(TriangleIndexGroup(tri.v3, c, b));
			updatedFaces.push_back(TriangleIndexGroup(a, b, c));
		}
		workingFaces = updatedFaces;
	}

	// done, now add triangles to mesh
	for (auto tri : workingFaces)
	{
		geometry.indices.push_back(tri.v1);
		geometry.indices.push_back(tri.v2);
		geometry.indices.push_back(tri.v3);
	}

	// handle noise and normals
	if (in_octaves > 0)
	{
		// get uv
		for (auto& vertPos : geometry.positions)
		{
			// vert pos is the same as normal, which we pass to asin
			double u = asin(vertPos.x) / CS::MathUtils::k_pi + 0.5;
			double v = asin(vertPos.y) / CS::MathUtils::k_pi + 0.5;

			double val = (Simplex::octaveNoise(u, v, in_octaves, in_lacunarity, in_persistence) + 1.0) * 0.5;

			vertPos += CS::Vector3::Normalise(vertPos) * (float)val;
		}

		// -- brute force our way to get neighbouring triangles

		// calculate triangle normals
		for (auto& tri : workingFaces) // done with working faces, can mess with them
		{
			Vector3 edge1(geometry.positions[tri.v2] - geometry.positions[tri.v1]);
			Vector3 edge2(geometry.positions[tri.v3] - geometry.positions[tri.v1]);

			tri.normal = Vector3::Normalise(Vector3::CrossProduct(edge1, edge2));

			//CS_LOG_VERBOSE("tri " + CS::ToString(tri.v1) + ", " + CS::ToString(tri.v2) + ", " + CS::ToString(tri.v3));
		}


		// calculate tangent and bitangents
		for (auto& tri : workingFaces) // done with working faces, can mess with them
		{
			Vector3 edge1(geometry.positions[tri.v2] - geometry.positions[tri.v1]);
			Vector3 edge2(geometry.positions[tri.v3] - geometry.positions[tri.v1]);

			tri.normal = Vector3::Normalise(Vector3::CrossProduct(edge1, edge2));

			// add new tangents and bitangents

			Vector3 bitangent(edge1);
			bitangent.CrossProduct(tri.normal);

			for (u32 i(0); i < 3; ++i)
			{
				geometry.tangents.push_back(edge1);
				geometry.bitangents.push_back(bitangent);
			}
		}

		// calculate vertex normals from average shared-triangle normals
		for (u16 vertIndex(0); vertIndex < geometry.positions.size(); ++vertIndex)
		{
			Vector3 vertNormal(CS::Vector3::k_zero);
			u8 triCount(0);

			// run all faces, find neighbours that share this vert
			for (auto nextTri : workingFaces)
			{
				// shared vert
				if (nextTri.contains(vertIndex))
				{
					// add normal
					vertNormal += nextTri.normal;

					++triCount;
				}
			}
			//CS_LOG_VERBOSE(CS::ToString(vertIndex) + ": triCount: " + CS::ToString(triCount));
			vertNormal /= triCount;
			geometry.normals.push_back( CS::Vector3::Normalise(vertNormal) );
		}
	}
	else
	{
		// normals are just vert positions
		for (auto vertPos : geometry.positions)
		{
			geometry.normals.push_back(Vector3::Normalise(vertPos));
		}
	}

	return geometry;
}


IsoSphere::IcoSphereCreator::Geometry CreateNonIndexedSphereGeometry(int recursionLevel, signed short in_octaves = 0, double in_lacunarity = 0.0, double in_persistence = 0.0)
{
	using namespace CS;

	IsoSphere::IcoSphereCreator::Geometry geometry;

	u32 numverts(20 * 3 + (20 * 4 * 3 * recursionLevel));
	geometry.positions.reserve(numverts);
	geometry.normals.reserve(numverts);

	// create 12 vertices of a icosahedron
	float t = (1.f + sqrtf(5.f)) * 0.5f;

	addVertex(geometry, Vector3(-1.f, t, 0.f));
	addVertex(geometry, Vector3(1.f, t, 0.f));
	addVertex(geometry, Vector3(-1.f, -t, 0.f));
	addVertex(geometry, Vector3(1.f, -t, 0.f));

	addVertex(geometry, Vector3(0.f, -1.f, t));
	addVertex(geometry, Vector3(0.f, 1.f, t));
	addVertex(geometry, Vector3(0.f, -1.f, -t));
	addVertex(geometry, Vector3(0.f, 1.f, -t));

	addVertex(geometry, Vector3(t, 0.f, -1.f));
	addVertex(geometry, Vector3(t, 0.f, 1.f));
	addVertex(geometry, Vector3(-t, 0.f, -1.f));
	addVertex(geometry, Vector3(-t, 0.f, 1.f));


	// create 20 triangles of the icosahedron
	std::vector<TriangleIndexGroup> workingFaces;

	// 5 faces around point 0
	workingFaces.push_back(TriangleIndexGroup(0, 11, 5));
	workingFaces.push_back(TriangleIndexGroup(0, 5, 1));
	workingFaces.push_back(TriangleIndexGroup(0, 1, 7));
	workingFaces.push_back(TriangleIndexGroup(0, 7, 10));
	workingFaces.push_back(TriangleIndexGroup(0, 10, 11));

	// 5 adjacent faces 
	workingFaces.push_back(TriangleIndexGroup(1, 5, 9));
	workingFaces.push_back(TriangleIndexGroup(5, 11, 4));
	workingFaces.push_back(TriangleIndexGroup(11, 10, 2));
	workingFaces.push_back(TriangleIndexGroup(10, 7, 6));
	workingFaces.push_back(TriangleIndexGroup(7, 1, 8));

	// 5 faces around point 3
	workingFaces.push_back(TriangleIndexGroup(3, 9, 4));
	workingFaces.push_back(TriangleIndexGroup(3, 4, 2));
	workingFaces.push_back(TriangleIndexGroup(3, 2, 6));
	workingFaces.push_back(TriangleIndexGroup(3, 6, 8));
	workingFaces.push_back(TriangleIndexGroup(3, 8, 9));

	// 5 adjacent faces 
	workingFaces.push_back(TriangleIndexGroup(4, 9, 5));
	workingFaces.push_back(TriangleIndexGroup(2, 4, 11));
	workingFaces.push_back(TriangleIndexGroup(6, 2, 10));
	workingFaces.push_back(TriangleIndexGroup(8, 6, 7));
	workingFaces.push_back(TriangleIndexGroup(9, 8, 1));

	// refine triangles
	for (int i = 0; i < recursionLevel; ++i)
	{
		std::vector<TriangleIndexGroup> updatedFaces;

		for (auto tri : workingFaces)
		{
			// replace triangle by 4 triangles, return appended vert number
			int a = addMiddlePoint(geometry, tri.v1, tri.v2);
			int b = addMiddlePoint(geometry, tri.v2, tri.v3);
			int c = addMiddlePoint(geometry, tri.v3, tri.v1);

			updatedFaces.push_back(TriangleIndexGroup(tri.v1, a, c));
			updatedFaces.push_back(TriangleIndexGroup(tri.v2, b, a));
			updatedFaces.push_back(TriangleIndexGroup(tri.v3, c, b));
			updatedFaces.push_back(TriangleIndexGroup(a, b, c));
		}
		workingFaces = updatedFaces;
	}


	// do magic pass for seam where triangles have vert  uvs both >0.9 and < 0.1, makes the triangle render shit
	std::vector<u32> seamFixIndices; // tri indices for before non-index generation
	std::vector<u32> seamFixPosGenVerts; // indices of the post non-index mesh
	{
		const f32 radius = 1.f;
		// run all triangles
		for (auto& tri : workingFaces)
		{
			// generate vert uvs
			Vector3 vert0(geometry.positions[tri.v1]);
			Vector3 vert1(geometry.positions[tri.v2]);
			Vector3 vert2(geometry.positions[tri.v3]);
			
			f32 u0( 0.5f + atan2(vert0.x, vert0.z) / (2 * CS::MathUtils::k_pi) );
			//f32 v0( 0.5f - asin(vert0.y / radius) / CS::MathUtils::k_pi );

			f32 u1( 0.5f + atan2(vert1.x, vert1.z) / (2 * CS::MathUtils::k_pi) );
			//f32 v1( 0.5f - asin(vert1.y / radius) / CS::MathUtils::k_pi );

			f32 u2( 0.5f + atan2(vert2.x, vert2.z) / (2 * CS::MathUtils::k_pi) );
			//f32 v2( 0.5f - asin(vert2.y / radius) / CS::MathUtils::k_pi );

			const f32 k_uvSensitivity(0.5f);
			// if diff between one or two is > 0.5
			bool vert0High(u1 - u0 > k_uvSensitivity || u2 - u0 > k_uvSensitivity);
			bool vert1High(u0 - u1 > k_uvSensitivity || u2 - u1 > k_uvSensitivity);
			bool vert2High(u0 - u2 > k_uvSensitivity || u1 - u2 > k_uvSensitivity);

			/*Vector2 diff0(fabs(u0-u1), fabs(v0-v1));
			Vector2 diff1(fabs(u1-u2), fabs(v1-v2));
			Vector2 diff2(fabs(u2-u0), fabs(v2-v0));*/

			
			// create one or two new verts in position of the low uv verts
			// and rewire triangle to use new indices			
			if (vert0High)
			{
				int index = addVertex(geometry, vert0); // add new vert at exact position
				tri.v1 = index; // assign tri index to be new vertex
				seamFixIndices.push_back(index);
			}

			if (vert1High)
			{
				int index = addVertex(geometry, vert1); // add new vert at exact position
				tri.v2 = index; // assign tri index to be new vertex
				seamFixIndices.push_back(index);
			}

			if (vert2High)
			{
				int index = addVertex(geometry, vert2); // add new vert at exact position
				tri.v3 = index; // assign tri index to be new vertex
				seamFixIndices.push_back(index);
			}			
		}
	}
	
	{
		std::vector<CS::Vector3> perTriPositions;
		perTriPositions.reserve((size_t)numverts);

		// done, now add verts for all tri indices
		for (auto tri : workingFaces)
		{
			perTriPositions.push_back(geometry.positions[tri.v1]);
			perTriPositions.push_back(geometry.positions[tri.v2]);
			perTriPositions.push_back(geometry.positions[tri.v3]);
		}

		// seam hack - TODO make the prev for loop indexed, can maybe do this pass there and skip the index->vert conversion
		for (u32 i(0); i < workingFaces.size(); ++i)
		{
			for (auto fixIndex : seamFixIndices)
			{
				if (fixIndex == workingFaces[i].v1)
					seamFixPosGenVerts.push_back(i * 3);
				if (fixIndex == workingFaces[i].v2)
					seamFixPosGenVerts.push_back(i * 3+1);
				if (fixIndex == workingFaces[i].v3)
					seamFixPosGenVerts.push_back(i * 3+2);
			}
		}

		// overwrite shared vert positions with 3-per-triangle verts
		geometry.positions = perTriPositions;
	}
	
	// handle noise and normals
	u32 posVertIndex(0);

	for (auto& vertPos : geometry.positions)
	{
		// vert pos is the same as normal, which we pass to asin
	//	double u = asin(vertPos.x) / CS::MathUtils::k_pi + 0.5; // old symetrical way
	//	double v = asin(vertPos.y) / CS::MathUtils::k_pi + 0.5;
	//	double u = vertPos.x + vertPos.z;						// newer working but clunky way
	//	double v = vertPos.y;

		
		//f32 u = acos(correctedPos.z);
		//f32 v = atan2(correctedPos.y, correctedPos.x);
					
		if (in_octaves > 0)
		{
			f32 u = 0.5f + atan2(vertPos.x, vertPos.z) / (2.f * CS::MathUtils::k_pi);
			f32 radius = 1.f;
			f32 v = 0.5f - asin(vertPos.y / radius) / CS::MathUtils::k_pi;

		//	double val = 1.f;
			double val = (Simplex::octaveNoise(u, v, in_octaves, in_lacunarity, in_persistence) + 1.0) * 0.5;

			vertPos += Vector3::Normalise(vertPos) * (float)val;

			// want to run prev verts and look for ones near our spherical coord with diff radius value
			// or maybe just run a smoother looking for high gradients?

			//if (u > 0.75f || u < 0.f || v > 0.75f || v < 0.f)
			//	CS_LOG_VERBOSE(CS::ToString(u) + " " + CS::ToString(v));
		}

		// calculate spherical texture coordinates, use the two angles to get the radius length
		CS::Vector3 correctedPos(vertPos);// -geometry.centroid);

		f32 tu = 0.5f + atan2(correctedPos.x, correctedPos.z) / (2.f * CS::MathUtils::k_pi);
		f32 radius = vertPos.Length();
		f32 tv = 0.5f - asin(correctedPos.y / radius) / CS::MathUtils::k_pi;

		/*if (tu > 1.f)
			tu = 1.f;
		if (tv > 1.f)
			tv = 1.f;*/

		// check if these are added to fix seam
		for (auto fixedIndex : seamFixPosGenVerts)
		{
			if (posVertIndex == fixedIndex)
			{
				tu += 1.f;
				break;
			}
		}

		++posVertIndex;

		// store spherical uv coords, as they're in a nice normalised domain
		geometry.uvs.push_back(CS::Vector2(tu, tv));
	}


	// build indices
	for (size_t i(0); i < geometry.positions.size(); ++i)
	{
		geometry.indices.push_back((u16)i);
	}

	// calculate vert normals
	for (size_t i(0); i < geometry.positions.size(); i += 3)
	{
		Vector3 edge1(geometry.positions[i + 1] - geometry.positions[i]);
		Vector3 edge2(geometry.positions[i + 2] - geometry.positions[i]);

		Vector3 normal = Vector3::Normalise(Vector3::CrossProduct(edge1, edge2));

		geometry.normals.push_back(normal);
		geometry.normals.push_back(normal);
		geometry.normals.push_back(normal);

		// calculate tangent and bitangents		
		Vector3 bitangent(edge1);
		bitangent.CrossProduct(normal);

		for (u32 i(0); i < 3; ++i)
		{
			geometry.tangents.push_back(edge1);
			geometry.bitangents.push_back(bitangent);
		}
		
		//CS_LOG_VERBOSE("tri normals " + ToString(normal));
	}
	
	return geometry;
}


struct Vertex
{
	CS::Vector4 m_position;
	CS::Vector3 m_normal;
	CS::Vector3 m_tangent;
	CS::Vector3 m_bitangent;
	CS::Vector2 m_texCoord;
};

//------------------------------------------------------------------------------
/// Creates a new mesh description describing a sphere model.
///
/// @author snik
///
/// @param in_radius - radius from center
/// @param in_textureRepeat - The number of times the texture is repeated on
/// each face.
/// @param in_fidelity - Number of times we will add triangles to the starting
/// tetrahedron to approximate a sphere
///
/// @return The new mesh descriptor.
//------------------------------------------------------------------------------
CS::ModelDesc CreateSphereDesc(float in_radius, const CS::Vector2& in_textureRepeat, u16 in_fidelity, IsoSphere::NoiseDescription in_noiseDesc, std::vector<CS::Vector3>& out_vertices, float in_debugScale = 1.f)
{
	IsoSphere::IcoSphereCreator::Geometry geometry = CreateNonIndexedSphereGeometry(in_fidelity, in_noiseDesc.m_octaves, in_noiseDesc.m_lacunarity, in_noiseDesc.m_persistence); //6, 2, 0.4); //CreateSphereGeometry(in_fidelity, 6, 2, 0.4);

	using namespace CS;

	Vector3 halfSize = Vector3(in_radius, in_radius, in_radius);

	u32 numVerts(geometry.positions.size());
	u32 numIndices(geometry.indices.size());

	//Vertices
	Vertex* vertices = new Vertex[numVerts];

	float normalDir = 1.0f;

	for (u32 i = 0; i < numVerts; ++i)
	{
		//CS::Vector2 normalisedUVs((geometry.uvs[i].x + CS::MathUtils::k_pi) / (CS::MathUtils::k_pi * 2.f), (geometry.uvs[i].y + CS::MathUtils::k_pi) / (CS::MathUtils::k_pi * 2.f));
		//CS::Vector2 normalisedUVs((geometry.uvs[i].x + CS::MathUtils::k_pi) / (CS::MathUtils::k_pi * 2.f), (geometry.uvs[i].y + CS::MathUtils::k_pi) / (CS::MathUtils::k_pi * 2.f));
		CS::Vector2 normalisedUVs(geometry.uvs[i].x, geometry.uvs[i].y);

		//normalisedUVs = CS::Vector2(normalisedUVs.x * in_debugScale, normalisedUVs.y*in_debugScale);

//		CS_LOG_VERBOSE("desc uvs " + CS::ToString(i) + ": " + CS::ToString(normalisedUVs.x) + " x " + CS::ToString(normalisedUVs.y));

		vertices[i] = { Vector4(geometry.positions[i], 1.f), geometry.normals[i], geometry.tangents[i], geometry.bitangents[i], normalisedUVs }; // cartesianUVs };
	}

	//Indices
	u16 *indices(new u16[numIndices]);
	//subMesh.mpIndexData = reinterpret_cast<u8*>(indices);

	for (u32 i = 0; i < geometry.indices.size(); ++i)
		indices[i] = geometry.indices[i];

	//if (in_flipNormals == true)
	{
		for (u32 i(0); i < numIndices; i += 3)
		{
			u16 temp = indices[i + 1];
			indices[i + 1] = indices[i + 2];
			indices[i + 2] = temp;
		}
	}

	// DEBUG NORMAL DRAWING
	for (u32 i(0); i < geometry.normals.size(); ++i)
	{
		//EntityUPtr
		//CS::PrimitiveEntityFactory* factory = CS::Application::Get()->GetSystem<ChilliSource::Core::PrimitiveEntityFactory>();
		//CS::EntitySPtr box( factory->CreateBox(CS::Colour::k_cyan, CS::Vector3(0.01f, 0.01f, 1.f)) );

		//box->GetTransform().SetLookAt(CS::Vector3::k_zero, geometry.normals[i], CS::Vector3::k_unitPositiveY);
		//box->GetTransform().SetPosition(geometry.positions[i] + (geometry.normals[i] * 0.5f));

		//DEBUG_ICONOSPHERE::normals.push_back(box);

		//CS_LOG_VERBOSE("adding normal box " + CS::ToString(i));
	}

	//Create the mesh.
	//meshDescriptor.mMeshes.push_back(subMesh);
	//meshDescriptor.mvMinBounds = Core::Vector3(-halfSize.x, -halfSize.y, -halfSize.z);
	//meshDescriptor.mvMaxBounds = Core::Vector3(halfSize.x, halfSize.y, halfSize.z);

	// copy verts(unindexed) to out params

	for (size_t i(0); i < geometry.indices.size(); ++i)
	{
		out_vertices.push_back(vertices[indices[i]].m_position.XYZ());	
	}

	auto name = "SphereMesh" + in_noiseDesc.ToString();
	auto polygonType = PolygonType::k_triangle;
	auto vertexFormat = VertexFormat::k_staticMesh;
	auto indexFormat = IndexFormat::k_short;
	AABB aabb(Vector3::k_zero, Vector3(in_radius*2.f, in_radius * 2.f, in_radius * 2.f));
	Sphere boundingSphere(Vector3::k_zero, in_radius);
	std::unique_ptr<const u8[]> vertexData(reinterpret_cast<const u8*>(vertices));
	std::unique_ptr<const u8[]> indexData(reinterpret_cast<const u8*>(indices));

	std::vector<MeshDesc> meshDescs;
	meshDescs.push_back(MeshDesc(name, polygonType, vertexFormat, indexFormat, aabb, boundingSphere, numVerts, numIndices, std::move(vertexData), std::move(indexData)));

	return ModelDesc(std::move(meshDescs), aabb, boundingSphere);
}


CS::ModelCSPtr IsoSphere::IcoSphereCreator::CreateSphereMesh(float in_radius, const CS::Vector2& in_textureRepeat, std::vector<CS::Vector3>& out_vertices, u16 in_fidelity, bool in_flipNormals, NoiseDescription in_noiseDesc, float in_debugScale)
{
	CS_ASSERT(CS::Application::Get()->GetTaskScheduler()->IsMainThread(), "Cannot create models on a background thread.");

	auto resourcePool = CS::Application::Get()->GetResourcePool();
	auto meshName = "_PrimitiveSphere(" + CS::ToString(in_radius) + "|" + CS::ToString(in_textureRepeat) + "|" + CS::ToString(in_flipNormals) + ")" + CS::ToString(in_debugScale) + in_noiseDesc.ToString();
	auto mesh = resourcePool->GetResource<CS::Model>(meshName);

	if (mesh == nullptr)
	{
		auto mutableMesh = resourcePool->CreateResource<CS::Model>(meshName);

		// create sphere decription
		auto desc = CreateSphereDesc(in_radius, in_textureRepeat, in_fidelity, in_noiseDesc, out_vertices, in_debugScale);
		mutableMesh->Build(std::move(desc));
		mutableMesh->SetLoadState(CS::Resource::LoadState::k_loaded);

		mesh = mutableMesh;
	}

	return mesh;
}


CS::EntitySPtr IsoSphere::IcoSphereCreator::CreateSphere(const CS::MaterialCSPtr& in_material, float in_radius, std::vector<CS::Vector3>& out_vertices, u16 in_fidelity, NoiseDescription in_noiseDesc, float in_debugScale)
{
	CS_ASSERT(CS::Application::Get()->GetTaskScheduler()->IsMainThread(), "Entities must be created on the main thread.");

	auto resourcePool(CS::Application::Get()->GetResourcePool());

	// create mesh
	CS::ModelCSPtr mesh = CreateSphereMesh(in_radius, CS::Vector2(1.f,1.f), out_vertices, in_fidelity, false, in_noiseDesc, in_debugScale);
		 
	CS::StaticModelComponentSPtr meshComponent = std::make_shared<CS::StaticModelComponent>(mesh, in_material);
	meshComponent->SetShadowCastingEnabled(true);

	auto entity = CS::Entity::Create();
	entity->SetName(CS::ToString(m_entityCount++) + "-Sphere");
	entity->AddComponent(meshComponent);
	
	// DEBUG normal drawing
	for (u32 i(0); i < DEBUG_ICONOSPHERE::normals.size(); ++i)
	{
		CS::EntitySPtr normal = DEBUG_ICONOSPHERE::normals[i];
		entity->AddEntity(normal);
	}

	return std::shared_ptr<CS::Entity>(std::move(entity));
}

void IsoSphere::IcoSphereCreator::Destroy()
{
	DEBUG_ICONOSPHERE::normals.clear();
}

