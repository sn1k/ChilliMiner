
#ifndef _MATH_SPHERE_ICONOSPHERE_H_
#define _MATH_SPHERE_ICONOSPHERE_H_

#include <ChilliSource/ChilliSource.h>

#include <ChilliSource/Core/Math/Vector3.h>
#include <vector>


namespace IsoSphere
{
	struct NoiseDescription
	{
	public:
		double m_lacunarity = 0.0;
		double m_persistence = 0.0;
		signed short m_octaves = 0;

		NoiseDescription(signed short in_octaves = 0, double in_lacunarity = 0.0, double in_persistence = 0.0) :
			m_lacunarity(in_lacunarity), m_persistence(in_persistence), m_octaves(in_octaves) {}

		std::string ToString()
		{
			return CS::ToString(m_octaves) + "_" + CS::ToString(m_lacunarity) + "_" + CS::ToString(m_persistence);
		}
	};

	class IcoSphereCreator
	{
	public:
		struct Geometry
		{
		public:
			std::vector<CS::Vector3> positions;
			std::vector<u16> indices;
			std::vector<CS::Vector3> normals;
			std::vector<CS::Vector3> tangents;
			std::vector<CS::Vector3> bitangents;
			std::vector<CS::Vector2> uvs;
			u32 m_vertexCount = 0;
		};

	public:

		static CS::EntitySPtr CreateSphere(const CS::MaterialCSPtr& in_material, float in_radius, std::vector<CS::Vector3>& out_vertices, u16 in_fidelity = 0, NoiseDescription in_noiseDesc = NoiseDescription(), float in_debugScale = 1.f);

		static void Destroy();

	private:
		static CS::ModelCSPtr CreateSphereMesh(float in_radius, const CS::Vector2& in_textureRepeat, std::vector<CS::Vector3>& out_vertices, u16 in_fidelity, bool in_flipNormals, NoiseDescription in_noiseDesc = NoiseDescription(), float in_debugScale = 1.f);


	private:
		static u64 m_entityCount;

	};
}
#endif