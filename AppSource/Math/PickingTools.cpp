#include <Math/PickingTools.h>
#include <CSBackend/Rendering/OpenGL/Model/GLMesh.h>
#include <ChilliSource/Rendering/Model/StaticModelComponent.h>
#include <ChilliSource/Rendering/Model/Model.h>
#include <ChilliSource/Rendering/Model/RenderMesh.h>

#define EPSILON 1e-8


using namespace CS;


// returns vec with (u, v, t) on intersection
bool PickingTools::IntersectTriangle(
	const Ray& inRay,
	const Vector3& inVert0,
	const Vector3& inVert1,
	const Vector3& inVert2,
	Vector3& outvec)
{
	double det, inv_det;

	// find vectors for two edges sharing vert
	Vector3 edge1(inVert1 - inVert0);
	Vector3 edge2(inVert2 - inVert0);

	// begin calculating determinant also used to calculate U parameter
	Vector3 pVec(Vector3::CrossProduct(inRay.vDirection, edge2));

	// if determinant is near zero ray lies in plane of triangle
	det = Vector3::DotProduct(edge1, pVec);

	// if det is 0, cross is at 90deg with other edge, so is in tri plane
	if (det > -EPSILON && det < EPSILON)
		return false;

	inv_det = 1.0 / det;

	// calculate distance from vert to ray origin
	Vector3 distToVert0(inRay.vOrigin - inVert0);

	// calculate U parameter and test bounds
	outvec.x = Vector3::DotProduct(distToVert0, pVec) * inv_det;

	if (outvec.x < 0.0 || outvec.x > 1.0)
		return false;

	// prepare to test V parameter
	Vector3 qVec(Vector3::CrossProduct(distToVert0, edge1));

	// calculate V parameter and test bounds
	outvec.y = Vector3::DotProduct(inRay.vDirection, qVec) * inv_det;

	if (outvec.y < 0.0 || (outvec.x + outvec.y) > 1.0)
		return false;

	// calculate t, scale parameters ray intersects triangle
	outvec.z = Vector3::DotProduct(edge2, qVec) * inv_det;

	return true;
}

// TODO comment
bool PickingTools::Pick(
	const Ray& in_ray,
	const Vector3& inCamPos,
	const Matrix4& inModel,
	const std::vector<CS::Vector3>& in_verts,
	Vector3& outHitPos,
	Vector3& outTriNormal)
{
	// hit model
	Vector3 hitPos(0.f, 0.f, 0.f);
	f32 closestDist(FLT_MAX);

	for (unsigned int i(0), numVerts(in_verts.size()-2); i < numVerts; i += 3) // check 3 verts per tri
	{
		u32 offset = i * 4; // vec4 position size
		Vector3 vert0 = in_verts[i] * inModel;
		Vector3 vert1 = in_verts[i+1] * inModel;
		Vector3 vert2 = in_verts[i+2] * inModel;

		if (!IntersectTriangle(in_ray, vert0, vert1, vert2, hitPos))
			continue;

		f32 len((hitPos - inCamPos).Length());

		//TODO
		// this will fix -- thinking the camera is somehow somewhere else so this check passes inverted
		if (len >= closestDist)
			continue;
		
		closestDist = len;

		outHitPos = Vector3(vert0 + ((vert1 - vert0) * hitPos.x) + ((vert2 - vert0) * hitPos.y));
		
		CS_LOG_VERBOSE("outHitPos " + CS::ToString(outHitPos) + " inCamPos " + CS::ToString(inCamPos));

		outTriNormal = Vector3::Normalise(Vector3::CrossProduct(Vector3::Normalise(vert1 - vert0), Vector3::Normalise(vert2 - vert0)));
	}

	return (closestDist < FLT_MAX);
}


/*
void RenderPickedTriangle()
{
	// -- render picked tri
	glUseProgram(programWhiteID);

	//GLuint MatrixID = Utils3D::GetUniformLocation(programWhiteID, "MVP");
	//glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVPMatrix[0][0]);


	glm::vec3 triangleVerts[3];
	UtilsDebug::GetTris(triangleVerts[0], triangleVerts[1], triangleVerts[2]);

	static const glm::f32 k_selectionTriScale(1.01f);
	triangleVerts[0] *= k_selectionTriScale;
	triangleVerts[1] *= k_selectionTriScale;
	triangleVerts[2] *= k_selectionTriScale;

	glm::mat4 MVPTri(projectionMatrix * cameraViewMatrix * mat4(1));

	glUniformMatrix4fv(Utils3D::GetUniformLocation(programWhiteID, "MVP"), 1, GL_FALSE, &MVPTri[0][0]);

	glBindVertexArray(0); // ensure no VAO is bound so we don't overwrite

	glBindBuffer(GL_ARRAY_BUFFER, selectedTriBuff);
	glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(glm::vec3), &triangleVerts[0], GL_STATIC_DRAW);

	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glDrawArrays(GL_TRIANGLES, 0, 3);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(0);
	// -- end tri
}*/
