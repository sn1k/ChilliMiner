#ifndef _MATH_PICKINGTOOLS_H_
#define _MATH_PICKINGTOOLS_H_

#include <ChilliSource/ChilliSource.h>
#include <ChilliSource/Core/Math.h>

#include <ChilliSource/Rendering/ForwardDeclarations.h>

class PickingTools
{
public:
	static bool IntersectTriangle(
		const CS::Ray& inRay,
		const CS::Vector3& inVert0,
		const CS::Vector3& inVert1,
		const CS::Vector3& inVert2,
		CS::Vector3& outvec);

	static bool Pick(
		const CS::Ray& in_ray,
		const CS::Vector3& inCamPos,
		const CS::Matrix4& inModel,
		const std::vector<CS::Vector3>& in_verts,
		CS::Vector3& outHitPos,
		CS::Vector3& outTriNormal);

};

#endif
